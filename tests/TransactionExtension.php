<?php


namespace Tests;

use App\Transaction;
use App\User;
use Carbon\Carbon;

trait TransactionExtension
{
    /**
     * @param $user User
     * @param $options array
     */
    public function createTransactionFor($user, array $options = [])
    {
        $sign = isset($options['sign']) ? $options['sign'] : '+';

        $transaction = new Transaction();
        $transaction->user_id = $user->id;
        $transaction->sum = isset($options['sum']) ? $sign . $options['sum'] : $sign . 100;
        $transaction->description = isset($options['description']) ? $options['description'] : 'Transaction description';
        $transaction->status = isset($options['status']) ? $options['status'] : 'success';
        $transaction->created_at = Carbon::now()->addMinute(isset($options['minutes']) ? $options['minutes'] : 1);
        $transaction->save();
    }

    public function createInOutTransactionFor($login)
    {
        $user = $this->createUserIfNotExists($login);
        $this->createTransactionFor($user, ['sum' => 1]);
        $this->createTransactionFor($user, ['sum' => 2, 'sign' => '-']);

        return $user;
    }
}