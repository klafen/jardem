<?php

namespace Tests\Backpack;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TransactionTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    /** @test */
    public function admin_can_not_see_add_transaction_button()
    {
        $this->createWaitedTransactionAndUser('user');

        $this->actingAsAdmin()
            ->visit('admin/transaction?status=wait')
            ->dontSee('Добавить')
            ->visit('admin/transaction')
            ->dontSee('Добавить');
    }

    /** @test */
    public function admin_can_not_see_approvement_buttons_where_transactions_approved()
    {
        $this->createWaitedTransactionAndUser('user');

        $this->actingAsAdmin()
            ->visit('admin/transaction')
            ->dontSee('Принять')
            ->dontSee('Отказать');
    }

    /** @test */
    public function admin_can_update_transaction_status_to_success()
    {
        $this->createWaitedTransactionAndUser('user');

        $this->actingAsAdmin()
            ->visit('admin/transaction?status=wait')
            ->click('Принять');

        $this->seeInDatabase('transactions', ['status' => 'success']);
    }

    /** @test */
    public function admin_can_update_transaction_status_to_fail()
    {
        $this->createWaitedTransactionAndUser('user');

        $this->actingAsAdmin()
            ->visit('admin/transaction?status=wait')
            ->click('Отказать');

        $this->seeInDatabase('transactions', ['status' => 'fail']);
    }

    /** @test */
    public function route_with_transaction_status_wait_show_only_waiting_transactions()
    {
        $this->createBaseTransactions();

        $this->actingAsAdmin()
            ->visit('admin/transaction?status=wait')
            ->see('123')
            ->dontSee('456');
    }

    /** @test */
    public function route_admin_transaction_show_only_transactions_with_fail_success_status()
    {
        $this->createBaseTransactions();

        $this->actingAsAdmin()
            ->visit('admin/transaction')
            ->dontSee('123')
            ->see('456')
            ->see('789');
    }

    private function createWaitedTransactionAndUser($login)
    {
        $user = $this->createUserIfNotExists($login);
        $this->createTransactionFor($user, ['status' => 'wait', 'sign' => '-']);
    }

    private function createBaseTransactions()
    {
        $user = $this->createUserIfNotExists('user');
        $this->createTransactionFor($user, ['sum' => 123, 'status' => 'wait', 'sign' => '-']);
        $this->createTransactionFor($user, ['sum' => 456, 'status' => 'fail', 'sign' => '-']);
        $this->createTransactionFor($user, ['sum' => 789, 'status' => 'success', 'sign' => '-']);
    }
}
