<?php

namespace Tests\Backpack;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class IsAdminTest extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;

    /** @test */
    public function i_can_grant_a_user_admin_privileges()
    {
        $user = $this->createUserIfNotExists('test');
        $this->assertFalse($user->isAdmin());

        $user->grantAdminPrivileges();
        $this->assertTrue($user->isAdmin());
    }

    /** @test */
    public function that_admin_can_access_admins_pages()
    {
        $this
            ->actingAsAdmin()
            ->visit('admin')
            ->seePageIs('admin/dashboard');
    }

    /** @test */
    public function that_user_cannot_access_admins_pages()
    {
        $this
            ->actingAsUser()
            ->visit('admin/')
            ->seePageIs('/');
    }
}
