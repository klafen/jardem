<?php

use App\Depozit\Desk;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class DesksTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();

        for ($level = 1; $level <= 6; $level++) {
            $this->createUserWithDeskLevel($level);
        }
    }

    /** @test */
    public function check_redirect_to_default_desk_level_if_level_does_not_match_values_from_1_to_6()
    {
        $this
            ->actingAsAdmin()
            ->visit('admin/desk?desk=0')
            ->seePageIs('admin/desk')
            ->visit('admin/desk?desk=7')
            ->seePageIs('admin/desk');
    }

    /** @test */
    public function check_default_desks_route_showing_only_users_with_first_level()
    {
        $user = $this->getUserFromLevel(1);

        $this
            ->actingAsAdmin()
            ->visit('admin/desk')
            ->see($user->login);
    }

    /** @test */
    public function check_2_level_desk()
    {
        $user = $this->getUserFromLevel(2);
        $this->assertUsersForCurrentLevel(2, $user);
    }

    /** @test */
    public function check_3_level_desk()
    {
        $user = $this->getUserFromLevel(3);
        $this->assertUsersForCurrentLevel(3, $user);
    }

    /** @test */
    public function check_4_level_desk()
    {
        $user = $this->getUserFromLevel(4);
        $this->assertUsersForCurrentLevel(4, $user);
    }

    /** @test */
    public function check_5_level_desk()
    {
        $user = $this->getUserFromLevel(5);
        $this->assertUsersForCurrentLevel(5, $user);
    }

    /** @test */
    public function check_6_level_desk()
    {
        $user = $this->getUserFromLevel(6);
        $this->assertUsersForCurrentLevel(6, $user);
    }


    private function createUserWithDeskLevel($deskLevel)
    {
        $user = $this->createUserIfNotExists('UserNumber' . $deskLevel);
        Desk::createForLevel($deskLevel)->addMember($user);

        return $user;
    }

    private function getUserFromLevel($level)
    {
        return User::where('login', 'UserNumber' . $level)->first();
    }

    private function getOtherUsers(User $user)
    {
        return User
            ::where('login', '!=', $user->login)
            ->where('login', '!=', 'admin')
            ->get();
    }

    private function assertUsersForCurrentLevel($level, $user)
    {
        $otherUsers = $this->getOtherUsers($user);

        $this
            ->actingAsAdmin()
            ->visit('admin/desk?desk=' . $level)
            ->see($user->login);

        foreach ($otherUsers as $user) {
            $this->dontSee($user->login);
        }
    }
}
