<?php

namespace Tests\Backpack;

use App\News;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class NewsTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    private $testItemValues = [
        'slug' => 'test_news_slug',
        'title' => 'Test News Title',
        'content' => 'Test News Content',
    ];

    /**
     * @var News
     */
    private $testItem = null;

    /** @test */
    public function that_the_news_can_be_created()
    {
        $this
            ->tryToCreateItemManually($this->testItemValues)
            ->seePageIs('admin/news')
            ->see($this->testItemValues['title'])
            ->see($this->testItemValues['slug'])
            ->see($this->testItemValues['content']);
    }

    /** @test */
    public function that_the_adding_validation_news_checking_unique_slug()
    {
        $this
            ->createTestItemInDb()
            ->tryToCreateItemManually(['slug' => $this->testItem->slug])
            ->see('Такой адрес (slug) уже имеется в базе.');
    }

    /** @test */
    public function that_the_adding_validation_news_required_title_slug_content()
    {
        $this
            ->tryToCreateItemManually([])
            ->see('Поле title обязательно')
            ->see('Поле slug обязательно')
            ->see('Поле content обязательно');
    }

    /** @test */
    public function that_the_added_news_is_showing()
    {
        $this
            ->createTestItemInDb()
            ->visit('news/' . $this->testItem->id)
            ->see($this->testItemValues['title'])
            ->see($this->testItemValues['content']);
    }

    /** @test */
    public function author_is_displayed_on_the_item_page()
    {
        $this->getTestAdministrator()->update([
            'first_name' => 'Admin',
            'last_name' => 'Adminovich',
        ]);

        $this
            ->tryToCreateItemManually($this->testItemValues)
            ->visit('news/' . News::bySlug('test_news_slug')->id)
            ->see('Admin Adminovich');
    }

    /** @test */
    public function that_the_editing_validation_news_required_title_slug_content()
    {
        $this
            ->tryToUpdateItemManually(['title' => '', 'slug' => '', 'content' => ''])
            ->see('Поле title обязательно')
            ->see('Поле slug обязательно')
            ->see('Поле content обязательно');
    }

    /**
     * @test
     */
    public function that_the_changed_slug_validated_correctly_before_updating()
    {
        $this
            ->createTestItemInDb()
            ->tryToUpdateItemManually(['slug' => $this->testItem->slug])
            ->see('Такой адрес (slug) уже имеется в базе.');
    }

    /**
     * @test
     */
    public function page_unchanged_slug_validated_correctly_before_updating()
    {
        $this
            ->tryToUpdateItemManually(['title' => 'Changed title'])
            ->seePageIs('admin/news');
    }

    /**
     * @param array $values
     * @return $this
     */
    public function tryToCreateItemManually($values = [])
    {
        return $this
            ->actingAsAdmin()
            ->visit('/admin/news/create')
            ->typeAll($values)
            ->press('Добавить');
    }

    /**
     * @param array $values
     * @return $this
     */
    public function tryToUpdateItemManually($values = [])
    {
        return $this
            ->createTestItemInDb()
            ->actingAsAdmin()
            ->visit("admin/news/{$this->testItem->id}/edit")
            ->typeAll($values)
            ->press('Сохранить');
    }

    /**
     * @return $this
     */
    private function createTestItemInDb()
    {
        $this->testItem = News::create($this->testItemValues);

        return $this;
    }
}
