<?php

namespace Tests\Backpack;

use App\Page;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class StaticPagesTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    private $testPageValues = [
        'slug' => 'test_page',
        'title' => 'Test Page Title',
        'content' => 'Test Page Content',
    ];

    /** @test */
    public function that_the_migration_has_static_pages()
    {
        foreach (['contacts', 'about', 'terms', 'main'] as $slug){
            $this->visit($slug)
                ->seePageIs(($slug != 'main') ? $slug : '/');
        }
    }

    /** @test */
    public function that_the_slug_main_redirect_to_home()
    {
        $this->visit('/main')
            ->seePageIs('/');
    }

    /**
     * @type Page
     */
    private $testPage = null;

    /**
     * @test
     */
    public function user_can_see_static_pages()
    {
        $this
            ->createTestPage()
            ->visit('/test_page')
            ->see('Test Page Title')
            ->see('Test Page Content');
    }

    /**
     * @test
     */
    public function user_receives_404_when_he_tries_to_access_non_existent_page()
    {
        $this
            ->get('/non-existent-page')
            ->seeStatusCode(404);
    }

    /**
     * @test
     */
    public function admin_can_create_static_pages()
    {
        $this
            ->startPageCreation()
            ->typeAll($this->testPageValues)
            ->press('Добавить')
            ->seeInDatabase('pages', $this->testPageValues);
    }

    /**
     * @test
     */
    public function admin_can_edit_static_pages()
    {
        $newValues = [
            'title' => 'New Page Title',
            'slug' => 'new-page-slug',
            'content' => 'New Page Content',
        ];

        $this
            ->startPageEditing()
            ->typeAll($newValues)
            ->press('Сохранить')
            ->seeInDatabase('pages', $newValues)
            ->dontSeeInDatabase('pages', $this->testPageValues);
    }

    /**
     * @test
     */
    public function admin_can_delete_static_pages()
    {
        // We cannot test it because it needs JS
    }

    /**
     * @test
     */
    public function pages_are_validated_before_creation()
    {
        $this
            ->startPageCreation()
            ->press('Добавить')
            ->seeValidationErrors();
    }

    /**
     * @test
     */
    public function pages_are_validated_before_updating()
    {
        $this
            ->startPageEditing()
            ->cleanInputs(['title', 'slug', 'content'])
            ->press('Сохранить')
            ->seeValidationErrors();
    }

    /**
     * @test
     */
    public function page_unchanged_slug_validated_correctly_before_updating()
    {
        $this
            ->startPageEditing()
            ->type('Changed title', 'title')
            ->press('Сохранить')
            ->seePageIs('admin/page');
    }

    /**
     * @test
     */
    public function that_the_changed_slug_validated_correctly_before_updating()
    {
        Page::create([
            'title' => 'Page title',
            'slug' => 'slug_for_testing',
            'content' => 'Content',
        ]);

        $this
            ->startPageEditing()
            ->type('slug_for_testing', 'slug')
            ->press('Сохранить')
            ->see('Такой адрес (slug) уже имеется в базе.');
    }

    /**
     * @return $this
     */
    private function startPageCreation()
    {
        return $this
            ->actingAsAdmin()
            ->visit('/admin/page/create');
    }

    /**
     * @return $this
     */
    private function startPageEditing()
    {
        return $this
            ->createTestPage()
            ->actingAsAdmin()
            ->visit("admin/page/{$this->testPage->id}/edit");
    }

    /**
     * @return $this
     */
    private function createTestPage()
    {
        $this->testPage = Page::create($this->testPageValues);

        return $this;
    }

    /**
     * @return $this
     */
    private function seeValidationErrors()
    {
        return $this->see('Пожалуйста, исправьте эти ошибки');
    }
}
