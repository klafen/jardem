<?php

namespace Tests\Backpack;

use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UsersTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    private $userData = [
        'first_name' => 'userName',
        'last_name' => 'userLastName',
        'email' => 'user@mail.com',
        'date_of_birth' => '1900-10-10',
    ];

    /** @test */
    public function that_the_users_admin_page_showing_users()
    {
        $this->createUserIfNotExists('user1');
        $this->createUserIfNotExists('user2');
        $this->createUserIfNotExists('user3');

        $this
            ->actingAsAdmin()
            ->visit('admin/user')
            ->see('user1')
            ->see('user2')
            ->see('user3');
    }

    /** @test */
    public function that_the_users_admin_page_is_not_show_payment_buttons_on_admins()
    {
        $this->createUserIfNotExists('unpaid-admin')->grantAdminPrivileges();

        $this
            ->actingAsAdmin()
            ->visit('admin/user')
            ->dontSee('Оплатил');
    }

    /** @test */
    public function guest_and_users_can_not_visit_approve_payment()
    {
        $user = $this->createUserIfNotExists('user');

        $this
            ->visit("admin/user/{$user->id}/approve-payment")
            ->seePageIs('/');

        $this
            ->actingAsUser()
            ->visit("admin/user/{$user->id}/approve-payment")
            ->seePageIs('/');
    }

    /** @test */
    public function guest_can_not_send_post_to_set_payment()
    {
        $user = $this->createUserIfNotExists('user');

        $this
            ->post("admin/user/set-payment", ['login' => $user->login])
            ->assertTrue(is_null($user->fresh()->payment_date));

        $this
            ->actingAsUser()
            ->post("admin/user/set-payment", ['login' => $user->login])
            ->assertTrue(is_null($user->fresh()->payment_date));
    }

    /** @test */
    public function admin_can_mark_user_as_paid()
    {

        $user = $this->createUserIfNotExists('user');

        $this
            ->actingAsAdmin()
            ->visit('admin/user')
            ->click('Оплатил');

        $this->assertTrue($user->payment_date == null);
        $this->assertTrue($user->fresh()->payment_date != null);
    }

    /** @test */
    public function admin_can_update_users_data()
    {
        // TODO: Felix, здесь и ниже повторяется один и тот же паттерн
        // 1. $user = $this->createUserIfNotExists('user');
        // ...
        // 2. $this->actingAsAdmin()->visit("admin/user/{$user->id}/edit")
        // 3. Потом заполняются поля
        // 4. Потом жмётся кнопка сохранить
        // Почему бы не создать для этого вспомогательный метод?

        $user = $this->createUserIfNotExists('user');
        $date = Carbon::today()->subYear(18);

        $this
            ->actingAsAdmin()
            ->visit("admin/user/{$user->id}/edit")
            ->type('userNameUpdated', 'first_name')
            ->type('userLastNameUpdated', 'last_name')
            ->type('updated@gmail.com', 'email')
            ->type($date, 'date_of_birth')
            ->press('Сохранить');

        $user = $user->fresh();
        $this->assertEquals($date, $user->date_of_birth);
        $this->assertEquals('userNameUpdated', $user->first_name);
        $this->assertEquals('userLastNameUpdated', $user->last_name);
        $this->assertEquals('updated@gmail.com', $user->email);
    }

    /** @test */
    public function check_required_user_fields_validation_while_updating()
    {
        $user = $this->createUserIfNotExists('user');

        $this
            ->actingAsAdmin()
            ->visit("admin/user/{$user->id}/edit")
            ->cleanInputs(['first_name', 'last_name', 'email', 'date_of_birth'])
            ->press('Сохранить');

        // TODO: Felix, если мы хардкодим сообщения об ошибках в тестах, то любое
        // изменение в тексте будет требовать изменения теста. Чтобы этого избежать,
        // надо использовать `lang()`. Если не получится, попроси меня помочь.

        $this
            ->see('Поле first name обязательно.')
            ->see('Поле last name обязательно.')
            ->see('Поле email обязательно.')
            ->see('Поле date of birth обязательно.');
    }

    /** @test */
    public function check_date_of_birth_user_field_18_years_old_validation_while_updating()
    {
        $user = $this->createUserIfNotExists('user');
        $date = $this->underageBirthDate();

        $this
            ->actingAsAdmin()
            ->visit("admin/user/{$user->id}/edit")
            ->type($date, 'date_of_birth')
            ->press('Сохранить');

        $this->see('Возраст пользователя должен быть больше 18 лет');
    }

    /** @test */
    public function check_email_user_field_validation_showing_error_while_updating()
    {
        $userMail = $this->createUserIfNotExists('userLogin');
        $user = $this->createUserIfNotExists('user');

        $this
            ->actingAsAdmin()
            ->visit("admin/user/{$user->id}/edit")
            ->type($userMail->email, 'email')
            ->press('Сохранить');

        $this->see('Такой адрес (email) уже имеется в базе.');
    }

    /** @test */
    public function check_email_user_field_validation_ignoring_self_checking_while_updating()
    {
        $user = $this->createUserIfNotExists('user');

        $this
            ->actingAsAdmin()
            ->visit("admin/user/{$user->id}/edit")
            ->type($user->email, 'email')
            ->press('Сохранить');

        $this->dontSee('Такой адрес (email) уже имеется в базе.');
    }

    /** @test */
    public function admin_can_update_user_password()
    {
        $user = User::create($this->userData);
        $user->login = 'user';
        $user->save();

        $this
            ->actingAsAdmin()
            ->visit("admin/user/{$user->id}/edit")
            ->typeAll($this->userData)
            ->type('updatedPassword', 'password')
            ->press('Сохранить');

        $this->call('POST', '/logout', [
            '_token' => csrf_token(),
        ]);

        $this
            ->visit('/')
            ->click('Вход')
            ->type('user', 'login')
            ->type('updatedPassword', 'password')
            ->press('Войти')
            ->assertEquals('user', \Auth::user()->login);
    }

    /** @test */
    public function admin_does_not_updating_user_password_via_empty_value()
    {
        $user = User::create($this->userData);
        $user->login = 'user';
        $user->password = bcrypt('secret');
        $user->save();

        $this
            ->actingAsAdmin()
            ->visit("admin/user/{$user->id}/edit")
            ->typeAll($this->userData)
            ->type('', 'password')
            ->press('Сохранить');

        $this->call('POST', '/logout', [
            '_token' => csrf_token(),
        ]);

        $this
            ->visit('/')
            ->click('Вход')
            ->type('user', 'login')
            ->type('secret', 'password')
            ->press('Войти')
            ->assertEquals('user', \Auth::user()->login);
    }

    /** @test */
    public function check_password_user_field_validation_show_errors_if_symbols_less_than_6()
    {
        $user = User::create($this->userData);

        $this
            ->actingAsAdmin()
            ->visit("admin/user/{$user->id}/edit")
            ->typeAll($this->userData)
            ->type('55555', 'password')
            ->press('Сохранить')
            ->see('Пароль должен содержать минимум 6 символов.');
    }
}
