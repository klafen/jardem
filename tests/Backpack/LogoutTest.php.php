<?php

namespace Tests\Backpack;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LogoutTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    /** @test */
    public function that_the_logout_buttons_routs_working()
    {
        //TODO написать этот тест, когда обновимся до 5.4, кнопки используют JS обработку клика
    }
}
