<?php

namespace Tests;

use Carbon\Carbon;

trait CarbonExtension
{
    /**
     * Overridden to automatically convert {@link Carbon}
     * instances to strings.
     *
     * @param string $text
     * @param string $element
     * @return $this
     */
    public function type($text, $element)
    {
        if ($text instanceof Carbon) {
            $text = $text->format('Y-m-d');
        }

        return parent::type($text, $element);
    }

    /**
     * Returns a birth date for an adult person (more than 18 years old).
     *
     * @return Carbon
     */
    protected function adultBirthDate()
    {
        return $this->now()->subYears(19);
    }

    /**
     * Returns a birth date for an underage person (less than 18 years old).
     *
     * @return Carbon
     */
    protected function underageBirthDate()
    {
        return $this->now()->subYears(17);
    }

    /**
     * Shortcut for `Carbon::now()`.
     *
     * @return Carbon
     */
    protected function now()
    {
        return Carbon::now();
    }

    /**
     * If the `$text` is a Carbon instance, converts it to string.
     *
     * @param string|Carbon $text
     * @return string
     */
    protected function normalizeText($text)
    {
        if ($text instanceof Carbon) {
            return $text->format('Y-m-d');
        }

        return $text;
    }
}