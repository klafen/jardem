<?php

namespace Tests;

trait RegistrationExtension
{
    /**
     * The complete set of steps needed to register a user
     * using the real registration form.
     *
     * @see RegistrationExtension::fillRegistrationForm for default values
     *
     * @param array $additionalValues
     * @return TestCase
     */
    private function registerUser($additionalValues = [])
    {
        /** @var \Tests\TestCase $this */

        return $this
            ->fillRegistrationForm($additionalValues)
            ->press('Регистрация');
    }

    /**
     * @param array $additionalValues
     * @return TestCase
     */
    private function fillRegistrationForm($additionalValues = [])
    {
        /** @var \Tests\TestCase $this */

        return $this
            ->visit('/register')
            ->check('confirm_terms')
            ->type('user', 'login')
            ->type('user@mail.com', 'email')
            ->type('secret', 'password')
            ->type('secret', 'password_confirmation')
            ->type('Country', 'country')
            ->type('City', 'city')
            ->type('userName', 'first_name')
            ->type('userLastName', 'last_name')
            ->type($this->adultBirthDate(), 'date_of_birth')
            ->typeAll($additionalValues);
    }
}