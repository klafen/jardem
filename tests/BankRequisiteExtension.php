<?php

namespace Tests;

use App\BankRequisite;
use Faker\Provider\Lorem;

trait BankRequisiteExtension
{
    public function createBankRequisiteFor($user, $extraValues = [])
    {
        $values = [
            'digit' => 1,
            'digitCount' => 16,
            'first_name' => 'uFirstName',
            'last_name' => 'uLastName'
        ];

        $values = array_merge($values, $extraValues);

        $bankAccount = '';

        for($i = 1; $i <= $values['digitCount']; $i ++) {
            $bankAccount .= $values['digit'];
        }

        $bankRequisite = new BankRequisite();
        $bankRequisite->user_id = $user->id;
        $bankRequisite->bank_requisite = Lorem::text(100);
        $bankRequisite->first_name = $values['first_name'];
        $bankRequisite->last_name = $values['last_name'];
        $bankRequisite->bank_account = $bankAccount;
        $bankRequisite->save();

        return $bankRequisite;
    }
}