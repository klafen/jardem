<?php

namespace Tests\User;

use App\BankRequisite;

class BankRequisiteTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function has_a_safe_to_render_version_of_bank_account()
    {
        $requisite = new BankRequisite;

        $requisite->bank_account = '1111222233334444';

        $this->assertEquals('1111 **** **** 4444', $requisite->safeBankAccount);
    }

    /** @test */
    public function safe_bank_account_supports_longer_numbers()
    {
        $requisite = new BankRequisite;

        $requisite->bank_account = '11112222333344445555';

        $this->assertEquals('1111 **** **** **** 5555', $requisite->safeBankAccount);
    }
}
