<?php

namespace Tests\User;

use App\User;

class UserTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function may_convert_itself_to_string()
    {
        $user = new User;
        $user->first_name = 'John';
        $user->last_name = 'Doe';

        $this->assertEquals('John Doe', $user->__toString());
    }

    /** @test */
    public function may_convert_itself_to_string_without_last_name()
    {
        $user = new User;
        $user->first_name = 'John';

        $this->assertEquals('John', $user->__toString());
    }

    /** @test */
    public function may_convert_itself_to_string_without_first_name()
    {
        $user = new User;
        $user->last_name = 'Doe';

        $this->assertEquals('Doe', $user->__toString());
    }
}
