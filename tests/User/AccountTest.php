<?php

namespace Tests\User;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AccountTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    /** @test */
    public function guest_cannot_access_account_via_get_request()
    {
        $this
            ->visit('/account/profile')
            ->seePageIs('/login');
    }

    /** @test */
    public function guest_cannot_send_post_request_to_account_route()
    {
        $this->post('/account/update-profile')
            ->assertRedirectedTo('/login');
    }

    /** @test */
    public function user_can_access_its_account()
    {
        $this
            ->actingAsUser()
            ->visit('/account/profile')
            ->seePageIs('/account/profile');
    }

    /** @test */
    public function that_the_user_can_edit_his_data_and_the_old_data_saved()
    {
        $this
            ->actingAsUser()
            ->visit('/account/profile-edit')
            ->type('UserName', 'first_name')
            ->type('UserLastName', 'last_name')
            ->type('UserCountry', 'country')
            ->type('UserCity', 'city')
            ->press('Обновить данные')
            ->seeInDatabase('users', [
                'country' => 'UserCountry',
                'city' => 'UserCity',
                'first_name' => 'UserName',
                'last_name' => 'UserLastName',
            ]);
    }

    /** @test */
    public function validation_of_the_city_and_country_showing_errors()
    {
        $this
            ->actingAsUser()
            ->visit('/account/profile-edit')
            ->press('Обновить данные')
            ->see('Необходимо указать страну')
            ->see('Необходимо указать город');
    }

    /** @test */
    public function that_the_referral_user_have_sponsor_login_in_account()
    {
        $sponsor = $this->createUserIfNotExists('sponsor')->markAsPaid();
        $referral = $this->createUserIfNotExists('referral');

        $referral->desired_sponsor_id = $sponsor->id;
        $referral->markAsPaid();

        $this
            ->actingAs($referral)
            ->visit('/account/profile')
            ->see('sponsor');
    }

    /** @test */
    public function that_a_hacker_cannot_update_sensitive_fields()
    {
        $user = $this->createUserIfNotExists('user');
        $data = [
                'is_admin' => 1,
                'has_paid' => 1,
                'sponsor_id' => 100,
            ] + $user->toArray();

        $this
            ->actingAs($user)
            ->post('/account', $data);

        $user = $user->fresh();
        $this->assertNotEquals(1, $user->is_admin, 'is_admin');
        $this->assertNotEquals(1, $user->has_paid, 'has_paid');
        $this->assertNotEquals(100, $user->sponsor_id, 'sponsor_id');
    }

    /** @test */
    public function that_the_user_seeing_link_to_account_and_does_not_seeing_link_to_dashboard()
    {
        $this->actingAsUser()
            ->visit('/')
            ->see('Личный кабинет')
            ->dontSee('Панель управления');
    }

    /** @test */
    public function that_the_admin_seeing_link_to_backpack_dashboard_and_does_not_seeing_link_to_account()
    {
        $this->actingAsAdmin()
            ->visit('/')
            ->see('Панель управления')
            ->dontSee('Личный кабинет');
    }
}
