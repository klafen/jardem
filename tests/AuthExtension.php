<?php

namespace Tests;

use App\User;

trait AuthExtension
{
    /**
     * Authenticates as a user with admin privileges.
     *
     * @return $this
     */
    protected function actingAsAdmin()
    {
        return $this->actingAs($this->getTestAdministrator());
    }

    /**
     * Authenticates as a normal user without admin privileges.
     *
     * @return $this
     */
    protected function actingAsUser()
    {
        return $this->actingAs($this->createUserIfNotExists('user'));
    }

    /**
     * Returns the shared instance of a user with admin privileges.
     *
     * @return User
     */
    protected function getTestAdministrator()
    {
        return $this->createUserIfNotExists('admin')->grantAdminPrivileges();
    }

    /**
     * @param string $login
     * @param array $extraValues
     * @return User
     */
    protected function createUserIfNotExists($login, $extraValues = [])
    {
        $user = User::byLogin($login);

        if (!$user) {
            $user = new User;

            $defaultValues = [
                'login' => $login,
                'email' => "{$login}@jardem-c.com",
                'password' => bcrypt($login),
                'first_name' => ucfirst($login) . " Name",
                'last_name' => ucfirst($login) . " Surname",
                'date_of_birth' => '1900-10-10',
                'desired_sponsor_id' => User::root()->id,
            ];

            $allValues = array_merge($defaultValues, $extraValues);

            foreach($allValues as $key => $value) {
                $user->{$key} = $value;
            }

            $user->save();
        }

        return $user;
    }

    protected function createUsers($count, $extraValues = [])
    {
        $results = [];

        for($i = 0; $i < $count; $i++) {
            $results[] = $this->createUserIfNotExists('user' . ($i + 1), $extraValues);
        }

        return $results;
    }
}