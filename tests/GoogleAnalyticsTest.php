<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class GoogleAnalyticsTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    /** @test */
    public function check_body_has_ga_code_if_env_variable_exists()
    {
        if (env('GOOGLE_ANALYTICS') != NULL) {
            $this->visit('/')
                ->see('https://www.google-analytics.com/analytics.js');
        }
    }

    /** @test */
    public function check_body_has_not_ga_code_if_env_variable_not_exists()
    {
        if (env('GOOGLE_ANALYTICS') == NULL) {
            $this->visit('/')
                ->dontSee('https://www.google-analytics.com/analytics.js');
        }
    }
}
