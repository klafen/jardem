<?php

namespace Tests;

use DB;
use Illuminate\Database\DatabaseManager;

abstract class TestCase extends \Illuminate\Foundation\Testing\TestCase
{
    use AuthExtension;
    use CarbonExtension;
    use InputsExtension;
    use TransactionExtension;
    use BankRequisiteExtension;

    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    protected function setUp()
    {
        parent::setUp();

        // In order to fix "Too many connections" issue that appears on large test suites.
        //
        // Other solutions that didn't work:
        // - https://github.com/Codeception/Codeception/pull/4032/commits/fd12807d8139a2850cb1858587347de93a9e7892
        // - https://www.neontsunami.com/posts/too-many-connections-using-phpunit-for-testing-laravel-51
        // - https://github.com/laravel/framework/issues/10619#issuecomment-195339132
        DB::statement('SET GLOBAL max_connections = 300');
    }

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(\Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    /**
     * Overridden to automatically convert {@link \Carbon\Carbon}
     * instances to strings.
     *
     * @param string $text
     * @param string $element
     * @return $this
     */
    public function type($text, $element)
    {
        return parent::type($this->normalizeText($text), $element);
    }

    /**
     * Overridden to automatically convert {@link \Carbon\Carbon}
     * instances to strings.
     *
     * @param string $text
     * @param bool $negate
     * @return $this
     */
    public function see($text, $negate = false)
    {

        return parent::see($this->normalizeText($text), $negate);
    }
}
