<?php

namespace Tests;

trait InputsExtension
{
    /**
     * Accepts an associative array where key is a field name
     * and value is a text that should be typed into the field.
     *
     * ```php
     * $this->typeAll([
     *   'title' => 'Title',
     *   'content' => 'Content',
     * ]);
     * ```
     *
     * @param array $values
     * @return $this
     */
    protected function typeAll($values)
    {
        foreach($values as $input => $value) {
            $this->type($value, $input);
        }

        return $this;
    }

    /**
     * Sets an empty string as value for all given fields.
     *
     * ```php
     * $this->cleanInputs(['title', 'content']);
     * ```
     *
     * @param array $names
     * @return $this
     */
    protected function cleanInputs($names)
    {
        foreach($names as $name) {
            $this->cleanInput($name);
        }

        return $this;
    }

    /**
     * Sets an empty string as value for the given field.
     *
     * ```php
     * $this->cleanInput('title');
     * ```
     *
     * @param string $name
     * @return $this
     */
    protected function cleanInput($name)
    {
        $this->type('', $name);

        return $this;
    }
}