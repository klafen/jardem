<?php

// TODO Felix, удаляй ниспользуемые импорты, IDE их подсвечивает

use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TransactionTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    /** @test */
    public function showing_transactions_history_in_desc_sort()
    {
        $user = $this->createUserIfNotExists('user');

        foreach (range(1, 6) as $sum) {
            $this->createTransactionFor($user, ['sum' => $sum, 'minutes' => $sum]);
        }

        $transactions = $this
            ->actingAs($user)
            ->visit('account/history')
            ->crawler()->filter('.b-history-table-item__amount');

        $this->assertRegExp('/6 тенге/', $transactions->getNode(0)->nodeValue);
        $this->assertRegExp('/5 тенге/', $transactions->getNode(1)->nodeValue);
    }

    /** @test */
    public function user_can_see_its_withdrawal_request_in_the_history()
    {
        $user = $this->createUserIfNotExists('user')->giveMoney(1000);

        $requisite = $this->createBankRequisiteFor($user);

        $this->actingAs($user)
            ->visit('account/withdrawals')
            ->submitForm('Заказать вывод средств', [
                'amount' => '600',
                'bank_id' => $requisite->id,
            ])
            ->visit('account/history')
            ->see('-600')
            ->see('Вывод средств');
    }

    /** @test */
    public function showing_transactions_history_by_in()
    {
        $user = $this->createInOutTransactionFor('user');

        $this
            ->actingAs($user)
            ->visit('account/history?transaction-type=in')
            ->see('1 тенге')
            ->dontSee('2 тенге');
    }

    /** @test */
    public function showing_transactions_history_by_out()
    {
        $user = $this->createInOutTransactionFor('user');

        $this
            ->actingAs($user)
            ->visit('account/history?transaction-type=out')
            ->dontSee('1 тенге')
            ->see('2 тенге');
    }
}
