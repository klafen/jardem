<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;

    /** @test */
    public function user_see_his_data()
    {
        $sponsor = $this->createUserIfNotExists('user1');
        $user = $this->createUserIfNotExists('user2', [
            'first_name' => 'UName',
            'last_name' => 'ULName',
            'country' => 'UCountry',
            'city' => 'UCity',
            'sponsor_id' => $sponsor->id,
            ]);

        $this->actingAs($user)
            ->visit('account/profile')
            ->see('user2')
            ->see('user2@depozitgold.com')
            ->see('1900-10-10')
            ->see('UName')
            ->see('ULName')
            ->see('UCountry')
            ->see('UCity')
            ->see('user1');
    }

    /** @test */
    public function user_not_see_empty_fields()
    {
        $user = $this->createUserIfNotExists('user');

        $user->sponsor_id = null;
        $user->first_name = null;
        $user->last_name = null;
        $user->country = null;
        $user->city = null;
        $user->save();

        $this
            ->actingAs($user)
            ->visit('account/profile')
            ->dontSee('Имя')
            ->dontSee('Адрес')
            ->dontSee('Спонсор');
    }
}
