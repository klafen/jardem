<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class WithdrawalTest extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;

    /** @test */
    public function user_without_payment_data_cant_see_withdrawal_form()
    {
        $user = $this->createUserIfNotExists('user');

        $this
            ->actingAs($user)
            ->visit('account/withdrawals')
            ->see('Для вывода средств заполните ваши платежные данные.')
            ->dontSee('Заказать вывод средств');
    }

    /** @test */
    public function user_with_payment_data_can_see_withdrawal_form()
    {
        $user = $this->createUserPaymentTransaction('user');

        $this
            ->actingAs($user)
            ->visit('account/withdrawals')
            ->dontSee('Для вывода средств заполните ваши платежные данные.')
            ->see('Заказать вывод средств');
    }

    /** @test */
    public function withdrawal_form_validation()
    {
        $user = $this->createUserPaymentTransaction('user');

        $this
            ->actingAs($user)
            ->visit('account/withdrawals')

            ->submitForm('Заказать вывод средств')
            ->see('Необходимо ввести сумму')

            ->submitForm('Заказать вывод средств', ['amount' => 'asd'])
            ->see('Сумма для вывода может содержать только цифры')

            ->submitForm('Заказать вывод средств', ['amount' => 499])
            ->see('Выводимая сумма должна быть больше 500')

            ->submitForm('Заказать вывод средств', ['amount' => 3500])
            ->see('У вас недостаточно средств')

            ->submitForm('Заказать вывод средств', ['amount' => 500, 'bank_id' => ''])
            ->see('Вы должны указать банковский счёт для вывода');
    }

    /** @test */
    public function user_cannot_create_transaction_with_non_existent_bank_id_using_post_request()
    {
        $user = $this->createUserPaymentTransaction('user');

        $this
            ->actingAs($user)
            ->post('/account/withdraw-funds', [
                'amount' => 500,
                'bank_id' => 'non-existent-bank-id',
            ])
            ->dontSeeInDatabase('transactions', [
                'sum' => -500,
                'bank_requisite_id' => 'non-existent-bank-id',
            ]);
    }

    /** @test */
    public function user_cannot_create_transaction_with_non_owned_bank_id_using_post_request()
    {
        $user1 = $this->createUserIfNotExists('user1')->giveMoney(500);
        $user2 = $this->createUserIfNotExists('user2');

        $requisite = $this->createBankRequisiteFor($user2);

        $this
            ->actingAs($user1)
            ->post('/account/withdraw-funds', [
                'amount' => 500,
                'bank_id' => $requisite->id,
            ])
            ->dontSeeInDatabase('transactions', [
                'sum' => -500,
                'bank_requisite_id' => $requisite->id,
            ]);
    }

    /** @test */
    public function user_can_request_withdrawal_with_correct_amount_and_bank_id()
    {
        $user = $this->createUserIfNotExists('user')->giveMoney(500);

        $requisite = $this->createBankRequisiteFor($user);

        $this
            ->actingAs($user)
            ->visit('account/withdrawals')
            ->submitForm('Заказать вывод средств', [
                'amount' => 500,
                'bank_id' => $requisite->id,
            ])
            ->press('Заказать вывод средств');

        $this
            ->actingAsAdmin()
            ->visit('admin/transaction?status=wait')
            ->see('500')
            ->see($requisite->bank_account)
            ->see($requisite->first_name)
            ->see($requisite->last_name);
    }

    /** @test */
    public function method_user_total_money_is_correct()
    {
        $user = $this->createUserIfNotExists('user');
        $this->createTransactionFor($user, ['sum' => 100]);
        $this->createTransactionFor($user, ['sum' => 40, 'status' => 'success', 'sign' => '-']);
        $this->createTransactionFor($user, ['sum' => 10, 'status' => 'wait', 'sign' => '-']);
        $this->createTransactionFor($user, ['sum' => 10, 'status' => 'fail', 'sign' => '-']);

        $this->assertEquals(50, $user->totalMoney());
    }

    private function createUserPaymentTransaction($login)
    {
        $user = $this->createUserIfNotExists($login);
        $this->createBankRequisiteFor($user);
        $this->createTransactionFor($user, ['sum' => 3000]);

        return $user;
    }
}
