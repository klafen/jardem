<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PaymentDataTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    private $user;

    protected function setUp()
    {
        parent::setUp();
        $this->user = $this->createUserIfNotExists('user');
    }

    /** @test */
    public function show_bank_account_with_hidden_digits()
    {
        $this->createBankRequisiteFor($this->user, ['digit' => '2']);
        $this->createBankRequisiteFor($this->user, ['digit' => '3', 'digitCount' => 20]);

        $this
            ->actingAs($this->user)
            ->visit('account/payment-data')
            ->see('2222 **** **** 2222')
            ->see('3333 **** **** **** 3333');
    }

    /** @test */
    public function adding_bank_requisite()
    {
        $this
            ->actingAs($this->user)
            ->visit('account/edit-requisite-form')
            ->type('uFirstName', 'first_name')
            ->type('uLastName', 'last_name')
            ->type('Bank name', 'bank_requisite')
            ->type('22222222222222222222', 'bank_account')
            ->press('Обновить данные')
            ->visit('account/payment-data')
            ->see('uFirstName')
            ->see('uLastName')
            ->see('Bank name')
            ->see('2222 **** **** **** 2222');
    }

    /** @test */
    public function editing_bank_requisite()
    {
        $this->createBankRequisiteFor($this->user);

        $this
            ->actingAs($this->user)
            ->visit('account/payment-data')
            ->click('Редактировать')
            ->type('uFirstName Edited', 'first_name')
            ->type('uLastName Edited', 'last_name')
            ->type('Edited bank requisite', 'bank_requisite')
            ->type('22222222222222222223', 'bank_account')
            ->press('Обновить данные')
            ->visit('account/payment-data')
            ->see('uFirstName Edited')
            ->see('uLastName Edited')
            ->see('Edited bank requisite')
            ->see('2222 **** **** **** 2223');
    }

    /** @test */
    public function user_can_see_correct_data_when_he_edits_payment_data()
    {
        // BUG: https://bitbucket.org/cuteimpact/depositgold/issues/42

        $requisite = $this->createBankRequisiteFor($this->user);

        $this
            ->actingAs($this->user)
            ->visit('account/payment-data')
            ->click('Редактировать')
            ->see($requisite->first_name)
            ->see($requisite->last_name);
    }

    /** @test */
    public function validation_on_editing_bank_requisite()
    {
        $this->createBankRequisiteFor($this->user);

        $this
            ->actingAs($this->user)
            ->visit('account/payment-data')
            ->click('Редактировать')
            ->type('', 'first_name')
            ->type('', 'last_name')
            ->type('', 'bank_requisite')
            ->type('', 'bank_account')
            ->press('Обновить данные')
            ->see('Поле "Имя" обязательно для заполнения')
            ->see('Поле "Фамилия" обязательно для заполнения')
            ->see('Поле "Название банка" обязательно для заполнения')
            ->see('Поле "Номер счета" обязательно для заполнения');
    }

    /** @test */
    public function validation_on_adding_bank_requisite()
    {
        $this->createBankRequisiteFor($this->user);

        $this
            ->actingAs($this->user)
            ->visit('account/payment-data')
            ->click('Редактировать')
            ->type('', 'first_name')
            ->type('', 'last_name')
            ->type('', 'bank_requisite')
            ->type('', 'bank_account')
            ->press('Обновить данные')
            ->see('Поле "Имя" обязательно для заполнения')
            ->see('Поле "Фамилия" обязательно для заполнения')
            ->see('Поле "Название банка" обязательно для заполнения')
            ->see('Поле "Номер счета" обязательно для заполнения');
    }
}
