<?php

namespace Tests\Account;

use App\Depozit\Desk;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\DOMExtension;
use Tests\ReferralOperations;
use Tests\RegistrationExtension;
use Tests\TestCase;

class ReferralDataTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    use RegistrationExtension;
    use ReferralOperations;
    use DOMExtension;

    /** @test */
    public function sponsor_can_see_only_paid_invited_users_their_login_and_names()
    {
        $sponsor = $this->createSponsor();

        $this->registerUser(['sponsor_login' => 'sponsor']);

        // Sponsor mustn't see referrals that aren't marked as paid by admin
        $this
            ->actingAs($sponsor)
            ->visit('account/referral-invited-users')
            ->see('Всего: 0');

        User::byLogin('user')->markAsPaid();

        // ...and when referral is marked as paid, sponsor must see him
        $this
            ->visit('account/referral-invited-users')
            ->see('user')
            ->see('userName')
            ->see('userLastName')
            ->see('Всего: 1');
    }

    /** @test */
    public function sponsor_does_not_see_reinvests_in_invited_users()
    {
        $sponsor = $this->createSponsor();

        $this->createReferralsFor($sponsor, 2);

        $sponsor->giveReinvests(1);

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-invited-users')
            ->see('Всего: 2');
    }

    /** @test */
    public function office_does_not_see_reinvests_in_invited_users()
    {
        $office = User::root();

        $this->createReferralsFor($office, 2);

        $office->giveReinvests(1);

        $this
            ->actingAs($office)
            ->visit('account/referral-invited-users')
            ->see('Всего: 2');
    }

    /** @test */
    public function sponsor_can_see_paid_invited_users_from_other_levels()
    {
        $sponsor = $this->createSponsor();

        $this->createReferralsFor($sponsor, 4);

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-invited-users')
            ->see('Всего: 4');
    }

    /** @test */
    public function sponsor_can_see_the_guys_from_his_triplet()
    {
        $sponsor = $this->createSponsor();

        $referrals = $this->createReferralsFor($sponsor, 2);

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-data')
            ->see($referrals[0]->login)
            ->see($referrals[1]->login);
    }

    /** @test */
    public function sponsor_can_see_empty_places_in_his_triplet()
    {
        $sponsor = $this->createSponsor();

        $invited = $this->createUsersInvitedBy($sponsor, 2);

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-data')
            ->assertElementsCount('.b-tree .b-matrix-user--empty', 3);

        $invited[0]->markAsPaid();

        $this
            ->visit('account/referral-data')
            ->assertElementsCount('.b-tree .b-matrix-user--empty', 2);

        $invited[1]->markAsPaid();

        $this
            ->visit('account/referral-data')
            ->assertElementsCount('.b-tree .b-matrix-user--empty', 1);
    }

    /** @test */
    public function member_can_see_the_guys_from_his_desk()
    {
        $desk = Desk::createForLevel(1);

        $members = $this->addMembersTo($desk, 3);

        $this
            ->actingAs($members[0])
            ->visit('account/referral-data')
            ->see($members[0]->login)
            ->see($members[1]->login);
    }

    /** @test */
    public function member_can_see_empty_places_in_his_desk()
    {
        $sponsor = $this->createSponsor();

        $desk = Desk::createForLevel(1)->addMember($sponsor);

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-data')

            // When sponsor gets in to the table he is alone there
            // so he must see 6 empty places.
            ->assertElementsCount('.b-desk .b-matrix-user--empty', 6);

        $this->addMembersTo($desk, 5);

        $this
            // We must reload the sponsor as it happens in the real
            // app when user visits the page the second time.
            ->actingAs($sponsor->fresh())
            ->visit('account/referral-data')

            // When 5 new users get into the table, there must be
            // only a single empty place.
            ->assertElementsCount('.b-desk .b-matrix-user--empty', 1);
    }

    /** @test */
    public function user_can_see_its_reinvests_in_the_tree()
    {
        $sponsor = $this->createSponsor()->giveReinvests(1);

        $reinvest = $sponsor->reinvests->first();

        $referrals = $this->createReferralsFor($reinvest, 2);

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-data')
            ->see($reinvest->login)
            ->see($referrals[0]->login)
            ->see($referrals[1]->login);
    }

    /** @test */
    public function user_can_see_its_reinvests_in_the_desks()
    {
        $sponsor = $this->createSponsor()->giveReinvests(1);

        $reinvest = $sponsor->reinvests->first();

        $desk = Desk::createForLevel(1)->addMember($reinvest);

        $members = $this->addMembersTo($desk, 2);

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-data')
            ->click('Стол 1')
            ->see($reinvest->login)
            ->see($members[0]->login)
            ->see($members[1]->login);
    }

    /** @test */
    public function user_can_filter_reinvests_by_tree()
    {
        $sponsor = $this->createSponsor()->giveReinvests(2);

        $reinvestFromTree = $sponsor->reinvests[0];
        $reinvestFromDesk = $sponsor->reinvests[1];

        Desk::createForLevel(1)->addMember($reinvestFromDesk);

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-data?desk=tree')
            ->see($reinvestFromTree->login)
            ->dontSee($reinvestFromDesk->login);
    }

    /** @test */
    public function user_can_filter_reinvests_by_desk_level_2()
    {
        $sponsor = $this->createSponsor()->giveReinvests(2);

        $reinvestFromTree = $sponsor->reinvests[0];
        $reinvestFromDesk = $sponsor->reinvests[1];

        // Desk level is TWO
        Desk::createForLevel(2)->addMember($reinvestFromDesk);

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-data')

            // When we choose the level ONE, user mustn't see
            // both reinvests because one of them is in the
            // tree, and second is on the level 2.
            ->click('Стол 1')
            ->see('У вас нет реинвестов на данном уровне')

            // When we choose the level TWO, user must see the
            // second reinvest.
            ->click('Стол 2')
            ->dontSee($reinvestFromTree->login)
            ->see($reinvestFromDesk->login);
    }

    /** @test */
    public function user_can_filter_reinvests_by_desk_level_3()
    {
        $sponsor = $this->createSponsor()->giveReinvests(2);

        $reinvestFromTree = $sponsor->reinvests[0];
        $reinvestFromDesk = $sponsor->reinvests[1];

        Desk::createForLevel(3)->addMember($reinvestFromDesk);

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-data')

            ->click('Стол 1')
            ->see('У вас нет реинвестов на данном уровне')

            ->click('Стол 2')
            ->see('У вас нет реинвестов на данном уровне')

            ->click('Стол 3')
            ->dontSee($reinvestFromTree->login)
            ->see($reinvestFromDesk->login);
    }

    /** @test */
    public function user_can_filter_reinvests_by_desk_level_4()
    {
        $sponsor = $this->createSponsor()->giveReinvests(2);

        $reinvestFromTree = $sponsor->reinvests[0];
        $reinvestFromDesk = $sponsor->reinvests[1];

        Desk::createForLevel(4)->addMember($reinvestFromDesk);

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-data')

            ->click('Стол 1')
            ->see('У вас нет реинвестов на данном уровне')

            ->click('Стол 2')
            ->see('У вас нет реинвестов на данном уровне')

            ->click('Стол 3')
            ->see('У вас нет реинвестов на данном уровне')

            ->click('Стол 4')
            ->dontSee($reinvestFromTree->login)
            ->see($reinvestFromDesk->login);
    }

    /** @test */
    public function user_can_filter_reinvests_by_desk_level_5()
    {
        $sponsor = $this->createSponsor()->giveReinvests(2);

        $reinvestFromTree = $sponsor->reinvests[0];
        $reinvestFromDesk = $sponsor->reinvests[1];

        Desk::createForLevel(5)->addMember($reinvestFromDesk);

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-data')

            ->click('Стол 1')
            ->see('У вас нет реинвестов на данном уровне')

            ->click('Стол 2')
            ->see('У вас нет реинвестов на данном уровне')

            ->click('Стол 3')
            ->see('У вас нет реинвестов на данном уровне')

            ->click('Стол 4')
            ->see('У вас нет реинвестов на данном уровне')

            ->click('Стол 5')
            ->dontSee($reinvestFromTree->login)
            ->see($reinvestFromDesk->login);
    }

    /** @test */
    public function user_can_filter_reinvests_by_desk_level_6()
    {
        $sponsor = $this->createSponsor()->giveReinvests(2);

        $reinvestFromTree = $sponsor->reinvests[0];
        $reinvestFromDesk = $sponsor->reinvests[1];
        $reinvestFromDesk->goToNextTurn();

        Desk::createForLevel(6)->addMember($reinvestFromDesk);

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-data')

            ->click('Стол 1')
            ->see('У вас нет реинвестов на данном уровне')

            ->click('Стол 2')
            ->see('У вас нет реинвестов на данном уровне')

            ->click('Стол 3')
            ->see('У вас нет реинвестов на данном уровне')

            ->click('Стол 4')
            ->see('У вас нет реинвестов на данном уровне')

            ->click('Стол 5')
            ->see('У вас нет реинвестов на данном уровне')

            ->click('Стол 6')
            ->dontSee($reinvestFromTree->login)
            ->see($reinvestFromDesk->login);
    }

    /** @test */
    public function user_can_paginate_through_its_reinvests()
    {
        config([
           'profile.reinvests_per_page' => 2,
        ]);

        $sponsor = $this->createSponsor()->giveReinvests(3);

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-data')
            ->assertElementsCount('.b-reinvests .b-matrix', 2)
            ->click('page-2')
            ->assertElementsCount('.b-reinvests .b-matrix', 1);
    }

    /** @test */
    public function pagination_works_with_the_filters()
    {
        config([
            'profile.reinvests_per_page' => 2,
        ]);

        $sponsor = $this->createSponsor()->giveReinvests(4);

        // Now 3 reinvests in the tree and 1 in the desks.
        Desk::createForLevel(1)->addMember($sponsor->reinvests->first());

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-data')
            ->click('page-2')

            // If there are 3 reinvests in the tree and we look
            // at the second page we should see only one reinvest.
            ->assertElementsCount('.b-reinvests .b-matrix', 1);
    }

    /** @test */
    public function pagination_is_dropped_when_user_changes_the_filter()
    {
        config([
            'profile.reinvests_per_page' => 2,
        ]);

        $sponsor = $this->createSponsor()->giveReinvests(3);

        $this
            ->actingAs($sponsor)
            ->visit('account/referral-data')
            ->click('page-2')
            ->click('Стол 1')
            ->click('Дерево')

            // After changing the filters we must be on the first
            // page with two reinvests.
            ->assertElementsCount('.b-reinvests .b-matrix', 2);
    }

    /** @test */
    public function user_can_see_only_desk_mates_that_are_below_him()
    {
        $users = $this->addMembersTo(Desk::createForLevel(1), 6);

        /*
         * If we login as `$users[0]` we will see this pyramid:
         *
         * ```
         *                  $users[0]
         *       $users[1]       |      $users[2]
         * $users[3]   $users[4] | $users[5]   <Free Place>
         * ```
         *
         * But if we login as `$users[1]`, he must be rendered at
         * the top of the pyramid and see only `$users[3]` and
         * `$users[4]`, like this:
         *
         * ```
         *                        $users[1]
         *         $users[3]          |         $users[4]
         * <Free Place>  <Free Place> | <Free Place>  <Free Place>
         * ```
         */

        $this
            ->actingAs($users[1])
            ->visit('account/referral-data')
            ->dontSee($users[0]->login)
            ->dontSee($users[2]->login)
            ->dontSee($users[5]->login)
            ->see($users[3]->login)
            ->see($users[4]->login);
    }
}