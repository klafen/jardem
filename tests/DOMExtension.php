<?php

namespace Tests;

trait DOMExtension
{
    /**
     * Verify the number of DOM elements.
     *
     * @param string $selector The DOM selector (jquery style)
     * @param int $number How many elements should be present in the DOM
     * @return $this
     */
    public function assertElementsCount($selector, $number)
    {
        /** @var \Tests\TestCase $this */

        $this->assertCount($number, $this->crawler()->filter($selector));

        return $this;
    }
}