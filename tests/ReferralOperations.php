<?php

namespace Tests;

use App\Depozit\Desk;
use App\User;

trait ReferralOperations {

    /**
     * @return User
     */
    private function createSponsor()
    {
        $user = $this->createUserIfNotExists('sponsor');

        $user->markAsPaid();

        return $user;
    }

    /**
     * @param $sponsor
     * @param $count
     * @param int $start
     * @return array
     */
    private function createReferralsFor($sponsor, $count, $start = 1)
    {
        $results = [];

        for($i = 0; $i < $count; $i++) {
            $results[] = $this->createReferralFor($sponsor, 'referral' . ($start + $i));
        }

        return $results;
    }

    /**
     * @param $sponsor
     * @param $referralLogin
     * @return User
     */
    private function createReferralFor($sponsor, $referralLogin)
    {
        return $this->createUserInvitedBy($sponsor, $referralLogin)->markAsPaid();
    }

    /**
     * Creates a list of users like if they all have registered using
     * the login of the given sponsor.
     *
     * @param User $sponsor
     * @param int $count
     * @param int $start
     * @return array
     */
    private function createUsersInvitedBy($sponsor, $count, $start = 1)
    {
        $results = [];

        for($i = 0; $i < $count; $i++) {
            $results[] = $this->createUserInvitedBy($sponsor, 'referral' . ($start + $i));
        }

        return $results;
    }

    /**
     * Creates a user like if he has registered using the login of
     * the given sponsor.
     *
     * @param User $sponsor
     * @param string $invitedUserLogin
     * @return mixed
     */
    private function createUserInvitedBy($sponsor, $invitedUserLogin)
    {
        return $this->createUserIfNotExists($invitedUserLogin, [
            'desired_sponsor_id' => $sponsor->id,
        ]);
    }

    private function addMembersTo(Desk $desk, $count, $extraValues = [])
    {
        $users = $this->createUsers($count, $extraValues);

        foreach($users as $user) {
            /** @var User $user */

            $desk->addMember($user);
        }

        return $users;
    }

    /**
     * @param $sponsor User
     * @param $expectedSchema array
     */
    private function assertSchema(User $sponsor, $expectedSchema)
    {
        $this->assertEquals($expectedSchema, $this->buildSchema($sponsor));
    }

    /**
     * @param User $user
     * @return array
     */
    private function buildSchema(User $user)
    {
        $referrals = [];

        foreach($user->referrals as $referral) {
            $referrals += $this->buildSchema($referral);
        }

        return [ $user->login => $referrals ];
    }
}