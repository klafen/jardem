<?php

namespace Tests\Depozit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ReferralOperations;
use Tests\TestCase;

class TripletTest extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;
    use ReferralOperations;

    /** @test */
    public function user_can_have_referrals()
    {
        $sponsor = $this->createSponsor();

        $this->createReferralFor($sponsor, 'referral1');
        $this->createReferralFor($sponsor, 'referral2');
        $this->createReferralFor($sponsor, 'referral3');

        $this->assertSchema($sponsor, [
            'sponsor' => [
                'referral1' => [],
                'referral2' => [],
                'referral3' => [],
            ]
        ]);
    }

    /** @test */
    public function user_can_not_have_more_than_three_direct_referrals()
    {
        $sponsor = $this->createSponsor();

        $this->createReferralsFor($sponsor, 4);

        $this->assertSchema($sponsor, [
            'sponsor' => [
                'referral1' => [
                    'referral4' => [],
                ],
                'referral2' => [],
                'referral3' => [],
            ]
        ]);
    }

    /** @test */
    public function referral_searches_the_first_available_sponsor()
    {
        $sponsor = $this->createSponsor();

        $this->createReferralsFor($sponsor, 7);

        $this->assertSchema($sponsor, [
            'sponsor' => [
                'referral1' => [
                    'referral4' => [],
                    'referral5' => [],
                    'referral6' => [],
                ],

                'referral2' => [
                    'referral7' => [],
                ],

                'referral3' => [],
            ]
        ]);
    }

    /** @test */
    public function referral_can_find_a_sponsor_on_the_third_level()
    {
        $sponsor = $this->createSponsor();

        $this->createReferralsFor($sponsor, 13);

        $this->assertSchema($sponsor, [
            'sponsor' => [
                'referral1' => [
                    'referral4' => [
                        'referral13' => [],
                    ],
                    'referral5' => [],
                    'referral6' => [],
                ],

                'referral2' => [
                    'referral7' => [],
                    'referral8' => [],
                    'referral9' => [],
                ],

                'referral3' => [
                    'referral10' => [],
                    'referral11' => [],
                    'referral12' => [],
                ],
            ]
        ]);
    }

    /** @test */
    public function referral_can_start_search_from_any_branch()
    {
        $sponsor = $this->createSponsor();

        $referrals = $this->createReferralsFor($sponsor, 3);

        $this->createReferralFor($referrals[1], 'referral4');
        $this->createReferralFor($referrals[0], 'referral5');

        $this->assertSchema($sponsor, [
            'sponsor' => [
                'referral1' => [
                    'referral5' => [],
                ],

                'referral2' => [
                    'referral4' => [],
                ],

                'referral3' => [],
            ]
        ]);
    }

    /** @test */
    public function referral_correctly_determines_the_sponsors_order()
    {
        $sponsor = $this->createSponsor();

        $referrals = $this->createReferralsFor($sponsor, 3);

        $this->createReferralFor($referrals[1], 'referral4');
        $this->createReferralFor($referrals[0], 'referral5');

        $this->createReferralsFor($sponsor, 8, 6);

        $this->assertSchema($sponsor, [
            'sponsor' => [
                'referral1' => [
                    'referral5' => [
                        'referral13' => [],
                    ],
                    'referral6' => [],
                    'referral7' => [],
                ],

                'referral2' => [
                    'referral4' => [],
                    'referral8' => [],
                    'referral9' => [],
                ],

                'referral3' => [
                    'referral10' => [],
                    'referral11' => [],
                    'referral12' => [],
                ],
            ]
        ]);
    }

    /** @test */
    public function referrals_considers_only_sponsors_from_the_given_branch()
    {
        $sponsor = $this->createSponsor();

        $referral1 = $this->createReferralFor($sponsor, 'referral1');

        $this->createReferralsFor($referral1, 12, 2);

        $this->assertSchema($sponsor, [
            'sponsor' => [
                'referral1' => [
                    'referral2' => [
                        'referral5' => [],
                        'referral6' => [],
                        'referral7' => [],
                    ],

                    'referral3' => [
                        'referral8' => [],
                        'referral9' => [],
                        'referral10' => [],
                    ],

                    'referral4' => [
                        'referral11' => [],
                        'referral12' => [],
                        'referral13' => [],
                    ],
                ],
            ]
        ]);
    }
}
