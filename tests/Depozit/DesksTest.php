<?php

namespace Tests\Depozit;

use App\Depozit\Desk;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ReferralOperations;
use Tests\TestCase;

class DesksTest extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;
    use ReferralOperations;

    /** @test */
    public function user_goes_to_the_first_level_desk_when_his_triplet_is_full()
    {
        $sponsor = $this->createSponsor();

        $this->createReferralsFor($sponsor, 3);

        $this->assertDeskLevelUsers(1, [
            'sponsor',
        ]);
    }

//    /** @test */
//    public function desk_position_correct_winner()
//    {
//        $this->fail('');
//    }
//
//    /** @test */
//    public function desk_position_correct_split()
//    {
//        $this->fail('');
//    }
//
//    /** @test */
//    public function from_desk_sorting()
//    {
//        $this->fail('');
//    }

    /** @test */
    public function desk_does_not_fall_into_natural_order()
    {
        $users = $this->createUsers(7);

        $desk = Desk::createForLevel(1);

        $desk->addMember($users[6]);
        $desk->addMember($users[0]);
        $desk->addMember($users[1]);
        $desk->addMember($users[2]);
        $desk->addMember($users[3]);
        $desk->addMember($users[4]);
        $desk->addMember($users[5]);

        $this->assertEquals(2, $users[6]->fresh()->desk->level);
    }

    /** @test */
    public function user_knows_that_he_is_a_member()
    {
        $user = $this->createUserIfNotExists('user');

        $this->assertFalse($user->isMember());

        $user->moveToDesks();

        $this->assertTrue($user->isMember());
    }

    /** @test */
    public function user_knows_its_desk_mates()
    {
        foreach($this->createUsers(6) as $user) {
            /** @var User $user */

            $user->moveToDesks();
        }

        $this->assertDeskMates('user1', [
            'user2',
            'user3',
            'user4',
            'user5',
            'user6',
        ]);
    }

//    /** @test */
//    public function desk_mates_relation_uses_desk_position_for_ordering()
//    {
//        $this->fail('');
//    }
//
//    /** @test */
//    public function desk_mates_relation_uses_desk_position_for_calculating()
//    {
//        $this->fail('');
//    }

    /** @test */
    public function first_member_goes_to_the_next_desk_when_the_first_is_completed()
    {
        foreach($this->createUsers(7) as $user) {
            /** @var User $user */

            $user->moveToDesks();
        }

        // The first user goes to the level two desk, because
        // the first is filled
        $this->assertDeskLevelUsers(2, [
            'user1',
        ]);

        // And the other users stay at the level one desk
        $this->assertDeskLevelUsers(1, [
            'user2',
            'user3',
            'user4',
            'user5',
            'user6',
            'user7',
        ]);
    }

    /** @test */
    public function desk_is_split_when_it_is_completed()
    {
        foreach($this->createUsers(7) as $user) {
            /** @var User $user */

            $user->moveToDesks();
        }

        $this->assertDeskMates('user2', ['user4', 'user5']);
        $this->assertDeskMates('user3', ['user6', 'user7']);
    }

    /** @test */
    public function when_desks_are_split_next_users_use_them_in_correct_order()
    {
        foreach($this->createUsers(10) as $user) {
            /** @var User $user */

            $user->moveToDesks();
        }

        $this->assertDeskMates('user2', [
            'user4',
            'user5',
            'user8',
            'user9',
            'user10',
        ]);
    }

    private function assertDeskLevelUsers($level, $expectedUsers)
    {
        $users = User::fromDesk($level)->get()->pluck('login')->values()->toArray();

        $this->assertEquals($expectedUsers, $users);
    }

    private function assertDeskMates($login, $expectedMates)
    {
        $mates = User::byLogin($login)->deskMates->pluck('login')->toArray();

        $this->assertEquals($expectedMates, $mates);
    }
}
