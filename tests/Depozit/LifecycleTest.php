<?php

namespace Tests\Depozit;

use App\Depozit\Desk;
use App\Depozit\IncorrectTurnException;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ReferralOperations;
use Tests\TestCase;

class LifecycleTest extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;
    use ReferralOperations;

    private static $REWARD_FOR_REFERRAL = 500;
    private static $REWARD_FOR_LEVEL_1_COMPLETED = 1000;
    private static $REWARD_FOR_LEVEL_2_COMPLETED = 2000;
    private static $REWARD_FOR_LEVEL_3_COMPLETED = 3000;
    private static $REWARD_FOR_LEVEL_4_COMPLETED_1_TURN = 4000;
    private static $REWARD_FOR_LEVEL_5_COMPLETED_1_TURN = 5000;

    private static $REWARD_FOR_LEVEL_4_COMPLETED_2_TURN = 6000;
    private static $REWARD_FOR_LEVEL_5_COMPLETED_2_TURN = 7000;
    private static $REWARD_FOR_LEVEL_6_COMPLETED = 8000;

    private static $REWARD_FOR_LEVEL_4_COMPLETED_3_TURN = 9000;
    private static $REWARD_FOR_LEVEL_5_COMPLETED_3_TURN = 10000;

    protected function setUp()
    {
        parent::setUp();

        config([
            'money.REWARD_FOR_REFERRAL' => static::$REWARD_FOR_REFERRAL,
            'money.REWARD_FOR_LEVEL_1_COMPLETED' => static::$REWARD_FOR_LEVEL_1_COMPLETED,
            'money.REWARD_FOR_LEVEL_2_COMPLETED' => static::$REWARD_FOR_LEVEL_2_COMPLETED,
            'money.REWARD_FOR_LEVEL_3_COMPLETED' => static::$REWARD_FOR_LEVEL_3_COMPLETED,
            'money.REWARD_FOR_LEVEL_4_COMPLETED_1_TURN' => static::$REWARD_FOR_LEVEL_4_COMPLETED_1_TURN,
            'money.REWARD_FOR_LEVEL_5_COMPLETED_1_TURN' => static::$REWARD_FOR_LEVEL_5_COMPLETED_1_TURN,

            'money.REWARD_FOR_LEVEL_4_COMPLETED_2_TURN' => static::$REWARD_FOR_LEVEL_4_COMPLETED_2_TURN,
            'money.REWARD_FOR_LEVEL_5_COMPLETED_2_TURN' => static::$REWARD_FOR_LEVEL_5_COMPLETED_2_TURN,
            'money.REWARD_FOR_LEVEL_6_COMPLETED' => static::$REWARD_FOR_LEVEL_6_COMPLETED,

            'money.REWARD_FOR_LEVEL_4_COMPLETED_3_TURN' => static::$REWARD_FOR_LEVEL_4_COMPLETED_3_TURN,
            'money.REWARD_FOR_LEVEL_5_COMPLETED_3_TURN' => static::$REWARD_FOR_LEVEL_5_COMPLETED_3_TURN,
        ]);
    }

    /** @test */
    public function sponsor_gets_money_for_every_new_direct_referral()
    {
        $sponsor = $this->createSponsor();

        $this->createReferralsFor($sponsor, 3);

        $this->assertEquals(static::$REWARD_FOR_REFERRAL * 3, $sponsor->totalMoney());
    }

    /** @test */
    public function member_gets_money_when_he_finishes_the_first_level()
    {
        $desk = Desk::createForLevel(1);

        $this->addMembersTo($desk, 7);

        $this->assertEquals(static::$REWARD_FOR_LEVEL_1_COMPLETED, User::byLogin('user1')->totalMoney());
    }

    /** @test */
    public function member_gets_money_when_he_finishes_the_second_level()
    {
        $desk = Desk::createForLevel(2);

        $this->addMembersTo($desk, 7);

        $this->assertEquals(static::$REWARD_FOR_LEVEL_2_COMPLETED, User::byLogin('user1')->totalMoney());
    }

    /** @test */
    public function member_gets_money_when_he_finishes_the_third_level()
    {
        $desk = Desk::createForLevel(3);

        $this->addMembersTo($desk, 7);

        $this->assertEquals(static::$REWARD_FOR_LEVEL_3_COMPLETED, User::byLogin('user1')->totalMoney());
    }

    /** @test */
    public function member_gets_money_when_he_finishes_the_fourth_level_on_the_first_turn()
    {
        $desk = Desk::createForLevel(4);

        $this->addMembersTo($desk, 7);

        $this->assertEquals(static::$REWARD_FOR_LEVEL_4_COMPLETED_1_TURN, User::byLogin('user1')->totalMoney());
    }

    /** @test */
    public function member_goes_to_the_second_turn_after_the_fifth_level_on_the_first_turn()
    {
        $desk = Desk::createForLevel(5);

        $this->addMembersTo($desk, 7);

        $this->assertEquals(2, User::byLogin('user1')->turn);
        $this->assertEquals(4, User::byLogin('user1')->desk->level);
    }

    /** @test */
    public function member_gets_3_reinvests_after_the_fifth_level_on_the_first_turn()
    {
        $desk = Desk::createForLevel(5);

        $this->addMembersTo($desk, 7);

        $this->assertCount(3, User::byLogin('user1')->reinvests);
    }

    /** @test */
    public function member_gets_money_when_he_finishes_the_fifth_level_on_the_first_turn()
    {
        $desk = Desk::createForLevel(5);

        $this->addMembersTo($desk, 7);

        $this->assertEquals(static::$REWARD_FOR_LEVEL_5_COMPLETED_1_TURN, User::byLogin('user1')->totalMoney());
    }

    /** @test */
    public function member_gets_money_when_he_finishes_the_fourth_level_on_the_second_turn()
    {
        $desk = Desk::createForLevel(4);

        $this->addMembersTo($desk, 7, ['turn' => 2]);

        $this->assertEquals(static::$REWARD_FOR_LEVEL_4_COMPLETED_2_TURN, User::byLogin('user1')->totalMoney());
    }

    /** @test */
    public function member_goes_to_the_sixth_level_when_he_finishes_the_fifth_on_the_second_turn()
    {
        $desk = Desk::createForLevel(5);

        $this->addMembersTo($desk, 7, ['turn' => 2]);

        $this->assertEquals(6, User::byLogin('user1')->desk->level);
    }

    /** @test */
    public function member_gets_money_when_he_finishes_the_fifth_level_on_the_second_turn()
    {
        $desk = Desk::createForLevel(5);

        $this->addMembersTo($desk, 7, ['turn' => 2]);

        $this->assertEquals(static::$REWARD_FOR_LEVEL_5_COMPLETED_2_TURN, User::byLogin('user1')->totalMoney());
    }

    /** @test */
    public function member_gets_3_reinvests_after_the_fifth_level_on_the_second_turn()
    {
        $desk = Desk::createForLevel(5);

        $this->addMembersTo($desk, 7, ['turn' => 2]);

        $this->assertCount(3, User::byLogin('user1')->reinvests);
    }

    /** @test */
    public function its_impossible_to_add_a_first_turn_member_to_the_sixth_level()
    {
        $this->expectException(IncorrectTurnException::class);

        $desk = Desk::createForLevel(6);

        $this->addMembersTo($desk, 1, ['turn' => 1]);
    }

    /** @test */
    public function member_goes_to_the_third_turn_after_the_sixth_level()
    {
        $desk = Desk::createForLevel(6);

        $this->addMembersTo($desk, 7, ['turn' => 2]);

        $this->assertEquals(3, User::byLogin('user1')->turn);
        $this->assertEquals(4, User::byLogin('user1')->desk->level);
    }

    /** @test */
    public function member_gets_money_when_he_finishes_the_sixth_level()
    {
        $desk = Desk::createForLevel(6);

        $this->addMembersTo($desk, 7, ['turn' => 2]);

        $this->assertEquals(static::$REWARD_FOR_LEVEL_6_COMPLETED, User::byLogin('user1')->totalMoney());
    }

    /** @test */
    public function member_gets_money_when_he_finishes_the_fourth_level_on_the_third_turn()
    {
        $desk = Desk::createForLevel(4);

        $this->addMembersTo($desk, 7, ['turn' => 3]);

        $this->assertEquals(static::$REWARD_FOR_LEVEL_4_COMPLETED_3_TURN, User::byLogin('user1')->totalMoney());
    }

    /** @test */
    public function member_goes_to_the_fourth_level_after_the_fifth_level_on_the_third_turn()
    {
        $desk = Desk::createForLevel(5);

        $this->addMembersTo($desk, 7, ['turn' => 3]);

        // The `turn` must not change
        $this->assertEquals(3, User::byLogin('user1')->turn);
        $this->assertEquals(4, User::byLogin('user1')->desk->level);
    }

    /** @test */
    public function member_gets_money_when_he_finishes_the_fifth_level_on_the_third_turn()
    {
        $desk = Desk::createForLevel(5);

        $this->addMembersTo($desk, 7, ['turn' => 3]);

        $this->assertEquals(static::$REWARD_FOR_LEVEL_5_COMPLETED_3_TURN, User::byLogin('user1')->totalMoney());
    }

    /** @test */
    public function member_gets_3_reinvests_after_the_fifth_level_on_the_third_turn()
    {
        $desk = Desk::createForLevel(5);

        $this->addMembersTo($desk, 7, ['turn' => 3]);

        $this->assertCount(3, User::byLogin('user1')->reinvests);
    }
}
