<?php

namespace Tests\Depozit;

use App\Depozit\UnpaidUserCanNotFollowException;
use App\Depozit\UserIsAlreadyPaidException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ReferralOperations;
use Tests\TestCase;

class PaymentsTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    use ReferralOperations;

    /** @test */
    public function it_is_impossible_to_mark_user_as_paid_twice()
    {
        $this->expectException(UserIsAlreadyPaidException::class);

        $referral = $this->createUserIfNotExists('referral');

        $referral->markAsPaid();
        $referral->markAsPaid();
    }

    /** @test */
    public function only_paid_user_can_follow_someone()
    {
        $this->expectException(UnpaidUserCanNotFollowException::class);

        $sponsor = $this->createSponsor();
        $referral = $this->createUserIfNotExists('referral');

        $referral->follow($sponsor);
    }
}