<?php

namespace Tests\Depozit;

use App\Depozit\Desk;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ReferralOperations;
use Tests\TestCase;

class ReinvestTest extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;
    use ReferralOperations;

    /** @test */
    public function reinvest_fields_are_automatically_filled()
    {
        $reinvest = $this->createUserIfNotExists('user')->makeReinvest();

        $this->assertEquals(true, $reinvest->is_reinvest);
        $this->assertEquals('User Name User Surname', $reinvest->first_name);
        $this->assertEquals('Reinvest #1', $reinvest->last_name);
        $this->assertEquals('user.reinvest1', $reinvest->login);
        $this->assertEquals('user.reinvest1@example.com', $reinvest->email);
        $this->assertNotEmpty($reinvest->password);
    }

    /** @test */
    public function reinvest_knows_who_is_its_owner()
    {
        $user = $this->createUserIfNotExists('user');

        $reinvest = $user->makeReinvest();

        $this->assertEquals($user->id, $reinvest->owner->id);
    }

    /** @test */
    public function owner_knows_its_reinvests()
    {
        $user = $this->createUserIfNotExists('user');

        $reinvest1 = $user->makeReinvest();
        $reinvest2 = $user->makeReinvest();
        $reinvest3 = $user->makeReinvest();

        $this->assertEquals(
            [
                $reinvest1->id,
                $reinvest2->id,
                $reinvest3->id,
            ],
            $user->reinvests()->get()->pluck('id')->toArray()
        );
    }

    /** @test */
    public function its_impossible_to_login_as_reinvest_even_if_you_know_its_password()
    {
        $reinvest = $this->createUserIfNotExists('user')->makeReinvest();
        $reinvest->password = bcrypt('secret');
        $reinvest->save();

        $this
            ->visit('/login')
            ->type($reinvest->login, 'login')
            ->type('secret', 'password')
            ->press('Войти')
            ->dontSeeIsAuthenticated();
    }

    /** @test */
    public function reinvest_transfers_received_money_to_the_owner()
    {
        $user = $this->createUserIfNotExists('user');

        $reinvest = $user->makeReinvest()->giveMoney(500);

        $this->assertEquals(0, $reinvest->totalMoney());
        $this->assertEquals(500, $user->totalMoney());
    }

    /** @test */
    public function reinvest_transfers_new_reinvests_to_the_owner()
    {
        $user = $this->createUserIfNotExists('user');

        $reinvest = $user->makeReinvest()->giveMoney(500);

        $reinvest->giveReinvests(3);

        $this->assertCount(4, $user->reinvests);
    }

    /** @test */
    public function reinvest_is_automatically_gets_into_the_tree()
    {
        $reinvest = $this->createUserIfNotExists('user')->makeReinvest();

        $this->assertTrue($reinvest->sponsor->id == User::root()->id);
        $this->assertTrue($reinvest->hasPaid());
    }

    /** @test */
    public function sponsor_is_not_rewarded_for_reinvested_referrals()
    {
        $reinvest = $this->createUserIfNotExists('user')->makeReinvest();

        $this->assertEquals(0, $reinvest->sponsor->totalMoney());
    }

//    /** @test */
//    public function user_must_meet_the_requirements_to_make_a_reinvest()
//    {
//        $this->fail('');
//    }
//
    /** @test */
    public function reinvest_is_deactivated_after_the_fifth_level()
    {
        $reinvest = $this->createUserIfNotExists('user')->makeReinvest();

        $desk = Desk::createForLevel(5);

        $desk->addMember($reinvest);

        $this->addMembersTo($desk, 6);

        // Reinvest must be removed from the desks.
        // It's what is meant by `deactivated`.
        $this->assertFalse($reinvest->fresh()->isMember());
    }

}
