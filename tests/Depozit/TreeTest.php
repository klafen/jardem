<?php

namespace Tests\Depozit;

use App\Depozit\Tree;
use App\User;
use Tests\TestCase;

class TreeTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        // To make the `User` class accept `id` on instantiation
        \Eloquent::unguard();
    }

    public function tearDown()
    {
        parent::tearDown();

        \Eloquent::reguard();
    }

    /** @test */
    public function it_is_instantiable()
    {
        new Tree(new User, collect([]));
    }

    /** @test */
    public function first_available_returns_the_user_itself_for_a_single_list_collection()
    {
        $rootUser = new User(['id' => 1]);

        $tree = new Tree($rootUser, collect([]));

        $this->assertEquals($rootUser, $tree->firstAvailable());
    }

    /** @test */
    public function first_available_returns_the_user_itself_if_his_triple_is_not_full()
    {
        $rootUser = new User(['id' => 1]);

        $collection = collect([
           new User(['id' => 2, 'sponsor_id' => 1]),
           new User(['id' => 3, 'sponsor_id' => 1]),
        ]);

        $tree = new Tree($rootUser, $collection);

        $this->assertEquals($rootUser, $tree->firstAvailable());
    }

    /** @test */
    public function first_available_returns_the_second_user_if_the_triplet_is_full()
    {
        $rootUser = new User(['id' => 1]);

        $collection = collect([
            $expected = new User(['id' => 2, 'sponsor_id' => 1]),
            new User(['id' => 3, 'sponsor_id' => 1]),
            new User(['id' => 4, 'sponsor_id' => 1]),
        ]);

        $tree = new Tree($rootUser, $collection);

        $this->assertEquals($expected, $tree->firstAvailable());
    }

    /** @test */
    public function first_available_returns_the_third_user_if_the_second_triplet_is_full()
    {
        $rootUser = new User(['id' => 1]);

        $collection = collect([
            new User(['id' => 2, 'sponsor_id' => 1]),
            new User(['id' => 5, 'sponsor_id' => 2]),
            new User(['id' => 6, 'sponsor_id' => 2]),
            new User(['id' => 7, 'sponsor_id' => 2]),

            $expected = new User(['id' => 3, 'sponsor_id' => 1]),
            new User(['id' => 4, 'sponsor_id' => 1]),
        ]);

        $tree = new Tree($rootUser, $collection);

        $this->assertEquals($expected, $tree->firstAvailable());
    }

    /** @test */
    public function first_available_returns_the_fifth_user_if_the_second_level_is_full()
    {
        $rootUser = new User(['id' => 1]);

        $collection = collect([
            new User(['id' => 2, 'sponsor_id' => 1]),
            $expected = new User(['id' => 5, 'sponsor_id' => 2]),
            new User(['id' => 6, 'sponsor_id' => 2]),
            new User(['id' => 7, 'sponsor_id' => 2]),

            new User(['id' => 3, 'sponsor_id' => 1]),
            new User(['id' => 8, 'sponsor_id' => 3]),
            new User(['id' => 9, 'sponsor_id' => 3]),
            new User(['id' => 10, 'sponsor_id' => 3]),

            new User(['id' => 4, 'sponsor_id' => 1]),
            new User(['id' => 11, 'sponsor_id' => 4]),
            new User(['id' => 12, 'sponsor_id' => 4]),
            new User(['id' => 13, 'sponsor_id' => 4]),
        ]);

        $tree = new Tree($rootUser, $collection);

        $this->assertEquals($expected, $tree->firstAvailable());
    }

    /** @test */
    public function first_available_ignores_users_from_other_branches()
    {
        $rootUser = new User(['id' => 1]);

        $collection = collect([
            new User(['id' => 2, 'sponsor_id' => 5]),
            new User(['id' => 3, 'sponsor_id' => 5]),
            new User(['id' => 4, 'sponsor_id' => 5]),
        ]);

        $tree = new Tree($rootUser, $collection);

        $this->assertEquals($rootUser, $tree->firstAvailable());
    }
}
