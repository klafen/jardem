<?php

namespace Tests\Auth;

use Auth;
use App\Email\ResetPasswordNotification;
use DB;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Notification;
use Tests\TestCase;

class ResetPasswordTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    /**
     * @var \App\User
     */
    private $user = null;

    public function setUp()
    {
        parent::setUp();

        $this->user = $this->createUserIfNotExists('user');
    }

    /** @test */
    public function user_can_reset_its_password()
    {
        $this
            ->tryRequestReset()
            ->tryUpdatePassword();

        $this->assertTrue(
            Auth::attempt(['email' => $this->user->email, 'password' => '123456']),
            'User cannot authorize with the new password'
        );
    }

    /** @test */
    public function user_is_redirected_to_the_main_page_after_resetting()
    {
        $this
            ->tryRequestReset()
            ->tryUpdatePassword()
            ->seePageIs('/');
    }


    /** @test */
    public function user_is_authenticated_after_resetting()
    {
        $this
            ->tryRequestReset()
            ->tryUpdatePassword()
            ->seeIsAuthenticated();
    }

    /** @test */
    public function the_reset_password_mail_is_sent_when_user_resets_its_password()
    {
        $this->tryRequestReset();

        Notification::assertSentTo($this->user, ResetPasswordNotification::class);
    }

    /**
     * @return $this
     */
    private function tryRequestReset()
    {
        Notification::fake();

        return $this
            ->visit('/password/reset')
            ->type($this->user->email, 'email')
            ->press('Отправить');
    }

    /**
     * @return $this
     */
    private function tryUpdatePassword()
    {
        $reset = DB
            ::table('password_resets')
            ->where('email', $this->user->email)
            ->first();

        return $this
            ->visit("password/reset/{$reset->token}")
            ->type($this->user->email, 'email')
            ->type('123456', 'password')
            ->type('123456', 'password_confirmation')
            ->press('Установить новый пароль')
            ->seePageIs('/');
    }
}
