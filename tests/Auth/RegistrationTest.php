<?php

namespace Tests\Auth;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\RegistrationExtension;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;
    use RegistrationExtension;

    /** @test */
    public function users_password_saved_in_bcrypt_and_user_can_logged_in_via_his_password()
    {
        $this->registerUser();

        $this
            ->post('logout')
            ->visit('login')
            ->type('user', 'login')
            ->type('secret', 'password')
            ->press('Войти')
            ->seeIsAuthenticated();
    }

    /** @test */
    public function that_the_registration_adds_new_user_to_db()
    {
        $this
            ->registerUser()
            ->seeInDatabase('users', ['login' => 'user']);
    }

    /** @test */
    public function user_is_redirected_to_its_account_after_registration()
    {
        $this
            ->registerUser()
            ->seePageIs('/account/profile');
    }

    /** @test */
    public function that_the_registered_user_has_is_admin_0_by_default()
    {
        $this
            ->registerUser()
            ->assertFalse(User::byLogin('user')->isAdmin());
    }

    /** @test */
    public function that_the_registered_user_has_paid_0_by_default()
    {
        $this
            ->registerUser()
            ->assertFalse(User::byLogin('user')->hasPaid());
    }

    /** @test */
    public function that_the_user_cant_be_registered_if_he_is_under_the_age_of_18()
    {
        $this
            ->registerUser(['date_of_birth' => $this->underageBirthDate()])
            ->see('Регистрация доступна только лицам, достигшим 18 лет')
            ->dontSeeInDatabase('users', ['login' => 'user']);
    }

    /** @test */
    public function that_the_sponsor_existence_validation_is_ok()
    {
        $this
            ->registerUser(['sponsor_login' => 'non-existent-sponsor'])
            ->see('Логин спонсора введен неверно');
    }

    /** @test */
    public function that_the_sponsor_payment_validation_is_ok()
    {
        $this->createUserIfNotExists('sponsor');

        $this
            ->registerUser(['sponsor_login' => 'sponsor'])
            ->see('Спонсора с таким логином нет в нашей базе, либо он еще не оплатил вход. В таком случае оставьте поле пустым');
    }

    /** @test */
    public function check_output_entered_users_data_from_session_after_invalid_submitted_form()
    {
        $additionalValues = [
            'login' => 'user',
            'email' => 'user@example.com',
            'first_name' => 'John',
            'last_name' => 'Galt',
            'country' => 'Country',
            'city' => 'City',
            'password' => 'secret',
            'date_of_birth' => $this->adultBirthDate(),
        ];

        $this->registerUser($additionalValues);

        foreach ($additionalValues as $field => $value) {
            if ($field != 'password') {
                $this->see($value);
            }
        }
    }

    /** @test */
    public function check_latin_validation_for__login__first_name__last_name()
    {
        $this->registerUser([
            'login' => 'кириллица',
            'first_name' => 'кириллица',
            'last_name' => 'кириллица',
        ]);

        $this->see('Логин может содержать только латинские буквы, цифры и знак подчеркивания');
        $this->dontSee('Имя должно быть заполнено латиницей');
        $this->dontSee('Фамилия должна быть заполнена латиницей');
    }

    /** @test */
    public function user_can_use_a_login_with_letters_numbers_and_underscore()
    {
        $this->registerUser([
            'login' => 'test_1',
        ]);

        $this->dontSee('Логин может содержать только латинские буквы, цифры и знак подчеркивания');
    }

    /** @test */
    public function sponsor_login_is_automatically_filled_from_the_GET_param()
    {
        $this
            ->visit('register?ref=sponsor_login')
            ->see('sponsor_login');
    }

    /** @test */
    public function user_has_office_as_sponsor_if_he_left_sponsor_login_empty()
    {
        $officeId = User::root()->id;

        $this
            ->registerUser()
            ->seeInDatabase('users', ['login' => 'user', 'desired_sponsor_id' => $officeId]);
    }
}
