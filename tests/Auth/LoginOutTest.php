<?php

namespace Tests\Auth;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LoginOutTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    /** @test */
    public function that_the_user_can_be_logged_in_via_his_login_instead_of_email()
    {
        $this->createUserIfNotExists('user');

        $this
            ->visit('/login')
            ->type('user', 'login')
            ->type('user', 'password')
            ->press('Войти')
            ->seeIsAuthenticated();
    }

    /** @test */
    public function check_that_the_user_getting_message_about_failed_credentials()
    {
        $this
            ->visit('/login')
            ->type('user', 'login')
            ->type('user', 'password')
            ->press('Войти')
            ->see(trans('auth.failed'));
    }

    /** @test */
    public function that_the_logout_is_working()
    {
        $this->actingAsUser();

        // We cannot click on the `Logout` button like a real user
        // because it's placed in a dropdown activated by JS.
        $this->call('POST', '/logout', [
            '_token' => csrf_token(),
        ]);

        $this->dontSeeIsAuthenticated();
    }
}
