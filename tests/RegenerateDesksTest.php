<?php

use App\User;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Tests\TestCase;

class RegenerateDesksTest extends TestCase
{
    use InteractsWithDatabase;

    /** @test */
    public function generates_correct_tree()
    {
        shell_exec("
            mysql -uhomestead -psecret -e \"drop database if exists depositgold_test\"
            mysql -uhomestead -psecret -e \"create database depositgold_test\"
            mysql -uhomestead -psecret -e \"use depositgold_test; source ~/Code/depositgold/database/backups/depozitgold-2017-11-02.sql;\"
        ");

        $this->seed('RegenerateDesksSeeder');

        User::report();
    }
}
