<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Upload dir
    |--------------------------------------------------------------------------
    |
    | The dir where to store the images (relative from public)
    |
    */
    'dir' => ['uploads'],

    /*
    |--------------------------------------------------------------------------
    | Filesystem disks (Flysytem)
    |--------------------------------------------------------------------------
    |
    | Define an array of Filesystem disks, which use Flysystem.
    | You can set extra options, example:
    |
    | 'my-disk' => [
    |        'URL' => url('to/disk'),
    |        'alias' => 'Local storage',
    |    ]
    */
    'disks' => [
        // 'uploads',
    ],

    /*
    |--------------------------------------------------------------------------
    | Routes group config
    |--------------------------------------------------------------------------
    |
    | The default group settings for the elFinder routes.
    |
    */

    'route' => [

        // Value `config('backpack.base.route_prefix').'/elfinder'`
        // is set here by default. But for some reasons, the production
        // installation of PHP doesn't allow to cross-use configs and
        // the result value will be `/elfinder`, instead of the expected
        // `/admin/elfinder`.
        //
        // That's why file uploading in the WYSIWYG was broken:
        // https://cuteimpact.myjetbrains.com/youtrack/issue/DG-25
        //
        // But if we leave it empty Backpack will set it in run-time and
        // it fixes the problem:
        // https://github.com/Laravel-Backpack/CRUD/commit/8835b80788f90297a936f3593bc6f426a334b25d
        //
        // More details:
        // https://github.com/Laravel-Backpack/CRUD/issues/311
        'prefix'     => null,
        'middleware' => ['web', 'auth'], //Set to null to disable middleware filter
    ],

    /*
    |--------------------------------------------------------------------------
    | Access filter
    |--------------------------------------------------------------------------
    |
    | Filter callback to check the files
    |
    */

    'access' => 'Barryvdh\Elfinder\Elfinder::checkAccess',

    /*
    |--------------------------------------------------------------------------
    | Roots
    |--------------------------------------------------------------------------
    |
    | By default, the roots file is LocalFileSystem, with the above public dir.
    | If you want custom options, you can set your own roots below.
    |
    */

    'roots' => null,

    /*
    |--------------------------------------------------------------------------
    | Options
    |--------------------------------------------------------------------------
    |
    | These options are merged, together with 'roots' and passed to the Connector.
    | See https://github.com/Studio-42/elFinder/wiki/Connector-configuration-options-2.1
    |
    */

    'options' => [],

];
