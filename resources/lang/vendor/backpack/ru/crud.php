<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack Crud Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD interface.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    // Create form
    'add'                 => 'Добавить',
    'back_to_all'         => 'Вернуться',
    'cancel'              => 'Отмена',
    'add_a_new'           => 'Добавить новый ',

        // Create form - advanced options
        'after_saving'            => 'После сохранения',
        'go_to_the_table_view'    => 'перейти к таблице просмотра',
        'let_me_add_another_item' => 'добавить еще один элемент',
        'edit_the_new_item'       => 'редактировать новый элемент',

    // Edit form
    'edit'                 => 'Редактировать',
    'save'                 => 'Сохранить',

    // Revisions
    'revisions'            => 'Revisions',
    'no_revisions'         => 'No revisions found',
    'created_this'          => 'created this',
    'changed_the'          => 'changed the',
    'restore_this_value'   => 'Restore this value',
    'from'                 => 'from',
    'to'                   => 'to',
    'undo'                 => 'Undo',
    'revision_restored'    => 'Revision successfully restored',

    // CRUD table view
    'all'                       => 'Все ',
    'in_the_database'           => 'в базе данных',
    'list'                      => 'Список',
    'actions'                   => 'Действия',
    'preview'                   => 'Предпросмотр',
    'delete'                    => 'Удалить',
    'admin'                     => 'Администратор',
    'details_row'               => 'Детали записи. Можно редактировать',
    'details_row_loading_error' => 'Не удалось загрузить детали. Попробуйте повторить попытку.',

        // Confirmation messages and bubbles
        'delete_confirm'                              => 'Вы уверены, что хотите удалить этот элемент?',
        'delete_confirmation_title'                   => 'Элемент удален',
        'delete_confirmation_message'                 => 'Элемент был полностью удален',
        'delete_confirmation_not_title'               => 'Элемент не удален',
        'delete_confirmation_not_message'             => "Произошла ошибка. Возможно, не достаточно прав для удаления",
        'delete_confirmation_not_deleted_title'       => 'Элемент не удален',
        'delete_confirmation_not_deleted_message'     => 'Ничего не произошло. Изменения не сохранились.',

        // DataTables translation
        'emptyTable'     => 'Таблица Базы Данных еще не заполнена',
        'info'           => 'Показаны с _START_ по _END_ из _TOTAL_ записей',
        'infoEmpty'      => 'Показаны с 0 по 0 из 0 записей',
        'infoFiltered'   => '(Отсортировано из _MAX_ записей)',
        'infoPostFix'    => '',
        'thousands'      => ',',
        'lengthMenu'     => '_MENU_ записей на страницу',
        'loadingRecords' => 'Загрузка...',
        'processing'     => 'Обработка...',
        'search'         => 'Поиск: ',
        'zeroRecords'    => 'По запросу ничего не найдено',
        'paginate'       => [
            'first'    => 'Первая',
            'last'     => 'Последняя',
            'next'     => 'Следующая',
            'previous' => 'Предыдущая',
        ],
        'aria' => [
            'sortAscending'  => ': сортировка по возрастанию',
            'sortDescending' => ': сортировка по убыванию',
        ],

    // global crud - errors
    'unauthorized_access' => 'У вас недостаточно прав, чтобы просматривать эту страницу',
    'please_fix' => 'Пожалуйста, исправьте эти ошибки:',

    // global crud - success / error notification bubbles
    'insert_success' => 'Элемент успешно добавлен.',
    'update_success' => 'Элемент успешно изменен.',

    // CRUD reorder view
    'reorder'                      => 'Reorder',
    'reorder_text'                 => 'Use drag&drop to reorder.',
    'reorder_success_title'        => 'Done',
    'reorder_success_message'      => 'Your order has been saved.',
    'reorder_error_title'          => 'Error',
    'reorder_error_message'        => 'Your order has not been saved.',

    // CRUD yes/no
    'yes' => 'Да',
    'no' => 'Нет',

    // Fields
    'browse_uploads' => 'Просмотреть загруженные',
    'clear' => 'Очистить',
    'page_link' => 'Абсолютный адрес страницы',
    'page_link_placeholder' => 'http://example.com/your-desired-page',
    'internal_link' => 'Внутренний адрес страницы',
    'internal_link_placeholder' => 'Втнутренний адрес. Пример: \'admin/page\' (без кавычек) для \':url\'',
    'external_link' => 'Внешняя ссылка',
    'choose_file' => 'Выбрать файл',

    //Table field
    'table_cant_add' => 'Невозможно добавить :entity',
    'table_max_reached' => 'Достигнуто максимально допустимое значение в :max',

];
