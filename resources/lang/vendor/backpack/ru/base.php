<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack\Base Language Lines
    |--------------------------------------------------------------------------
    */

    'registration_closed'  => 'Регистрация окрыта.',
    'first_page_you_see'   => 'На эту страницу вы попадаете, если зашли как администратор.',
    'login_status'         => 'Статус входа',
    'logged_in'            => 'Вы вошли!',
    'toggle_navigation'    => 'Переключить навигацию',
    'administration'       => 'АДМИНИСТРИРОВАНИЕ',
    'user'                 => 'ПОЛЬЗОВАТЕЛЬ',
    'logout'               => 'Выйти',
    'login'                => 'Войти',
    'register'             => 'Зарегистрироваться',
    'name'                 => 'Имя',
    'email_address'        => 'E-Mail',
    'password'             => 'Пароль',
    'confirm_password'     => 'Подтверждение пароля',
    'remember_me'          => 'Запомнить меня',
    'forgot_your_password' => 'Забыли пароль?',
    'reset_password'       => 'Сбросить пароль',
    'send_reset_link'      => 'Послать запрос сброса пароля',
    'click_here_to_reset'  => 'Нажмите сюда, чтобы сбросить ваш пароль',
    'unauthorized'         => 'Не авторизованы.',
    'dashboard'            => 'Панель администрирования',
    'handcrafted_by'       => 'Handcrafted by',
    'powered_by'           => 'Powered by',
];
