<?php
use Illuminate\Support\Facades\Input;
$sponsor = Input::get('ref') ? : '';
$inviter = Input::get('ref') ? : '';
?>
@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="container">
            <h2 class="post-title">Регистрация</h2>

            <form method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}

                <div class="registration-form row">
                    <div class="registration-form__left-pane col-md-6 col-xs-12">
                        <div class="form-field-label">Пакет</div>
                        @if ($errors->has('login'))
                            <span class="form-help-error">
                                <strong>{{ $errors->first('tariff_id') }}</strong>
                            </span>
                        @endif

                        <select  class="form-field" id="tariff_id" name="tariff_id" required>
                            <option></option>
                            <option value="1" @if (old('tariff_id')=='1') selected="selected" @endif >25000 тнг (START)</option>
                            <option value="2" @if (old('tariff_id')=='2') selected="selected" @endif>75000 тнг (GOLD)</option>
                            <option value="3" @if (old('tariff_id')=='3') selected="selected" @endif>1000000 тнг (VIP)</option>
                        </select>


                        <div class="form-field-label">Логин</div>
                        @if ($errors->has('login'))
                            <span class="form-help-error">
                                <strong>{{ $errors->first('login') }}</strong>
                            </span>
                        @endif
                        <input
                                class="form-field"
                                id="login"
                                type="text"
                                placeholder="Логин *"
                                name="login"
                                value="{{ old('login') }}"
                                required
                                autofocus
                        />

                        <div class="form-field-label">Пароль</div>
                        @if ($errors->has('password'))
                            <span class="form-help-error">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <input
                                class="form-field"
                                id="password"
                                type="password"
                                placeholder="Пароль *"
                                name="password"
                                value="{{ old('password') }}"
                                required
                                autofocus
                        />

                        <div class="form-field-label">Подтверждение пароля</div>
                        <input
                                id="password_confirmation"
                                type="password"
                                placeholder="Подтверждение пароля *"
                                class="form-field"
                                name="password_confirmation"
                                required
                                autofocus
                        />

                        <div class="form-field-label">Логин спонсора</div>
                        @if ($errors->has('sponsor_login'))
                            <span class="form-help-error">
                                <strong>{{ $errors->first('sponsor_login') }}</strong>
                            </span>
                        @endif
                        <input
                                class="form-field"
                                id="sponsor_login"
                                type="text"
                                placeholder="Логин спонсора"
                                name="sponsor_login"
                                value="{{ old('sponsor_login') ? : $sponsor}}"
                                autofocus
                        />

                        <div class="form-field-label">E-mail</div>
                        @if ($errors->has('email'))
                            <span class="form-help-error">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <input
                                class="form-field"
                                id="email"
                                type="text"
                                placeholder="E-mail *"
                                name="email"
                                value="{{ old('email') }}"
                                required
                                autofocus
                        />
                        
                        <div class="form-field-label">Телефон</div>
                        @if ($errors->has('phone'))
                            <span class="form-help-error">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                        <input
                                class="form-field"
                                id="phone"
                                type="text"
                                placeholder="Телефон *"
                                name="phone"
                                value="{{ old('phone') }}"
                                required
                                autofocus
                        />
                        
                        <div class="form-field-label">Скайп</div>
                        @if ($errors->has('skype'))
                            <span class="form-help-error">
                                <strong>{{ $errors->first('skype') }}</strong>
                            </span>
                        @endif
                        <input
                                class="form-field"
                                id="skype"
                                type="text"
                                placeholder="Скайп"
                                name="skype"
                                value="{{ old('skype') }}"
                                autofocus
                        />
                    </div>

                    <div class="registration-form__right-pane col-md-6 col-xs-12">
                        <div class="form-field-label">Имя</div>
                        @if ($errors->has('first_name'))
                            <span class="form-help-error">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                        <input
                                class="form-field"
                                id="first_name"
                                type="text"
                                placeholder="Имя *"
                                name="first_name"
                                value="{{ old('first_name') }}"
                                required
                                autofocus
                        >

                        <div class="form-field-label">Фамилия</div>
                        @if ($errors->has('last_name'))
                            <span class="form-help-error">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                        <input
                                class="form-field"
                                id="last_name"
                                type="text"
                                placeholder="Фамилия *"
                                name="last_name"
                                value="{{ old('last_name') }}"
                                required
                                autofocus
                        />

                        <div class="form-field-label">Дата рождения</div>
                        @if ($errors->has('date_of_birth'))
                            <span class="form-help-error">
                                <strong>{{ $errors->first('date_of_birth') }}</strong>
                            </span>
                        @endif
                        <input
                                id="date_of_birth"
                                placeholder="Дата рождения *"
                                type="text"
                                onfocus="(this.type='date')"
                                class="form-field"
                                name="date_of_birth"
                                value="{{ old('date_of_birth') }}"
                                required
                                autofocus
                        />

                        <div class="form-field-label">Страна</div>
                        @if ($errors->has('country'))
                            <span class="form-help-error">
                                <strong>{{ $errors->first('country') }}</strong>
                            </span>
                        @endif
                        <input
                                class="form-field"
                                id="country"
                                type="text"
                                placeholder="Страна *"
                                name="country"
                                value="{{ old('country') }}"
                                required
                                autofocus
                        />


                        <div class="form-field-label">Город</div>
                        @if ($errors->has('city'))
                            <span class="form-help-error">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                        <input
                                class="form-field"
                                id="city"
                                type="text"
                                placeholder="Город *"
                                name="city"
                                value="{{ old('city') }}"
                                required
                                autofocus
                        />
                        
                        <div class="form-field-label">Пригласитель</div>
                        @if ($errors->has('inviter'))
                            <span class="form-help-error">
                                <strong>{{ $errors->first('inviter') }}</strong>
                            </span>
                        @endif
                        <input
                                class="form-field"
                                id="inviter"
                                type="text"
                                placeholder="Пригласитель"
                                name="inviter"
                                value="{{ old('inviter') ? : $inviter }}"
                                autofocus
                        />
                        
                        <div class="form-field-label">Домашний адрес</div>
                        @if ($errors->has('address'))
                            <span class="form-help-error">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                        <input
                                class="form-field"
                                id="address"
                                type="text"
                                placeholder="Домашний адрес *"
                                name="address"
                                value="{{ old('address') }}"
                                autofocus
                                required
                        />
                    </div>
                </div>
                <div class="registration-form-bottom-pane">
                    @if ($errors->has('confirm_terms'))
                        <div class="form-help-error">
                            <strong>{{ $errors->first('confirm_terms') }}</strong>
                        </div>
                    @endif
                    <label class="form-checkbox">
                        <input
                                class="form-checkbox__input"
                                type="checkbox"
                                name="confirm_terms"
                                value="true"
                        <?php if (NULL != (old('confirm_terms'))) {
                            echo 'checked="checked"';
                        } ?>
                        />
                        <span class="form-checkbox__text" id="link_terms">
                            Я принимаю <a href="/terms1">договор сотрудничества</a>
                        </span>
                    </label>

                    <button type="submit" class="form-button">Регистрация</button>
                </div>
            </form>
        </div>
    </div>
    <script>
        var tariff_select=$('#tariff_id');
        tariff_select.change(function(){
            if ($('#tariff_id :selected').val()!='') {
                document.getElementById('link_terms').innerHTML ='Я принимаю <a href="/terms'+$('#tariff_id :selected').val()+'">договор сотрудничества</a>';
            }
            else {
                document.getElementById('link_terms').innerHTML ='Я принимаю <a href="/terms1">договор сотрудничества</a>';
            }

        });
    </script>
@endsection
