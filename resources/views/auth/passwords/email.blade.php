@extends('layouts.app')

<!-- Main Content -->
@section('content')
    <div class="main">
        <div class="container">
            <h2 class="post-title">Запрос сброса пароля</h2>

            @if (session('status'))
                <div class="form-help-success">
                    {{ session('status') }}
                </div>
            @endif

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                {{ csrf_field() }}

                <label for="email" class="form-field-label">Адрес электронной почты</label>
                @if ($errors->has('email'))
                    <span class="form-help-error">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                @endif
                <input
                    id="email"
                    type="email"
                    class="form-field"
                    name="email"
                    value="{{ old('email') }}"
                    required
                    placeholder="Адрес электронной почты*"
                />

                <button type="submit" class="form-button">
                    Отправить
                </button>

            </form>

        </div>
    </div>
@endsection
