@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="container">
            <h2 class="post-title">Сброс пароля</h2>

            @if (session('status'))
                <div class="form-help-success">
                    {{ session('status') }}
                </div>
            @endif

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">

                <label for="email" class="form-field-label">Адрес электронной почты</label>
                @if ($errors->has('email'))
                    <span class="form-help-error">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                <input id="email" type="email" class="form-field" name="email" value="{{ $email or old('email') }}" required autofocus>

                <label for="password" class="form-field-label">Пароль</label>
                @if ($errors->has('password'))
                    <span class="form-help-error">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                <input id="password" type="password" class="form-field" name="password" required>

                <label for="password-confirm" class="form-field-label">Проверка пароля</label>
                @if ($errors->has('password_confirmation'))
                    <span class="form-help-error">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
                <input id="password-confirm" type="password" class="form-field" name="password_confirmation" required>

                <button type="submit" class="form-button">
                    Установить новый пароль
                </button>
            </form>

        </div>
    </div>
@endsection
