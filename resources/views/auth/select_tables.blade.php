<?php
    use Illuminate\Support\Facades\Input;
    $sponsor = Input::get('ref') ? : '';
?>
@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="container">

            

            {{--  <h2>Ваш стол {{ $table }} и состоит из {{ $table }} чел!</h2>  --}}

            {{-- {{ dd($user->desk) }} --}}

            {{--  <a href="{{ route('register', ['table' => $table]) }}">Регистрация за {{ $table }}</a>  --}}


            <h2 class="post-title">Пожалуйста выберите стол!</h2>


            <div class="bd-example">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Назавние</th>
                            {{-- <th scope="col">Max кол-во допустимых учасников</th> --}}
                            {{-- <th scope="col">Учасники</th> --}}
                            <th scope="col">Выбрать</th>
                        </tr>
                    </thead>

                    <tbody>

                        @foreach ($tables as $key => $item)
                            <tr class="success">
                                <th>{{ $item->name }}</th>
                                {{-- <td>{{ $item->count }}</td> --}}
                                {{-- <td>{{ count($item->users) }}</td> --}}
                                <td>
                                    <a href="{{ $item->slug }}">Выбрать</a>
                                </td>
                            </tr>
                        @endforeach

                        <tr class="success">
                            <th>
                                <span class="label label-success">новый</span>
                                75 тыс
                            </th>
                                {{-- <td>{{ $item->count }}</td> --}}
                                {{-- <td>{{ count($item->users) }}</td> --}}
                                <td>
                                <a href="http://25000.jardem-c.com/register" target="_blank" >Выбрать</a>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
