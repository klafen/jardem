@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="container">
            <h2 class="post-title">Вход</h2>

            <form role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}
                <div class="login-form">
                    <div class="form-group">
                        <label for="login" class="form-field-label">Ваш логин</label>
                        @if ($errors->has('login'))
                            <span class="form-help-error">
                            <strong>{{ $errors->first('login') }}</strong>
                        </span>
                        @endif
                        <input
                            id="login"
                            type="text"
                            class="form-field"
                            name="login"
                            value="{{ old('login') }}"
                            required
                            autofocus
                        />
                    </div>
                    <div class="form-group">
                        <label for="password" class="form-field-label">Пароль</label>
                        @if ($errors->has('password'))
                            <div class="form-help-error">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                        @endif

                            <input id="password" type="password" class="form-field" name="password" required />

                    </div>


                    <label class="form-checkbox">
                        <input
                            class="form-checkbox__input"
                            type="checkbox"
                            name="remember"
                            {{ old('remember') ? 'checked' : ''}}
                        />
                        <span class="form-checkbox__text">Запомнить меня</span>
                    </label>


                    <button type="submit" class="form-button">Войти</button>

                    <a class="login-form-reset-password" href="{{ url('/password/reset') }}">Забыли пароль?</a>
                </div>
            </form>
        </div>
    </div>
@endsection
