@if(!$entry->hasPaid() && !$entry->isAdmin())
    <a class="btn btn-xs btn-default mark-as-paid" data-login="{{ $entry->login }}" href="{{ url($crud->route.'/'.$entry->getKey().'/approve-payment') }}">Оплатил</a>
@endif