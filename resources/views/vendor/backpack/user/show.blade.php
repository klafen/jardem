@extends('vendor.backpack.base.layout')

@section('content')
    <div>
        <table class="table table-bordered table-striped display">
            <tr>
                <td>Логин:</td>
                <td>{{ $user->login }}</td>
            </tr>
            <tr>
                <td>E-mail:</td>
                <td>{{ $user->email }}</td>
            </tr>
            <tr>
                <td>Спонсор:</td>
                <td>{{ $user->showSponsor() }}</td>
            </tr>
            <tr>
                <td>Пригласитель:</td>
                <td>@if( $user->inviter){{ $user->inviter->login }}@endif</td>
            </tr>
            <tr>
                <td>Имя:</td>
                <td>{{ $user->first_name }}</td>
            </tr>
            <tr>
                <td>Фамилия:</td>
                <td>{{ $user->last_name }}</td>
            </tr>
            <tr>
                <td>Дата рождения:</td>
                <td>{{ $user->date_of_birth }}</td>
            </tr>
            <tr>
                <td>Страна:</td>
                <td>{{ $user->country }}</td>
            </tr>
            <tr>
                <td>Город:</td>
                <td>{{ $user->city }}</td>
            </tr>
            <tr>
                <td>Реквизиты банка:</td>
                <td>{{ $user->bank_account }}</td>
            </tr>
            <tr>
                <td>Банковский счет:</td>
                <td>{{ $user->bank_account }}</td>
            </tr>
            <tr>
                <td>Бенефициар:</td>
                <td>
                    <div>{{ $user->isBen() ? 'Да' : 'Нет' }}</div>
                </td>
            </tr>
            <tr>
                <td>Статус оплаты:</td>
                <td>
                    <div>{{ $user->hasPaid() ? 'Оплачен' : 'Не оплачен' }}</div>
                </td>
            </tr>
        </table>

        @if($user->hasPaid())
            @include('share._tree', ['user' => $user])
        @endif

        @if($user->isMember())
            @include('share._desk', ['user' => $user])
        @endif
    </div>
@endsection