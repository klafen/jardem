<?php $requisite = $entry->{$column['name']}; ?>

<td>
    @if($requisite)
        <div>{{ $requisite->first_name }} {{ $requisite->last_name }}</div>
        <div>{{ $requisite->bank_requisite }}</div>
        <div>{{ $requisite->bank_account }}</div>
    @else
        Не указаны
    @endif
</td>