@if($entry->{$column['name']} == 'wait')
	<td class="b-history-table-item__status b-history-table-item__status--wait">Ожидается</td>
@elseif($entry->{$column['name']} == 'fail')
	<td class="b-history-table-item__status b-history-table-item__status--fail">Отклонён <div class="icon"></div></td>
@elseif($entry->{$column['name']} == 'success')
	<td class="b-history-table-item__status b-history-table-item__status--success">Успешно <div class="icon"></div></td>
@endif