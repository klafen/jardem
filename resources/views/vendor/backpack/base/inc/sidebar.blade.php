@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel" style="height: 40px">
          <div class="pull-left info">
            <p>{{ Auth::user() }}</p>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">{{ trans('backpack::base.administration') }}</li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <li>
            <a href="{{ url(config('backpack.base.route_prefix', 'admin').'/dashboard') }}">
              <i class="fa fa-dashboard"></i>
              <span>{{ trans('backpack::base.dashboard') }}</span>
            </a>
          </li>

          <li>
            <a href="{{ url('admin/page') }}">
              <i class="fa fa-tag"></i>
              <span>Страницы</span>
            </a>
          </li>

          <li>
            <a href="{{ url('admin/news') }}">
              <i class="fa fa-tag"></i>
              <span>Новости</span>
            </a>
          </li>

          <li>
            <a href="{{ url('admin/material') }}">
              <i class="fa fa-tag"></i>
              <span>Обучающие материалы</span>
            </a>
          </li>

          <li>
            <a href="{{ url('admin/user') }}">
              <i class="fa fa-tag"></i>
              <span>Пользователи</span>
            </a>
          </li>

        
          <li>
            <a href="{{ url('admin/desk') }}">
              <i class="fa fa-tag"></i> 
              <span>Столы</span>
            </a>
          </li>

          <li>
            <a href="{{ url('admin/transaction?status=wait') }}">
              <i class="fa fa-tag"></i>
              <span>Необработанные заявки</span></a>
          </li>

          <li>
            <a href="{{ url('admin/transaction') }}">
              <i class="fa fa-tag"></i>
              <span>Обработанные заявки</span>
            </a>
          </li>

          <!-- ======================================= -->
          <li class="header">{{ trans('backpack::base.user') }}</li>

          <li>
            <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              <i class="fa fa-sign-out"></i> 
              <span>{{ trans('backpack::base.logout') }}</span>
            </a>
          </li>

          <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                style="display: none;">
            {{ csrf_field() }}
          </form>

        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif


{{-- @if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel" style="height: 40px">
          <div class="pull-left info">
            <p>{{ Auth::user() }}</p>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">{{ trans('backpack::base.administration') }}</li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
          <li><a href="{{ url('admin/page') }}"><i class="fa fa-tag"></i> <span>Страницы</span></a></li>
          <li><a href="{{ url('admin/news') }}"><i class="fa fa-tag"></i> <span>Новости</span></a></li>
          <li><a href="{{ url('admin/material') }}"><i class="fa fa-tag"></i> <span>Обучающие материалы</span></a></li>
          <li><a href="{{ url('admin/user') }}"><i class="fa fa-tag"></i> <span>Пользователи</span></a></li>
          <li><a href="{{ url('admin/desk') }}"><i class="fa fa-tag"></i> <span>Столы</span></a></li>
          <li><a href="{{ url('admin/transaction?status=wait') }}"><i class="fa fa-tag"></i> <span>Необработанные заявки</span></a></li>
          <li><a href="{{ url('admin/transaction') }}"><i class="fa fa-tag"></i> <span>Обработанные заявки</span></a></li>

          <!-- ======================================= -->
          <li class="header">{{ trans('backpack::base.user') }}</li>
          <li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></li>
          <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                style="display: none;">
            {{ csrf_field() }}
          </form>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif --}}
