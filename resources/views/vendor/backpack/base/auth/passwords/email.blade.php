@extends('backpack::layout')

<!-- Main Content -->
@section('content')
    <div class="main">
        <div class="container">
            <h2 class="post-title">{{ trans('backpack::base.reset_password') }}</h2>

            @if (session('status'))
                <div class="form-help-success">
                    {{ session('status') }}
                </div>
            @endif

            <form class="form-horizontal" role="form" method="POST" action="{{ url(config('backpack.base.route_prefix', 'admin').'/password/email') }}">
                {!! csrf_field() !!}

                <label for="email" class="form-field-label">{{ trans('backpack::base.email_address') }}</label>
                @if ($errors->has('email'))
                    <span class="form-help-error">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                <input
                    type="email"
                    class="form-control"
                    name="email"
                    id="email"
                    value="{{ old('email') }}"
                />

                <button type="submit" class="form-button">
                    {{ trans('backpack::base.send_reset_link') }}
                </button>

            </form>
        </div>
    </div>
@endsection
