@extends('backpack::layout')

@section('content')
    <div class="main">
        <div class="container">
            <h2 class="post-title">{{ trans('backpack::base.reset_password') }}</h2>

            @if (session('status'))
                <div class="form-help-success">
                    {{ session('status') }}
                </div>
            @endif

            <form class="form-horizontal" role="form" method="POST" action="{{ url(config('backpack.base.route_prefix', 'admin').'/password/reset') }}">
                {!! csrf_field() !!}

                <input type="hidden" name="token" value="{{ $token }}">

                <label for="email" class="form-field-label">{{ trans('backpack::base.email_address') }}</label>
                @if ($errors->has('email'))
                    <span class="form-help-error">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                <input id="email" type="email" class="form-field" name="email" value="{{ $email or old('email') }}" required autofocus>

                <label for="password" class="form-field-label">{{ trans('backpack::base.password') }}</label>
                @if ($errors->has('password'))
                    <span class="form-help-error">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                <input id="password" type="password" class="form-field" name="password" required>

                <label for="password-confirm" class="form-field-label">{{ trans('backpack::base.confirm_password') }}</label>
                @if ($errors->has('password_confirmation'))
                    <span class="form-help-error">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
                <input id="password-confirm" type="password" class="form-field" name="password_confirmation" required>

                <button type="submit" class="form-button">
                    {{ trans('backpack::base.reset_password') }}
                </button>
            </form>

        </div>
    </div>
@endsection
