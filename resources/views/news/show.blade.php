@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="container">
            <h1 class="post-title">{{ $item->title }}</h1>

            <div class="post-news">
                <div class="post-news__content">
                    {!! $item->content !!}
                </div>

                <div class="post-news__author">
                    {!! $item->author !!}
                </div>

                @include('share.buttons')
            </div>
        </div>
    </div>
@endsection
