@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="container">
            <h3 class="post-title">
                {{ Route::is('materials') ? 'Обучающие материалы' : 'Новости' }}
            </h3>

            @foreach($news->chunk(3) as $row)
                <div class="news-container">
                    @foreach($row as $item)
                        <div class="news-preview">
                            <h2 class="news-preview__title">
                                <a href="{{ Route::is('materials') ? 'materials' :  'news' }}/{{ $item->id }}">
                                    {{ $item->title }}
                                </a>
                            </h2>
                            <div class="news-preview__text">
                                {!! str_limit(strip_tags($item->content), 200) !!}
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach

            {!! $news->render() !!}
        </div>
    </div>
@endsection