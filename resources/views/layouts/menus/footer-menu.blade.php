@if(Auth::check() || !env('DEMO_INSTANCE'))
    <div class="header-menu">
        <ul>
            <li><a href="{{ url('/news') }}">Новости</a></li>
            <li><a href="{{ url('/materials') }}">Обучающие материалы</a></li>
            <li><a href="#">Вебинары</a>
            <li><a href="{{ url('/about') }}">О нас</a></li>
            <li><a href="{{ url('/contacts') }}">Контакты</a></li>
            <li><a href="{{ url('/marketing') }}">Маркетинг</a></li>
        </ul>
    </div>
@endif