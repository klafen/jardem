@if(Auth::check() || !env('DEMO_INSTANCE'))
    <div class="hidden-sm hidden-md hidden-lg">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" style="float:none;">
            <span style="font-size: 14px;">Развернуть меню</span>
        </button>
    </div>

    <div class="header-menu hidden-xs">
        <ul>
            <li><a href="{{ url('/news') }}">Новости</a></li>
            <li><a href="{{ url('/materials') }}">Обучающие материалы</a></li>
            <li><a href="#">Вебинары</a>
            <li><a href="{{ url('/about') }}">О нас</a></li>
            <li><a href="{{ url('/contacts') }}">Контакты</a></li>
            <li><a href="{{ url('/marketing') }}">Маркетинг</a></li>
        </ul>
    </div>
@endif