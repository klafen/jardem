<div class="col-md-2 col-sm-3 col-xs-4">
    <div class="header-logo"><a href="/">Jardem Corporation</a></div>
</div>
<div class="col-md-6 col-sm-5 col-xs-3">
    @include('layouts.menus.main-menu')
</div>
<div class="col-sm-4 col-xs-5">
        @if (Auth::guest())
            <div class="header-login">
                <div class="col-sm-6 col-xs-12 text-right" ><a class="form-button btn-default" href="{{ url('/login') }}">Вход</a></div>
                <div class="col-sm-6 col-xs-12 text-left "><a class="form-button" href="{{ url('/register') }}">Регистрация</a></div>
            </div>
        @else
                    <div class="header-account">
                        <div class="header-account-icon hidden-xs"></div>
                        <div class="header-account-title">
                            <a href="/account/history">{{ Auth::user()->totalMoney() }} тнг</a>
                        </div>
                    </div>

            <div class="header-profile">
                <div class="header-profile-top">
                    <div class="header-profile-name">{{ Auth::user()->login }}</div>
                    <div class="header-profile-arrow"></div>
                </div>
                <div class="header-profile-menu">
                    <ul>
                        @if(Auth::user()->isAdmin())
                            <li>
                                <a href="{{ url('/admin') }}">
                                    Панель управления
                                </a>
                            </li>
                        @endif

                        @if(Auth::user()->login != 'admin')
                            <li>
                                <a href="{{ url('/account/profile') }}">
                                    Личный кабинет
                                </a>
                            </li>
                        @endif

                        <li>
                            <a
                                    href="{{ url('/logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                            >
                                Выйти
                            </a>

                            <form
                                    id="logout-form"
                                    action="{{ url('/logout') }}"
                                    method="POST"
                                    style="display: none"
                            >
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        @endif
</div>





