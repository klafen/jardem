<div class="banner">
    <div class="owl-carousel owl-theme">
        <div class="bannerItem">
            <img src="images/banner/banner-1.jpg" alt="">
        </div>

        <div class="bannerItem">
            <img src="images/banner/banner-2.jpg" alt="">
        </div>

        <div class="bannerItem">
            <img src="images/banner/banner-3.jpg" alt="">
        </div>

        <div class="bannerItem">
            <img src="images/banner/banner-4.jpg" alt="">
        </div>
    </div>
</div>