<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name=viewport content="width=device-width,initial-scale=1">
    <title>JARDEM CORPORATION</title>
    <!-- Material Design icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- Your styles -->
    <link rel="stylesheet" href="{{ url('/css/app.css') }}">
    <link rel="stylesheet" href="{{ url('/css/sidebar.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
</head>
<body>
<div class="flex-container">
    @if(Request::is('/') && (count($transactions)>0))
        <marquee class="marquee" behavior="alternate" direction="left">
            @foreach($transactions as $item)
                <span style="color: yellow;"> ::: {{ $item->localized_description }}: {{ $item->sum }} тг. участнику <span style="color: tomato;">{{ $item->user->login }}</span> :::</span>
                {{--  ::: {{ $item->localized_description }}: {{ $item->sum }} тг. участнику {{ $item->user->first_name . ' ' . $item->user->last_name }} :::  --}}
            @endforeach
        </marquee>
    @endif
    <div class="header {{ Route::getCurrentRoute()->getPath() == "/" ? 'header--home-page' : '' }}">
        @include('layouts.sections.header')
    </div>
        <nav class="hidden-sm hidden-lg hidden-md navbar" role="navigation" style="min-height:0;margin-bottom:0;">
            <div class="container">
                <div id="navbar" class="navbar-collapse collapse">
                    <ul  class="nav navbar-nav navbar-right">
                        <li><a href="{{ url('/news') }}">Новости</a></li>
                        <li><a href="{{ url('/materials') }}">Обучающие материалы</a></li>
                        <li><a href="#">Вебинары</a>
                        <li><a href="{{ url('/about') }}">О нас</a></li>
                        <li><a href="{{ url('/contacts') }}">Контакты</a></li>
                        <li><a href="{{ url('/marketing') }}">Маркетинг</a></li>
                    </ul>
                </div>
            </div>
        </nav>

    <div class="content">
        @yield('content')
    </div>

    <div class="footer">
        @include('layouts.sections.footer')
    </div>
</div>

<style>
    .marquee {
        padding: 5px;
        background: #333;
        color: #ccc;
    }
</style>

<script src="{{asset('js/app.js')}}"></script>
@if(env('GOOGLE_ANALYTICS') != NULL)
    @include('g-analytics.script')
@endif
</body>