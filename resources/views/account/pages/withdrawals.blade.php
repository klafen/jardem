@extends('account.layout')

@section('accountContent')
    <h1 class="post-title">На вашем балансе</h1>

    <div class="b-withdrawals">
        <div class="b-withdrawals__amount">{{ Auth::user()->totalMoney() }} тнг</div>

        @if(Session::get('transactionSuccess'))
            <h1 class="post-title">Ваша заявка отправлена на обработку.</h1>
        @endif

        @if(!Auth::user()->bankRequisites->count())
            <h1 class="post-title">Для вывода средств заполните ваши платежные данные.</h1>
        @else
            <form method="POST" action="/account/withdraw-funds">
                {{ csrf_field()}}

                <div class="form-field-label">Минимальная сумма для вывода 500 тнг</div>

                @include('account.pages._show-form-error', ['field' => 'amount'])

                <input class="form-field" type="text" placeholder="Сумма вывода" name="amount"
                       value="{{ old('amount') }}">

                <div class="form-group">
                    <div class="form-field-label">Банковский счет</div>

                    @include('account.pages._show-form-error', ['field' => 'bank_id'])

                    <select class="form-field" name="bank_id">
                        <option value>Выберите из списка</option>

                        @foreach(Auth::user()->bankRequisites as $bankRequisite)
                            <option value="{{ $bankRequisite->id }}">
                                {{ $bankRequisite->safeBankAccount }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <button class="form-button">Заказать вывод средств</button>
            </form>
        @endif

        @if(Auth::user()->totalMoney() == 0)
            <div class="error-message">У вас недостаточно средств для выполнения запроса</div>
        @endif
    </div>
@stop