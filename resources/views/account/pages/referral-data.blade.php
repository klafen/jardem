@extends('account.layout')

@section('accountContent')
    <div class="b-tabs-container">
        @include('account.pages.referral-data-tabs', ['thisuser'=>$user, 'active'=>$user->login])
        <div class="b-tabs-content">
            <h1 class="post-title">Реферальные данные ({{$user->login}})</h1>

            <div class="b-referral-data-page">
                @include('share._filter')
                @if($mode=='tree')
                    @if($user->hasPaid())
                        @include('share._tree_full', ['user' => $user])
                    @endif

                @elseif ($mode=='desk')
                    @if ($desk)
                        @include('share._desk_old', ['desk'=>$desk])
                    @else
                        <div>  Вы пока не завершили на данный стол</div>
                    @endif
                @else
                    @if($user->hasPaid())
                        @include('share._tree', ['user' => $user])
                    @endif
                    @if($user->isMember())
                        @include('share._desk', ['user' => $user])
                    @endif
                @endif



            </div>

            {{-- We need this block for tests --}}
            <div class="b-reinvests">

                {{--
                    The list of `$reinvests` may be filtered in the controller
                    so don't use `$user->reinvests`, use the variable instead.


                @if($reinvests->count())

                    @foreach($reinvests as $reinvest)
                        @if($reinvest->isMember())
                            @include('share._desk', ['user' => $reinvest])
                        @elseif($reinvest->hasPaid())
                            @include('share._tree', ['user' => $reinvest])
                        @endif
                    @endforeach

                @else
                    <div style="text-align: center">
                        У вас нет реинвестов на данном уровне
                    </div>
                @endif

                {!! $reinvests->render() !!}
                --}}
            </div>
        </div>
    </div>
@stop
