@extends('account.layout')

@section('accountContent')
    <h1 class="post-title">Персональные данные</h1>

    <form method="POST" action="{{ url('/account/update-profile') }}">
        {{ csrf_field() }}

        <div class="form-field-label">Имя <span class="text-danger">Можно изменить только через запрос администратору</span></div>
        @if ($errors->has('first_name'))
            <span class="form-help-error">
                <strong>{{ $errors->first('first_name') }}</strong>
            </span>
        @endif
        <input
            class="form-field"
            id="first_name"
            type="text"
            placeholder="Имя *"
            name="first_name"
            value="{{ (old('first_name')) ? old('first_name') : $data->first_name }}"
            required
            autofocus
            disabled
        />

        <div class="form-field-label">Фамилия <span class="text-danger">Можно изменить только через запрос администратору</span></div>
        @if ($errors->has('last_name'))
            <span class="form-help-error">
                <strong>{{ $errors->first('last_name') }}</strong>
            </span>
        @endif
        <input
            class="form-field"
            id="last_name"
            type="text"
            placeholder="Фамилия *"
            name="last_name"
            value="{{ (old('last_name')) ? old('last_name') : $data->last_name }}"
            required
            autofocus
            disabled
        />

        <div class="form-field-label">Страна</div>
        @if ($errors->has('country'))
            <span class="form-help-error">
                <strong>{{ $errors->first('country') }}</strong>
            </span>
        @endif
        <input
            class="form-field"
            type="text"
            name="country"
            placeholder="Страна"
            value="{{ (old('country')) ? old('country') : $data->country }}"
        />

        <div class="form-field-label">Город</div>
        @if ($errors->has('city'))
            <span class="form-help-error">
                <strong>{{ $errors->first('city') }}</strong>
            </span>
        @endif
        <input
            class="form-field"
            type="text"
            name="city"
            placeholder="Город"
            value="{{ (old('city')) ? old('city') : $data->city }}"
        />

        <div class="form-field-label">Адрес</div>
        @if ($errors->has('address'))
            <span class="form-help-error">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
        <input
                class="form-field"
                type="text"
                name="address"
                placeholder="Адрес"
                value="{{ (old('address')) ? old('address') : $data->address }}"
        />

        <div class="form-field-label">Телефон</div>
        @if ($errors->has('phone'))
            <span class="form-help-error">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
        <input
                class="form-field"
                type="text"
                name="phone"
                placeholder="Телефон"
                value="{{ (old('phone')) ? old('phone') : $data->phone }}"
        />

        <div class="form-field-label">Skype</div>
        @if ($errors->has('skype'))
            <span class="form-help-error">
                <strong>{{ $errors->first('skype') }}</strong>
            </span>
        @endif
        <input
                class="form-field"
                type="text"
                name="skype"
                placeholder="Skype"
                value="{{ (old('skype')) ? old('skype') : $data->skype }}"
        />


        <button class="form-button">Обновить данные</button>
    </form>
@stop