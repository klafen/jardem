<div class="b-tabs-header">
    <ul>
        <li @if($active== Auth::user()->login) class="b-tab--is-active" @endif ><a href="{{route('referralData')}}">Реферальные данные</a></li>
        @php
        if ($thisuser->owner) {
            $thisuser=$thisuser->owner;
        }
        @endphp

        @foreach($thisuser->reinvests as $reinvest)
            <li @if($active==$reinvest->login) class="b-tab--is-active" @endif><a href="{{route('referralData', ['user_login'=>$reinvest->login])}}">{{$reinvest->last_name}}</a></li>
        @endforeach
        <li @if($active=='invited') class="b-tab--is-active" @endif><a href="{{route('referralInvitedUsers')}}">Приглашенные пользователи</a></li>
    </ul>
</div>

