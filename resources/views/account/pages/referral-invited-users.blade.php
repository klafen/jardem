@extends('account.layout')

@section('accountContent')
    <div class="b-tabs-container">
        @include('account.pages.referral-data-tabs', ['thisuser'=> Auth::user(), 'active'=>'invited'])
        <div class="b-tabs-content">
            <div class="b-referral-data-page">

                <div class="form-field-label">
                    Ссылка для рефералов
                </div>

                <div class="form-field-value">
                    http://jardem-c.com/register?ref={{ Auth::user()->login }}
                </div>

                <h1 class="post-title">
                    Приглашенные пользователи, которые оплатили. Всего: {{ $users->total() }}
                </h1>

                <ul class="list-invited-users">
                    @foreach($users as $user)
                        <li class="invited-user">
                            <div class="invited-user__icon"></div>
                            <div class="invited-user__name"><a href="{{route('referralData',['user_login'=>$user->login] )}}">{{ $user->login }} - {{ $user }}</a></div>
                        </li>
                    @endforeach
                </ul>

                {!! $users->render() !!}
            </div>
        </div>
    </div>
@stop
