@extends('account.layout')

@section('accountContent')
    <h1 class="post-title">Ваши платежные данные</h1>

    <div class="b-all-bank-cards">
        @foreach($data as $card)
            <div class="b-bank-card">
                <div>{{ $card }}</div>
                <div class="b-bank-card__name">{{ $card->bank_requisite }}</div>
                <div class="b-bank-card__number">{{ $card->safeBankAccount }}</div>
                <div class="b-bank-card__edit">
                    <a href="edit-requisite-form?id={{ $card->id }}">
                        Редактировать
                    </a>
                </div>
            </div>
        @endforeach

        <div class="b-add-card"><a href="edit-requisite-form"></a></div>
    </div>
@stop