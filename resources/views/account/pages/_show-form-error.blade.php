@if ($errors->has($field))
    <span class="form-help-error">
        <strong>{{ $errors->first($field) }}</strong>
    </span>
@endif