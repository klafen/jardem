@extends('account.layout')

@section('accountContent')
    <h1 class="post-title">Персональные данные</h1>

    <div class="profile-info">
        <div class="form-field-label">Ссылка для рефералов</div>
        <div class="form-field-value">{{ config('app.domain') }}/register?ref={{ $data->login }}</div>

        @if($data->date_of_birth)
            <div class="form-field-label">Тариф:</div>
            <div class="form-field-value">{{ $data->tariff->caption }} тнг</div>
        @endif

        @if($data->first_name || $data->last_name)
            <div class="form-field-label">Имя:</div>
            <div class="form-field-value">
                {{ $data }}
            </div>
        @endif

        @if($data->date_of_birth)
            <div class="form-field-label">Дата рождения:</div>
            <div class="form-field-value">{{ $data->date_of_birth }}</div>
        @endif

        @if($data->email)
            <div class="form-field-label">E-mail:</div>
            <div class="form-field-value">{{ $data->email }}</div>
        @endif

        @if($data->phone)
            <div class="form-field-label">Телефон:</div>
            <div class="form-field-value">{{ $data->phone }}</div>
        @endif

        @if($data->skype)
            <div class="form-field-label">Skype:</div>
            <div class="form-field-value">{{ $data->skype }}</div>
        @endif

        @if($data->country || $data->city)
            <div class="form-field-label">Адрес:</div>
            <div class="form-field-value">
                @if($data->country) {{ $data->country }}, @endif {{-- Вывод запятой --}}
                {{ $data->city }}@if($data->address), {{ $data->address }} @endif
            </div>
        @endif


            <div class="form-field-label">Спонсор</div>
            <div class="form-field-value">{{ $data->showSponsor() }}</div>
            <div class="form-field-label">Пригласитель</div>
            <div class="form-field-value">@if($data->inviter){{ $data->inviter->login }}@endif </div>

        <a href="profile-edit" class="form-button">Редактировать</a>
    </div>
@stop