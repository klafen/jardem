@extends('account.layout')

@section('accountContent')
    <h1 class="post-title">Редактирование платежные данных</h1>

    <form method="POST" action="{{ url('/account/update-requisite') }}">
        {{ csrf_field() }}

        <div class="form-field-label">Имя:</div>
        @include('account.pages._show-form-error', ['field' => 'first_name'])
        <input
                class="form-field"
                type="text"
                placeholder="Имя"
                name="first_name"
                value="{{ (old('first_name')) ? old('first_name') : '' }}"
        />

        <div class="form-field-label">Фамилия:</div>
        @include('account.pages._show-form-error', ['field' => 'last_name'])
        <input
                class="form-field"
                type="text"
                placeholder="Фамилия"
                name="last_name"
                value="{{ (old('last_name')) ? old('last_name') : '' }}"
        />

        <div class="form-field-label">Название Банка:</div>
        @include('account.pages._show-form-error', ['field' => 'bank_requisite'])
        <textarea
            class="form-field"
            placeholder="{{ (old('bank_requisite')) ? old('bank_requisite') : 'Название банка'}}"
            name="bank_requisite"
        ></textarea>

        <div class="form-field-label">Номер счета:</div>
        @include('account.pages._show-form-error', ['field' => 'bank_account'])
        <input
            class="form-field"
            type="text"
            placeholder="Номер счета"
            name="bank_account"
            value="{{ (old('bank_account')) ? old('bank_account') : '' }}"
        />

        <button class="form-button">Обновить данные</button>
    </form>
@stop
