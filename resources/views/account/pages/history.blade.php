@extends('account.layout')

@section('accountContent')
    <h1 class="post-title">История ваших операций</h1>

    <div class="b-history">
        <div class="b-history-filter">
            <div class="b-history-filter__title">Фильтр:</div>

            <ul class="b-history-filter__list">
                <li><a href="history">Все</a></li>
                <li><a href="?transaction-type=in">Пополнение средств</a></li>
                <li><a href="?transaction-type=out">Вывод средств</a></li>
            </ul>
        </div>

        <table class="b-history-table">
            @foreach($data as $item)
                <tr class="b-history-table-item">
                    <td class="b-history-table-item__amount b-history-table-item__amount--{{ ($item->sum > 0) ? 'plus' : 'minus' }}">
                        {{ $item->sum }} тенге
                    </td>

                    <td class="b-history-table-item__content">
                        {{ $item->localizedDescription }}
                    </td>

                    <td class="b-history-table-item__date">
                        {{ $item->created_at }}
                    </td>

                    @if($item->status == 'wait')
                        <td class="b-history-table-item__status b-history-table-item__status--wait">Ожидается</td>
                    @elseif($item->status == 'fail')
                        <td class="b-history-table-item__status b-history-table-item__status--fail">Отклонён <div class="icon"></div></td>
                    @elseif($item->status == 'success')
                        <td class="b-history-table-item__status b-history-table-item__status--success">Успешно <div class="icon"></div></td>
                    @endif

                </tr>
            @endforeach
        </table>

        {!! $data->render() !!}
    </div>
@stop
