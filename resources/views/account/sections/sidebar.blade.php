<?php $uri =  Request::server('REQUEST_URI')?>
<div class="side-bar-menu">
    <ul>
        <li class="{{ ($uri == '/account/referral-data' || $uri == '/account/referral-invited-users') ? 'is-active' : '' }}"><a href="referral-data">Реферальные данные</a></li>
        <li class="{{ $uri == '/account/profile' ? 'is-active' : '' }}"><a href="profile">Персональные данные</a></li>
        <li class="{{ $uri == '/account/payment-data' ? 'is-active' : '' }}"><a href="payment-data">Платежные данные</a></li>
        <li class="{{ $uri == '/account/withdrawals' ? 'is-active' : '' }}"><a href="withdrawals">Вывести средства</a></li>
        <li class="{{ $uri == '/account/history' ? 'is-active' : '' }}"><a href="history">История операций</a></li>
    </ul>
</div>