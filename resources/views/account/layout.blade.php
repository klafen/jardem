@extends('layouts.app')

@section('content')
    <div class="aside">
        @include('account.sections.sidebar')
    </div>
    <div class="main">
        @yield('accountContent')
    </div>
@stop