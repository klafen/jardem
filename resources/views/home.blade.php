@extends('layouts.app')

@section('content')
    <div class="home-page-container">
        @include('layouts.carousel')


        <div class="home-page-content">
            <div class="row">
                <div class="col-md-7 col-xs-12">
                    {!! $mainPage->content !!}
                </div>
                <div class="col-md-5 col-xs-12">
                    {{--  НОВОСТИ  --}}
                    @include('includes.sidebar_news')
                    {{--  НОВЫЕ УЧАСТНИКИ  --}}
                    @include('includes.sidebar_news_users')
                </div>
            </div>
            <div class="home-page-list">
                <div class="row">
                    <div class="col-sm-1 hidden-xs"></div>
                    <div class="col-sm-2 col-xs-6">
                        <div class="home-page-list__icon">
                            <div class="home-page-list__number">1</div>
                            <img src="{{asset('images/icons/icon-home-page-list-1.png')}}" alt="">
                        </div>
                        <div class="home-page-list__title">
                            Уникальная <br/>
                            методика
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-6">
                        <div class="home-page-list__icon">
                            <div class="home-page-list__number">2</div>
                            <img src="images/icons/icon-home-page-list-5.png" alt="">
                        </div>
                        <div class="home-page-list__title">
                            Благотворительность
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-6">
                        <div class="home-page-list__icon">
                            <div class="home-page-list__number">3</div>
                            <img src="images/icons/icon-home-page-list-3.png" alt="">
                        </div>
                        <div class="home-page-list__title">
                            Финансовые<br/>
                            гарантии
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-6">
                        <div class="home-page-list__icon">
                            <div class="home-page-list__number">4</div>
                            <img src="images/icons/icon-home-page-list-2.png" alt="">
                        </div>
                        <div class="home-page-list__title">
                            Безграничные<br/>
                            возможности
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-6">
                        <div class="home-page-list__icon">
                            <div class="home-page-list__number">5</div>
                            <img src="images/icons/icon-home-page-list-4.png" alt="">
                        </div>
                        <div class="home-page-list__title">
                            Путешествия
                        </div>
                    </div>
                    <div class="col-sm-1 hidden-xs"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
