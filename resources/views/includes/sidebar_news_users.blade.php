<aside class="col-md-12 col-sm-6 col-xs-12 side-bar border" style=" border: 1px solid #bebebe; border-radius: 5px; background: #fafafa;" >
    <ul class="nav nav-tabs" id="myTab">
        <h4 class="heading-small">НОВЫЕ УЧАСТНИКИ</h4>
    </ul>
        
    <div style="max-height: 300px; overflow-y: scroll;">
        
        @foreach ($users as $user)
            <p style="border-bottom: 1px dashed silver">К нам присоединился новый пользователь <span style="color: tomato;">{{ $user->login }}</span> к столу <span style="color: green;">{{ $user->tariff->caption }}</span></p>
        @endforeach
        
    </div>
</aside>