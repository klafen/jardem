@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="container">
            <h1 class="post-title">{{ $page->title }}</h1>

            <div class="post-news">
                <div class="post-news__content">
                    {!! $page->content !!}
                </div>

                @include('share.buttons')
            </div>
        </div>
    </div>
@endsection