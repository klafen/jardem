@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            Inline Charts
            <small>multiple types of charts</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Charts</a></li>
            <li class="active">Inline Charts</li>
        </ol>
    </section>
@endsection

@section('content')

    
    @include('includes.session_errors')
    

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box box-info ">
                    <div class="box-header with-border">
                        <h3 class="box-title">Horizontal Form</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <form action="{{ route('admin.tables.update', $table->id) }}" method="POST">
                        <input type="hidden" name="_method" value="PUT">
                        
                        {{ csrf_field() }}

                        <div class="box-body">

                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label for="inputEmail3">Название стола</label>
                                    <input type="text" name="name" class="form-control" value="{{ $table->name }}" id="inputEmail3" placeholder="Название стола">
                                </div>
                            
                                <div class="form-group">
                                    <label for="inputPassword3">Префикс стола</label>
                                    <input type="text" name="slug" class="form-control" value="{{ $table->slug }}" id="inputPassword3" placeholder="Префикс стола например: table-2">
                                </div>
                            
                                <label for="inputPassword3"> Количество участников</label>
                                <div class="input-group col-sm-12">
                                    <span class="input-group-addon">
                                        <input name="active" type="checkbox" {{ ($table->active) ? 'checked' : 'null' }}>
                                    </span>
                                    <input name="count" type="number" min="1" max="10" value="{{ $table->count }}" class="form-control col-sm-12"  id="inputPassword3" placeholder="Количество участников">
                                </div>

                                <div class="input-group col-sm-12">
                                    <br>
                                    <label for="inputPassword3"> Количество Домов</label>
                                    <input name="count_home" type="number" min="1" max="10" value="{{ $table->count_home }}" class="form-control col-sm-12"  id="inputPassword3" placeholder="Количество Добов">
                                </div>

                                <div class="input-group col-sm-12">
                                    <br>
                                    <label for="inputPassword3"> Количество мест в доме</label>
                                    <input name="count_place_home" type="number" min="1" max="10" value="{{ $table->count_place_home }}" class="form-control col-sm-12"  id="inputPassword3" placeholder="Количество мест в доме">
                                </div>

                                <div class="input-group col-sm-12">
                                    <br>
                                    <label for="inputPassword3">Награда за (3 урвень) </label>
                                    <input name="y_lv_3" type="number" value="{{ $table->y_lv_3 }}" class="form-control col-sm-12"  id="inputPassword3" placeholder="Укажите награду за (3 урвень)">
                                </div>

                                <div class="input-group col-sm-12">
                                    <br>
                                    <label for="inputPassword3">Награда за (4 урвень) </label>
                                    <input name="y_lv_4" type="number" value="{{ $table->y_lv_4 }}" class="form-control col-sm-12"  id="inputPassword3" placeholder="Укажите награду за (4 урвень)">
                                </div>

                                <div class="input-group col-sm-12">
                                    <br>
                                    <label for="inputPassword3">Награда за (5 урвень) </label>
                                    <input name="y_lv_5" type="number" value="{{ $table->y_lv_5 }}" class="form-control col-sm-12"  id="inputPassword3" placeholder="Укажите награду за (5 урвень)">
                                </div>

                                <div class="input-group col-sm-12">
                                    <br>
                                    <label for="inputPassword3">Награда за (6 урвень) </label>
                                    <input name="y_lv_6" type="number" value="{{ $table->y_lv_6 }}" class="form-control col-sm-12"  id="inputPassword3" placeholder="Укажите награду за (6 урвень)">
                                </div>

                                <div class="input-group col-sm-12">
                                    <br>
                                    <label for="inputPassword3">Награда за (7 урвень) </label>
                                    <input name="y_lv_7" type="number" value="{{ $table->y_lv_7 }}" class="form-control col-sm-12"  id="inputPassword3" placeholder="Укажите награду за (7 урвень)">
                                </div>

                                <div class="input-group col-sm-12">
                                    <br>
                                    <label for="inputPassword3">Награда на (8 урвень) </label>
                                    <input name="y_lv_8" type="number" value="{{ $table->y_lv_8 }}" class="form-control col-sm-12"  id="inputPassword3" placeholder="Укажите награду за (8 урвень)">
                                </div>

                                <div class="input-group col-sm-12">
                                    <br>
                                    <label for="inputPassword3">Награда на (9 урвень) </label>
                                    <input name="y_lv_9" type="number" value="{{ $table->y_lv_9 }}" class="form-control col-sm-12"  id="inputPassword3" placeholder="Укажите награду за (9 урвень)">
                                </div>

                                <div class="input-group col-sm-12">
                                    <br>
                                    <label for="inputPassword3">Награда на (10 урвень) </label>
                                    <input name="y_lv_10" type="number" value="{{ $table->y_lv_10 }}" class="form-control col-sm-12"  id="inputPassword3" placeholder="Укажите награду за (10 урвень)">
                                </div>



                            </div>

                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label for="inputEmail3">Правила стола</label>
                                    {{--  <input type="email" class="form-control" id="inputEmail3" placeholder="Email">  --}}
                                    <textarea name="description" class="form-control" id="desc" cols="30" rows="10" placeholder="Правило стола...">{{ $table->description }}</textarea>
                                </div>
                            </div>
                            
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{ route('admin.tables.index') }}" class="btn btn-default">Назад</a>

                            <input type="submit" class="btn btn-info pull-right" value="Обновить">
                            {{--  <bottom type="submit" class="btn btn-info pull-right">Создать</bottom>  --}}

                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
