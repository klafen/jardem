@extends('backpack::layout')

@section('content')
    <section class="content">

        <a href="{{ url('admin/tables/' . $table->id . '/edit') }}" style="margin: 2px 0;" class="btn btn-sm btn-success btn-edit">
            <i class="fa fa-edit"></i> Редактировать
        </a>
        
        <div>
            <table class="table table-bordered table-striped display">
                <tbody>
                    
                    <tr>
                        <td>Название стола</td>
                        <td>{{ $table->name }}</td>
                    </tr>

                    <tr>
                        <td>Префикс стола</td>
                        <td>{{ $table->slug }}</td>
                    </tr>

                    <tr>
                        <td>Max кол-во участвуещих таблицы</td>
                        <td>{{ $table->count }}</td>
                    </tr>

                    <tr>
                        <td>Активный</td>
                        <td>
                            
                            @if ($table->active)
                                <span class="label label-success"> активный</span>
                            @else
                                <span class="label label-danger"> не активный</span>
                            @endif
                            
                        </td>
                    </tr>

                    <tr>
                        <td>Правило таблицы</td>

                        <td>{{ $table->description ? $table->description : "Не указано" }}</td>
                        
                    </tr>

                    <hr>

                   
                   {{-- @if (count($table->users) > 0)
                    <tr>
                            <td class="danger">Внимание!</td>
                            <td class="danger">Важно!</td>
                        </tr>

                        <tr>
                            <td class="danger">Кол-во участвующих</td>
                            <td class="danger">{{ count($table->users) }}</td>
                        </tr>
                   @endif --}}
                   

                </tbody>
            </table>
        </div>

        <form action="{{ url('admin/tables/' . $table->id) }}" method="POST">
            <input type="hidden" name="_method" value="DELETE">
            {{ csrf_field() }}

            <button type="submit" class="btn btn-sm btn-danger">
                <i class="fa fa-trash"></i> Удалить
            </button>
        </form>
        
    </section>
@endsection