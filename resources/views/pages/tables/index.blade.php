@extends('backpack::layout')


@section('header')
    <section class="content-header">


        <h1>
            Inline Charts
            <small>multiple types of charts</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Charts</a></li>
            <li class="active">Inline Charts</li>
        </ol>
    </section>
@endsection


@section('content')




<!-- /.row -->
<div class="row">
  <div class="col-xs-12">
    <div class="box">
        
      <div class="box-header">

        <div class="col-md-2">
            <a href="{{ url('admin/tables/create') }}" class="btn btn-block btn-success btn-sm">Создать новый стол.</a>
        </div><br><br><br>

        <h3 class="box-title">Responsive Hover Table</h3>
        
        <div class="box-tools">
          <div class="input-group input-group-sm" style="width: 150px;">
                
            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

            
            <div class="input-group-btn">
              <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">

        <table class="table table-bordered table-striped display dataTable table-hover">
            <tr>
                <th>Навание</th>
                <th>Префикс</th>
                {{-- <th>Кол-во Участников</th> --}}
                <th>Ссылка</th>
                <th>Активный</th>
                <th>Плавила</th>

                <th>Кол-во Домов</th>
                <th>Кол-во мест в доме</th>

                <th>Награда за (3 урвень)</th>
                {{-- <th>Укажите награду за (4 урвень)</th> --}}
                {{-- <th>Укажите награду за (5 урвень)</th> --}}
                {{-- <th>Укажите награду за (6 урвень)</th> --}}
                {{-- <th>Укажите награду за (7 урвень)</th> --}}
                {{-- <th>Укажите награду за (8 урвень)</th> --}}
                {{-- <th>Укажите награду за (9 урвень)</th> --}}
                <th>Награда за (10 урвень)</th>

                <th>Действи</th>
            </tr>
                
            @foreach ($tables as $table)

            {{--  {{ dd($table) }}  --}}

                <tr>
                    <td>{{ $table->name }}</td>
                    <td>{{ $table->slug }}</td>
                    {{-- <td>{{ count($table->users) }}</td> --}}
                    <td>{{ $table->slug }}</td>

                    <td>{{ $table->count_home }}</td>
                    <td>{{ $table->count_place_home }}</td>
                    <td>{{ $table->y_lv_3 }}</td>
                    {{-- <td>{{ $table->y_lv_4 }}</td> --}}
                    {{-- <td>{{ $table->y_lv_5 }}</td> --}}
                    {{-- <td>{{ $table->y_lv_6 }}</td> --}}
                    {{-- <td>{{ $table->y_lv_7 }}</td> --}}
                    {{-- <td>{{ $table->y_lv_8 }}</td> --}}
                    {{-- <td>{{ $table->y_lv_9 }}</td> --}}
                    <td>{{ $table->y_lv_10 }}</td>

                    <td>
                        <span class="label label-{{ ($table->active) ? 'success' : 'danger' }}">{{ $table->active }}</span>
                    </td>
                    <td>{{ str_limit($table->description, 20) }}</td>

                    <td>
                        <a class="btn btn-sm btn-info" href="{{ route('admin.tables.show', ['id' => $table->id]) }}">
                            <i class="fa fa-info-circle"></i> Подробнее</a>
                        <br>	

                        {{--  <a href="{{ url('admin/tables/' . $table->id . '/show') }}">wdawdawdawdawd</a>  --}}

                        

                        {{--  <a href="{{ url('admin/tables/' . $table->id) }}" style="margin: 2px 0;" data-id="{{ $table->id }}" class="btn btn-sm btn-danger">
                            DELETE
                        </a>  --}}

                        
                    </td>
                </tr>

            @endforeach
          
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>

{{--  <script src="{{ asset('js') }}/ajax.js"></script>  --}}

@endsection
