<div class="b-matrix b-desk-level-{{ $desk->level }}">
    <div class="b-matrix-header">
        <div class="b-matrix-header__name">
            Закрытый стол №{{$desk->level}}, закрыт: {{$desk->updated_at}}
        </div>
    </div>

    <div class="b-matrix-content b-desk">
        <div class="row">
            <div class="col-12 coltree">
                @include('share._user', ['user' => $desk->histories[0]->user])
            </div>
        </div>

        <div class="row">
            <div class="col-6 coltree">
                @include('share._user', ['user' => $desk->histories[1]->user])
            </div>

            <div class="col-6 coltree">
                @include('share._user', ['user' => $desk->histories[2]->user])
            </div>
        </div>

        <div class="row">
            <div class="col-3 coltree">
                @include('share._user', ['user' => $desk->histories[3]->user])
            </div>

            <div class="col-3 coltree">
                @include('share._user', ['user' => $desk->histories[4]->user])
            </div>

            <div class="col-3 coltree">
                @include('share._user', ['user' => $desk->histories[5]->user])
            </div>

            <div class="col-3 coltree">
                @include('share._user', ['user' => $desk->histories[6]->user])
            </div>
        </div>
    </div>
</div>