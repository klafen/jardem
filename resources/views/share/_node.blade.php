@if(count($parent->referrals)>0)
    <li>
        <ul class="browser-default default-style" style="list-style-type:none;">
        @for($i = 0; $i < 3; $i++)
            @php $thisuser=$parent->referrals->get($i) @endphp
            @if ($thisuser)
                @include('share._node_user', ['current_user' => $thisuser])
                @include('share._node', ['parent' =>$thisuser])
            @else
            <li></li>
            @endif
        @endfor
        </ul>
    </li>
@else

@endif