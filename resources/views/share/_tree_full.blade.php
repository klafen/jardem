<div class="b-matrix b-tree">
    <div class="b-matrix-header">
        <div class="b-matrix-header__name">
            Дерево
        </div>

        <div class="b-matrix-header__position">

        </div>
    </div>
    <div class="b-matrix-content">
        <div class="row">
            <ul class="browser-default default-style" style="list-style-type:none;">
                @include('share._node_user', ['current_user' => $user])
                @include('share._node', ['parent' => $user])
            </ul>
        </div>
    </div>


</div>