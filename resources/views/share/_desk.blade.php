<div class="b-matrix b-desk-level-{{ $user->desk->level }}">
    <div class="b-matrix-header">
        <div class="b-matrix-header__name">
            Текуший стол: №{{$user->desk->level}}
        </div>
    </div>

    <div class="b-matrix-content b-desk">
        <div class="row">
            <div class="col-12 coltree">
                @include('share._user', ['user' => $user])
            </div>
            @if($user->tariff_id !=3)
                <div style="position:absolute; font-size:45px; right:10px;">{{$user->desk_order}}</div>
            @endif
        </div>

        <div class="row">
            <div class="col-6 coltree">
                @include('share._user', ['user' => $user->deskMates->get(0)])
            </div>

            <div class="col-6 coltree">
                @include('share._user', ['user' => $user->deskMates->get(1)])
            </div>
        </div>

        <div class="row">
            <div class="col-3 coltree">
                @include('share._user', ['user' => $user->deskMates->get(2)])
            </div>

            <div class="col-3 coltree">
                @include('share._user', ['user' => $user->deskMates->get(3)])
            </div>

            <div class="col-3 coltree">
                @include('share._user', ['user' => $user->deskMates->get(4)])
            </div>

            <div class="col-3 coltree">
                @include('share._user', ['user' => $user->deskMates->get(5)])
            </div>
        </div>
    </div>
</div>