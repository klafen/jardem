@if($user)
    <div class="b-matrix-user">
        <div class="b-matrix-user__icon"></div>
        <div class="b-matrix-user__name"><a href="{{route('referralData',['user_login'=>$user->login] )}}">{{ $user->login }}</a></div>
    </div>
@else
    <div class="b-matrix-user b-matrix-user--empty">
        <div class="b-matrix-user__icon"></div>
        <div class="b-matrix-user__name"></div>
    </div>
@endif