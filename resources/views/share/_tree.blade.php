<div class="b-matrix b-tree">
    <div class="b-matrix-header">
        <div class="b-matrix-header__name">
            Дерево
        </div>

        <div class="b-matrix-header__position">

        </div>
    </div>

    <div class="b-matrix-content">
        <div class="row">
            <div class="col-12 coltree">
                @include('share._user', ['user' => $user])
            </div>
        </div>

        <div class="row">
            @if ($user->tariff_id == 3)
                @for($i = 0; $i < 2; $i++)
                    <div class="col-6 coltree">
                        @include('share._user', ['user' => $user->referrals->get($i)])
                    </div>
                @endfor
            @else
                @for($i = 0; $i < 3; $i++)
                    <div class="col-4 coltree">
                        @include('share._user', ['user' => $user->referrals->get($i)])
                    </div>
                @endfor
            @endif

        </div>
    </div>
</div>