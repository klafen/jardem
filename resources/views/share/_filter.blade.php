<ul class="pagination">

    @foreach($filter->getOptions() as $option => $name)
        @if ($option == $filter->currentOption())
            <li class="active">
                <span>{{ $name }}</span>
            </li>
        @else
            <li>
                <a id={{ "filter-{$option}" }} href="{{ $filter->url($option) }}">
                    {{ $name }}
                </a>
            </li>
        @endif
    @endforeach

</ul>