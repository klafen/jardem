<?php

namespace App\Email;

use Illuminate\Auth\Notifications\ResetPassword as ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPasswordNotification extends ResetPassword
{
    /**
     * Build the mail representation of the notification.
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line([
                'Вы получили это письмо, т.к. кто-то запросил сброс пароля для вашего аккаунта.',
                'Нажмите кнопку ниже, чтобы сбросить ваш пароль:',
            ])
            ->action('Сбросить пароль', url('/password/reset', $this->token))
            ->line('Если вы не запрашивали сброс пароля, проигнорируйте это сообщение.');
    }
}
