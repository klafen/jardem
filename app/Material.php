<?php

namespace App;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Material.
 *
 * @property int $id
 * @property string $slug
 * @property string $title
 * @property string $content
 * @property int $author_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Query\Builder|\App\Material whereAuthorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Material whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Material whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Material whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Material whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Material whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Material whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Material extends Model
{
    use CrudTrait;

    protected $table = 'materials';
    protected $fillable = ['slug', 'title', 'content'];

    /**
     * Returns the Material with the given slug and fails with 404
     * if it doesn't exist.
     *
     * @param string $slug
     * @return Material
     */
    public static function bySlug($slug)
    {
        return static::whereSlug($slug)->firstOrFail();
    }

    public function author()
    {
        return $this->belongsTo(User::class);
    }
}
