<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    protected $fillable = ["name", "slug", "count", "active", "count_home", "count_place_home", 
                            "y_lv_3", "y_lv_4", "y_lv_5", "y_lv_6", "y_lv_7", "y_lv_8", "y_lv_9", "y_lv_10",
                            "description", "description"];

    /**
     * Связь между Пользователями
     *
     * @return void
     */
    public function users()
    {
        
        return $this->hasMany('App\User', "tables_id", "id"); // , 'foreign_key', 'other_key'
        // return $this->belongsTo('App\User', "id", "tables_id"); // , 'foreign_key', 'other_key'
    }
}
