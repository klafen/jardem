<?php

namespace App;

use App\Depozit\Desk;
use App\Depozit\Tariff;
use App\Depozit\Tree;
use App\Depozit\UnpaidUserCanNotFollowException;
use App\Depozit\UserIsAlreadyPaidException;
use App\Email\ResetPasswordNotification;
use Backpack\CRUD\CrudTrait;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * App\User.
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 * @property int $id
 * @property string $login
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $birthday
 * @property string $country
 * @property string $city
 * @property string $remember_token
 * @property string $address
 * @property string $phone
 * @property string $skype
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereBirthday($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 * @property string $date_of_birth
 * @property int $sponsor_id
 * @property int $inviter_id
 * @property string $bank_account
 * @method static \Illuminate\Database\Query\Builder|\App\User whereDateOfBirth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereSponsorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereBankAccount($value)
 * @property-read \App\User $sponsor
 * @property-read \App\User $inviter
 * @property-read \App\User $owner
 * @property bool $is_admin
 * @property bool $is_ben
 * @method static \Illuminate\Database\Query\Builder|\App\User whereIsAdmin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereIsBen($value)
 * @property \Carbon\Carbon $payment_date
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePaymentDate($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Transaction[] $transactions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BankRequisite[] $bankRequisites
 * @property int $level
 * @property int $priority
 * @property int $desired_sponsor_id
 * @property int $desk_id
 * @property int $turn
 * @property int $is_reinvest
 * @property int $owner_id
 * @property-read \App\Depozit\Desk $desk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $referrals
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Reinvest[] $reinvests
 * @property-read \App\Reinvest $lastreinvest
 * @method static \Illuminate\Database\Query\Builder|\App\User fromDesk($level)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereDesiredSponsorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereDeskId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereIsReinvest($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereOwnerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePriority($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereTurn($value)
 * @property int $desk_order
 * @property int $desk_position
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $allDeskMates
 * @property-read mixed $desk_mates
 * @property-read mixed $position_in_desk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $invited
 * @property int $tariff_id
 * @property-read \App\Depozit\Tariff $tariff
 * @method static \Illuminate\Database\Query\Builder|\App\User whereTariffId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User fromTree()
 * @method static \Illuminate\Database\Query\Builder|\App\User members()
 * @method static \Illuminate\Database\Query\Builder|\App\User fromdesktariff($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User onlyPaid()
 * @method static \Illuminate\Database\Query\Builder|\App\User onlyReal()
 * @method static \Illuminate\Database\Query\Builder|\App\User whereDeskOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereDeskPosition($value)
 */
class User extends Authenticatable
{
    use CrudTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login',
        'email',
        'date_of_birth',
        'first_name',
        'last_name',
        'bank_account',
        'country',
        'city',
        'address',
        'phone',
        'tariff_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'date_of_birth',
        'payment_date',
        'created_at',
        'updated_at',
    ];

    /**
     * Used as the default sponsor for those users who
     * registered without any sponsor login.
     *
     * @return User
     */
    public static function root()
    {
        return self::byLogin('office');
    }

    /**
     * Returns all users who was marked as paid by admin.
     *
     * @param Builder $query
     */
    public static function scopeOnlyPaid(Builder $query)
    {
        $query->whereNotNull('payment_date');
    }

    /**
     * Returns only users that are not reinvests.
     *
     * @param Builder $query
     */
    public static function scopeOnlyReal(Builder $query)
    {
        $query->whereIsReinvest(false);
    }

    /**
     * Returns only users who haven't got into the desks.
     *
     * @param Builder $query
     */
    public static function scopeFromTree(Builder $query)
    {
        $query->whereDeskId(0);
    }

    /**
     * @param Builder $query
     */
    //возвращает количество членов 
    public static function scopeMembers(Builder $query)
    {
        $query->where('desk_id', '!=', 0);
    }

    /**
     * Возвращает всех пользователей, членов стола указанного уровня
     * Returns all users that are members of a desk with the given level.
     *
     * @param Builder $query
     * @param $level
     */
    public static function scopeFromDesk(Builder $query, $level)
    {
        $query
            ->selectRaw('users.*')
            ->leftJoin('desks', 'users.desk_id', '=', 'desks.id')
            ->where('desks.level', $level)
            ->orderBy('desk_order');
    }

    /**
     * Возвращает всех пользователей, членов столов указанного тарифа
     * Returns all users that are members of a desk with the given level.
     *
     * @param Builder $query
     * @param $tariff
     */
    public static function scopeFromDeskTariff(Builder $query, $tariff)
    {
        $query->where('desk_id', '!=', 0)->where('tariff_id','=',$tariff)->orderBy('desk_order');
    }

    /**
     * Returns `true` if there is a user with the given login
     * and this user is NOT a reinvest.
     *
     * @param string $login
     * @return bool
     */
    public static function isRealUser($login)
    {
        return static::whereLogin($login)->whereIsReinvest(false)->exists();
    }

    public function giveMoney($sum, $description = '')
    {
        $transaction = new Transaction;

        $transaction->sum = $sum;
        $transaction->status = 'success';
        $transaction->description = $description;
        $transaction->user()->associate($this);
        $transaction->save();

        return $this;
    }

    public function giveReinvests($count)
    {
        for ($i = 0; $i < $count; $i++) {
            $this->makeReinvest();
        }

        return $this;
    }

    public function makeReinvest()
    {
        return Reinvest::makeFor($this);
    }

    /**
     * @return $this
     */
    public function goToNextTurn()
    {
        $this->turn += 1;

        $this->save();

        return $this;
    }

    /**
     * Is this user a member of any table?
     *
     * @return bool
     */
    public function isMember()
    {
        return (boolean) $this->desk_id;
    }

    /**
     * связывание пользователя с указаным спонсором и назначаем спонсора
     *
     * @param User $desiredSponsor
     * @return $this
     * @throws UnpaidUserCanNotFollowException
     */
    public function follow(User $desiredSponsor)
    {
        //если пользователь уже был отмечен как проплаченный, то его связывать нельзя (уже связан)
        if (! $this->hasPaid()) {
            throw new UnpaidUserCanNotFollowException;
        }

        //если цепляем НЕ реинвест, то надо его сцеплять с реинвестом указанного спонсора
        if (! $this->is_reinvest) {
            //если у указанного спонсора есть реинвесты, то будем цеплять к реинвесту
            //если у пригласителя есть реинвесты, то надо цеплять к ним
            if ($this->inviter->reinvests->count() > 0) {
                $this->inviter_id = $this->inviter->lastreinvest()->id;
            }
            //аналогично и со спонсором
            //если у спонсора есть реинвесты, то надо цеплять к ним
            if ($desiredSponsor->reinvests->count() > 0) {
                $desiredSponsor = $desiredSponsor->lastreinvest();
                $this->desired_sponsor_id = $desiredSponsor->id;
            }
        }

        //определяем доступного спонсора у указанного спонсора
        //$availableSponsor = $desiredSponsor->firstAvailable();
        //поправка - с учетом тарифа
        $availableSponsor = $desiredSponsor->firstAvailableForTariff($desiredSponsor->tariff_id);
        
        //помещаем пользователя в дерево выявленного спонсора и сохраняем
        $this->placeToTree($availableSponsor)->save();

        //если проплативший пользователь не реинвест и тариф не на миллион, то надо выдать/не выдать бонус за приглашенного
        if (! $this->is_reinvest && $this->tariff_id !=3) {
            //объект пригласителя
            $inviter= $this->inviter;
            //если есть реинвесты, то считаем личников у реинвеста, иначе - у пользователя
            if ($inviter->reinvests->count() > 0)  {
                $countinvited= $inviter->lastreinvest()->invited()->onlyPaid()->count();
            }
            else {
                $countinvited=$inviter->invited()->onlyPaid()->count();
            }
            //пригласителю начисляем бонус пригласителю за приглашенного пользователя, если у него количество приглашенных больше минимальнго числа
            if ($countinvited > config('money.'.$inviter->tariff->caption.'.MIN_FOR_REWARD_FOR_REFERRAL')) {
                $inviter->giveMoney(config('money.'.$inviter->tariff->caption.'.REWARD_FOR_REFERRAL'), 'REWARD_FOR_REFERRAL');
            }
            //иначе - надо начислять бонус за глубину
            else {
                //получаем пригласителя пригласителя оплатившего пользователя (дедушку)
                $parent_inviter= $inviter->inviter;
                $depth=0;
                //если дедушка найден, то
                if ($parent_inviter) {
                    //считаем глубину относительно дедушки, для этого идем вверх по дереву до тех пор, пока спонсор не будет равен дедушке
                    $parent=$this->sponsor;
                    //пока определяется родитель, он не равен дедушке и глубина не превысила 10
                    While ($parent && $parent !=$parent_inviter && $depth<=10) {
                        $parent=$parent->sponsor;
                        $depth++;
                    }
                }
                //если глубина больше либо равна трем и не равна 11 (условие выхода из цикла), и у дедушки есть больше  нужного количества личников то
                if ($depth>=3 && $depth!=11 && $parent_inviter->invited()->onlyPaid()->count() > config('money.'.$inviter->tariff->caption.'.MIN_FOR_REWARD_FOR_REFERRAL')) {
                    //даем дедушке награду за глубину
                    $parent_inviter->giveMoney(config('money.'.$inviter->tariff->caption.'.REWARD_FOR_REFERRAL_DEPTH.'.$depth), 'REWARD_FOR_REFERRAL_DEPTH');
                }
            }
        }
        //пробуем двигать спонсора по столам.
        $availableSponsor->tryToMoveToDesks();

        return $this;
    }

    public function firstAvailable()
    {
        // Even if there were 10K users it wouldn't affect performance. It _might_
        // consume some RAM, but the operation is very rare, so it's OK.
        //сортируем пользвоателей по дате оплаты, т.к в этом порядке они должны попадать в дерево
        // We must order users by the `payment_date` because *it is* the order
        // by which they get into the tree.
        $allUsers = static::orderBy('payment_date')->get();

        return (new Tree($this, $allUsers))->firstAvailable();
    }

    //подбираем в соответствии с тарифом
    public function firstAvailableForTariff($tariff_id)
    {
        // Even if there were 10K users it wouldn't affect performance. It _might_
        // consume some RAM, but the operation is very rare, so it's OK.
        //сортируем пользвоателей по дате оплаты, т.к в этом порядке они должны попадать в дерево
        // We must order users by the `payment_date` because *it is* the order
        // by which they get into the tree.
        $allUsers = static::whereTariffId($tariff_id)->orderBy('payment_date')->get();

        return (new Tree($this, $allUsers))->firstAvailable();
    }

    /**
     * Returns the user with the given login or `null`.
     *
     * @param string $login
     * @return User
     */
    public static function byLogin($login)
    {
        return static::whereLogin($login)->first();
    }
    


    /**
     * @returns User
     */
    public function grantAdminPrivileges()
    {
        if (! $this->is_admin) {
            $this->is_admin = true;
            $this->save();
        }

        return $this;
    }

    /**
     * @returns User
     */
    public function grantBen()
    {
        if (! $this->is_ben) {
            $this->is_ben = true;
            $this->save();
        }

        return $this;
    }

    /**
     * @return User
     * @throws UserIsAlreadyPaidException
     */
    
    //помечаем как оплаченного
    public function markAsPaid()
    {
        DB::transaction(function () {
            //если уже оплачен, то выдаем иключение
            if ($this->hasPaid()) {
                throw new UserIsAlreadyPaidException;
            }
            //задаем дату оплаты
            $this->payment_date = Carbon::now();
            //если указан пригласитель
            if ($this->desired_sponsor_id) {

                // We must make a new instance of user because if we search
                // it statically (like `User::find()`) in the context of
                // \App\Reinvest, it returns an instance of Reinvest
                // instead of User.
                //ищем объект спонсора в БД
                $desiredSponsor = (new self)->find($this->desired_sponsor_id);
                //привязываем пользователя к спонсору: начисляем бонусы, двигаем по столам и т.п.
                $this->follow($desiredSponsor);
            }
            //сохраняем пользователя
            $this->save();
        });

        return $this;
    }

    public function isAdmin()
    {
        return (bool) $this->is_admin;
    }

    public function isBen()
    {
        return (bool) $this->is_ben;
    }

    public function hasPaid()
    {
        return (bool) $this->payment_date;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sponsor()
    {
        return $this->belongsTo(self::class, 'sponsor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inviter()
    {
        return $this->belongsTo(self::class, 'inviter_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(self::class, 'owner_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function desk()
    {
        return $this->belongsTo(Desk::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tariff()
    {
        return $this->belongsTo(Tariff::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function bankRequisites()
    {
        return $this->hasMany(BankRequisite::class);
    }

    /**
     * Все личники пользователя (связь через поле пригласитель), без реинвестов.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invited()
    {
        return $this->hasMany(self::class, 'inviter_id')->onlyReal();
    }

    /**
     * Все оплатившие личники пользователя (связь через поле пригласитель), без реинвестов.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invitedonlypaid()
    {
        return $this->hasMany(self::class, 'inviter_id')->onlyReal()->onlyPaid();
    }

    /**
     * все рефералы (связь через поле спонсор)
     * It's not invited users, it's members of the user's triplet.
     * If you want to get all users who chose this user as their
     * sponsor during registration, use the `invited` relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function referrals()
    {
        return $this->hasMany(self::class, 'sponsor_id');
    }

    public function getDeskMatesAttribute()
    {
        $deskMates = $this->allDeskMates->filter(function ($mate) {
            return $mate->id != $this->id;
        })->values();

        if ($this->desk_position == 1) {
            return $deskMates;
        }

        if ($this->desk_position == 2) {
            return $deskMates->slice(2, 2)->values();
        }

        if ($this->desk_position == 3) {
            return $deskMates->slice(4, 2)->values();
        }

        return collect([]);
    }

    /**
     * Пользователи, которые находятся на том же столе,исключая самого пользователя
     * Users that sit at the same desk with the current user
     * excluding the user itself.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function allDeskMates()
    {
        return $this
            ->hasMany(self::class, 'desk_id', 'desk_id')
            ->orderBy('desk_position');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reinvests()
    {
        return $this->hasMany(Reinvest::class, 'owner_id');
    }


    public function lastreinvest()
    {
        return $this->reinvests->last();
    }



    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function __toString()
    {
        return trim("{$this->first_name} {$this->last_name}");
    }

    public function totalMoney()
    {
        $totalMoney = $this->transactions()
            ->where('status', '!=', 'fail')
            ->sum('sum');

        return $totalMoney;
    }

    public function showSponsor()
    {
        // TODO Felix, здесь производится двойной запрос к БД, нужно переделать в один

        if ($this->sponsor()->first()) {
            return $this->sponsor()->first()->login;
        }
    }

    public function getRequisites()
    {
        return BankRequisite::whereUserId($this->id)->get();
    }

    /**
     * Если у пользователя есть 3 реферала, то он будет помещен в стол
     * If this user has enough referrals he will be moved to the desks.
     */
    public function tryToMoveToDesks()
    {
        //проверяем количество рефералов
        if ($this->referrals()->count() == config('money.'.$this->tariff->caption.'.QUANTITY_FOR_GO_TO_TABLE')) {
            //двигаем пользователя в стол
            $this->moveToDesks();
        }
    }

    /**
     * добавляет пользователя на стол уровня 1
     *
     * @return $this
     */
    public function moveToDesks()
    {
        //ищем доступный стол первого уровня заданного тарифа
        //$desk = Desk::getAvailableForLevel(1);
        $desk = Desk::getAvailableForLevelTariff(1,$this->tariff_id);
        //добавляем нового члена к столу
        $desk->addMember($this);
        //выставляем порядок в столе для данного человека: он будет равен количеству людей в столе
        $this->desk_order = self::fromdesktariff($this->tariff_id)->count();
        //соханяем
        $this->save();

        return $this;
    }

    private function placeToTree(User $targetSponsor)
    {
        $this->sponsor()->associate($targetSponsor);

        return $this;
    }

    public function buildSchema(User $user)
    {
        $referrals = [];

        foreach ($user->referrals as $referral) {
            $referrals += $this->buildSchema($referral);
        }

        return [$user->login => $referrals];
    }

    public static function report()
    {
        $result = [
            1 => self::fromDesk(1)->pluck('login'),
            2 => self::fromDesk(2)->pluck('login'),
            3 => self::fromDesk(3)->pluck('login'),
            4 => self::fromDesk(4)->pluck('login'),
            5 => self::fromDesk(5)->pluck('login'),
            6 => self::fromDesk(6)->pluck('login'),
            7 => self::fromDesk(7)->pluck('login'),
        ];

        print json_encode($result, JSON_PRETTY_PRINT);
    }
}
