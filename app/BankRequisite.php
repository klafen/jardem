<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\BankRequisite.
 *
 * @property int $id
 * @property string $bank_requisite
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\BankRequisite whereBackAccount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BankRequisite whereBankRequisite($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BankRequisite whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BankRequisite whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BankRequisite whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @method static \Illuminate\Database\Query\Builder|\App\BankRequisite whereUserId($value)
 * @property string $bank_account
 * @method static \Illuminate\Database\Query\Builder|\App\BankRequisite whereBankAccount($value)
 * @property string $first_name
 * @property string $last_name
 * @method static \Illuminate\Database\Query\Builder|\App\BankRequisite whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BankRequisite whereLastName($value)
 */
class BankRequisite extends Model
{
    public function getSafeBankAccountAttribute()
    {
        $digitsStart = substr($this->bank_account, 0, 4);
        $digitsEnd = substr($this->bank_account, -4);

        $hiddenDigitsCount = strlen($this->bank_account) - 8;

        // Create middle path of string digits like '**** **** ****'

        $middle = [];

        for ($i = 0; $i < $hiddenDigitsCount / 4; $i++) {
            $middle[] = '****';
        }

        return $digitsStart.' '.implode(' ', $middle).' '.$digitsEnd;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function __toString()
    {
        return trim("{$this->first_name} {$this->last_name}");
    }
}
