<?php

namespace App\Depozit;

use App\User;
use DB;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Depozit\Desk.
 *
 * @property int $id
 * @property int $level
 * @property bool $closed
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $tariff_id
 * @property-read \App\Depozit\Tariff $tariff
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $members
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Depozit\DeskHistory[] $histories
 * @method static \Illuminate\Database\Query\Builder|\App\Depozit\Desk whereClosed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Depozit\Desk whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Depozit\Desk whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Depozit\Desk whereLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Depozit\Desk whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Depozit\Desk whereTariffId($value)
 * @mixin \Eloquent
 */
class Desk extends Model
{


    //поиск доступного стола заданного уровня для заданного тарифа
    public static function getAvailableForLevelTariff($level, $tariff_id)
    {
        //запрос на поиск стола
        $desk = static
            ::whereLevel($level) //задаем уровень
                ->whereTariffId($tariff_id) //задаем тариф
            ->orderBy('id') //сортируем по айди
            ->whereClosed(false) //берем незакрытые
            ->first(); //возвращаем первый попавшийся
       // return $desk ?: static::createForLevel($level);
        //если такого не найдено, то создадим новый заданного уровня и тарифа
        return $desk ?: static::createForLevelTariff($level, $tariff_id);
    }


    //создание стола заданного уровня и тарифа
    /**
     * @param $level
     * @return Desk
     */
    public static function createForLevelTariff($level, $tariff_id)
    {
        $desk = new static;
        $desk->level = $level;
        $desk->tariff_id=$tariff_id;
        $desk->closed = false;
        $desk->save();

        return $desk;
    }

    //добавление нового члена в стол
    public function addMember(User $member)
    {
        //проверяем соответствие уровня стола и круга пользователя
        $this->assertMemberCorrectness($member);


        DB::transaction(function () use ($member) {

            //собственно добавили члена
            $this->members()->save($member);
            //устанавливаем позицию пользователя в столе
            $member->desk_position = $this->members()->count();
            //сохраняем пользователя
            $member->save();
            //если членов в столе больше 7, то
            if ($this->members()->count() == 7) {
                //закрываем стол
                $this->close();
                //наградаем победителя за переход из стола в стол
                $this->processWinner();
                //надо перезагрузить членов, чтобы отдать предыдушие изменения коллекции
                // We must reload the `members` to reflect the previous
                // change in the collection.
                $this->load('members');
                //разделяем стол
                $this->split();
            }
        });

        return $this;
    }

    /**
     * This method isn't an equivalent of calling `addMember` for
     * every given user. This methods just adds the users to the
     * list and doesn't check is the table completed, should it
     * be split, etc.
     *
     * @param array $users
     */
    public function addMembers($users)
    {
        DB::transaction(function () use ($users) {
            foreach ($users as $user) {
                $this->addMember($user);
            }
        });
    }

    //проверка соответствия круга turn добавляемого пользователя и уровня level стола desk
    private function assertMemberCorrectness(User $member)
    {
        if ($this->level == 6 && $member->turn == 1) {
            throw new IncorrectTurnException(
                'You can not add a first turn member to a sixth level desk.'
            );
        }
    }

    private function close()
    {
        $this->createHistory();
        //ставим флаг на закрытие
        $this->closed = true;

        $this->save();
    }

    private function createHistory()
    {
        //для каждого пользователя делаем соотвутстующую запись
        foreach ($this->members as $member) {
            $deskhistory= new DeskHistory();
            $deskhistory->desk_id = $this->id;
            $deskhistory->user_id = $member->id;
            $deskhistory->position = $member->desk_position;
            $deskhistory->level = $this->level;
            $deskhistory->save();
        }

        $this->save();
    }

    private function nextTable()
    {
        return static::getAvailableForLevelTariff($this->level + 1,$this->tariff_id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function members()
    {
        return $this->hasMany(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function histories()
    {
        return $this->hasMany(DeskHistory::class, 'desk_id');
    }

    //процедура награждения пользователя-победителя за переход из стола в стол
    private function processWinner()
    {
        //определяем пользователя
        /** @var User $winner */
        $winner = $this->members->where('desk_position', 1)->first();

        //зануляем его позицию в столе
        $winner->desk_position = null;
        
        //выдаем награду за переход из стола в стол (ОТКЛЮЧЕНО за ненадобностью)
       // $this->rewardMember($winner);


        //еесли уровень стола равен максимально возможному количеству столов, то
        if ($this->level == config('money.'.$winner->tariff->caption.'.MAX_DESCS')) {
            //убираем его из стола нафиг
            $winner->desk_id = null;
            $winner->save();
            //даем награду в соответствии с количеством оплативших личников
            $invitedcount=$winner->invited()->onlyPaid()->count();

            //TODO если бенефециар и тариф не миллион, то даем бонус тому, кто под него регистрировал рефералов
            if ($winner->is_ben && $winner->tariff_id != 3) {
                //перебираем тех, кто стоит под бенефициаром
                foreach ($winner->referrals as $referral){
                    //если указанный на форме регистрации спонсор не равен пригласителю этого рефферала
                    //значит пригласитель целенаправленно регал под бенефициара
                    if ($referral->desired_sponsor_id != $referral->inviter_id) {
                        //значит надо выдавать бонус пригласителю этого рефералла бенефициара
                        $inviter=$referral->inviter;
                        $inviter->giveMoney(config('money.'.$winner->tariff->caption.'.REWARD_FOR_BEN_COMPLETE'), 'REWARD_FOR_BEN_COMPLETE');
                    }
                   
                }
            }

            //если реинвест, то награждать будет хозяина реинвеста
            if ($winner->is_reinvest) {
                $winner->owner;
            }
            //если больше 30 личников
            if ($invitedcount>=30) {
                $winner->giveMoney(config('money.'.$winner->tariff->caption.'.REWARD_FOR_MAX_DESCS.30'), 'REWARD_FOR_MAX_DESCS');
            }
            elseif ($invitedcount>=10) {
                $winner->giveMoney(config('money.'.$winner->tariff->caption.'.REWARD_FOR_MAX_DESCS.10'), 'REWARD_FOR_MAX_DESCS');
            }
            elseif ($invitedcount>=3) {
                $winner->giveMoney(config('money.'.$winner->tariff->caption.'.REWARD_FOR_MAX_DESCS.3'), 'REWARD_FOR_MAX_DESCS');
            }
            
            //даем реивест победителю
            $winner->giveReinvests(1);
        }
        //иначе - надо двигать по столам
        else {
            $this->nextTable()->addMember($winner);
        }
        /*
        //если уровень стола равен 5 и пользователь - реинвест, то
        if ($this->level == 5 && $winner->is_reinvest) {
            //деактивируем стол
            // When reinvest finishes the fifth level, it must be deactivated.
            $winner->desk()->dissociate();
            $winner->save();
        } elseif ($this->level == 5 && $winner->turn == 1) { //иначе, если уровень стола 5 и победитель на первом круге, то
            $winner->goToNextTurn(); //переводим его на следующий круг

            static::getAvailableForLevel(4)->addMember($winner); //помещаем пользователя в стол 4 уровня
        } elseif ($this->level == 5 && $winner->turn == 3) { //иначе, если уровень стола 5 и круг пользователя 4, то
            static::getAvailableForLevel(4)->addMember($winner); //помещаем пользователя в стол 4 уровня
        } elseif ($this->level == 6) { //иначе, если уровень стола = 6, то
            $winner->goToNextTurn(); //переводим победителя на следующий круг
            static::getAvailableForLevel(4)->addMember($winner); //помещаем пользователя в стол 4 уровня
        } else { //иначе
            $this->nextTable()->addMember($winner); //- добавляем нового члена к столу следующего уровня
        }*/
    }

    /**
     * Когда пользователь переходит на новый стол, то нужно будет выплачивать ему награду
     * When member goes to the next desk, this desk must
     * reward him with an appropriate amount of money.
     *
     * @param User $member
     */
    private function rewardMember(User $member)
    {
        if ($this->level == 1) {
            $member->giveMoney(config('money.REWARD_FOR_LEVEL_1_COMPLETED'), 'REWARD_FOR_LEVEL_1_COMPLETED');
        }

        if ($this->level == 2) {
            $member->giveMoney(config('money.REWARD_FOR_LEVEL_2_COMPLETED'), 'REWARD_FOR_LEVEL_2_COMPLETED');
        }

        if ($this->level == 3) {
            $member->giveMoney(config('money.REWARD_FOR_LEVEL_3_COMPLETED'), 'REWARD_FOR_LEVEL_3_COMPLETED');
        }

        if ($member->turn == 1) {
            if ($this->level == 4) {
                $member->giveMoney(config('money.REWARD_FOR_LEVEL_4_COMPLETED_1_TURN'), 'REWARD_FOR_LEVEL_4_COMPLETED_1_TURN');
            }

            if ($this->level == 5) {
                $member->giveMoney(config('money.REWARD_FOR_LEVEL_5_COMPLETED_1_TURN'), 'REWARD_FOR_LEVEL_5_COMPLETED_1_TURN');
            }
        }

        if ($member->turn == 2) {
            if ($this->level == 4) {
                $member->giveMoney(config('money.REWARD_FOR_LEVEL_4_COMPLETED_2_TURN'), 'REWARD_FOR_LEVEL_4_COMPLETED_2_TURN');
            }

            if ($this->level == 5) {
                $member->giveMoney(config('money.REWARD_FOR_LEVEL_5_COMPLETED_2_TURN'), 'REWARD_FOR_LEVEL_5_COMPLETED_2_TURN');
            }

            if ($this->level == 6) {
                $member->giveMoney(config('money.REWARD_FOR_LEVEL_6_COMPLETED'), 'REWARD_FOR_LEVEL_6_COMPLETED');
            }
        }

        if ($member->turn == 3) {
            if ($this->level == 4) {
                $member->giveMoney(config('money.REWARD_FOR_LEVEL_4_COMPLETED_3_TURN'), 'REWARD_FOR_LEVEL_4_COMPLETED_3_TURN');
            }

            if ($this->level == 5) {
                $member->giveMoney(config('money.REWARD_FOR_LEVEL_5_COMPLETED_3_TURN'), 'REWARD_FOR_LEVEL_5_COMPLETED_3_TURN');
            }
        }
    }

    /**
     * Создает два новых стола того же уровня и разделяет членов текущего стола между двумя новыми
     *
     * Creates two new desks of the same level and splits members of
     * the current desk between the two new.
     */
    private function split()
    {
        //первый стол
        $desk1 = $this->createForLevelTariff($this->level,$this->tariff_id);
        //второй стол
        $desk2 = $this->createForLevelTariff($this->level,$this->tariff_id);
        //сортируем членов по позиции в столе
        $members = $this->members->sortBy('desk_position')->values();
        //добаляем членов в первый стол
        $desk1->addMembers(collect([
            $members->get(0),
            $members->get(2),
            $members->get(3),
        ]));
        //добавляем членов во второй стол
        $desk2->addMembers(collect([
            $members->get(1),
            $members->get(4),
            $members->get(5),
        ]));
    }
}

