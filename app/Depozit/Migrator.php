<?php

namespace App\Depozit;

use App;
use App\Transaction;
use App\User;
use DB;

class Migrator
{
    public function migrate()
    {
        // Clean up Nadezhda
        DB::update('UPDATE users SET payment_date = null');

        $this->migrateRoot();

        if (! App::environment('testing') && ! env('CI')) {
            $this->cleanUpSponsorId();

            $tree = json_decode(get_users_tree_json(), true);

            $this->processBranch($tree);

            DB::delete('DELETE FROM desks');
            DB::update('UPDATE users SET desk_id = null');
            DB::update('UPDATE users SET desk_position = null');
            DB::update('UPDATE users SET desk_order = null');

            $this->cleanUpDeskRewards();

            foreach (get_desks_order() as $login) {
                if (! User::byLogin($login)) {
                    dd($login);
                }

                User::byLogin($login)->tryToMoveToDesks();
            }
        }
    }

    protected function cleanUpSponsorId()
    {
        DB::update('UPDATE users SET desired_sponsor_id = sponsor_id');
        DB::update('UPDATE users SET sponsor_id = null');
    }

    protected function migrateRoot()
    {
        $root = User::root();
        $root->priority = 0;
        $root->level = 0;
        $root->save();
        $root->markAsPaid();

        return $root;
    }

    protected function processBranch($branch, User $sponsor = null)
    {
        foreach ($branch as $login => $childBranch) {
            $user = User::byLogin($login);

            if (! $user) {
                dd($login);
            }

            $user->payment_date = \Carbon\Carbon::now();
            $user->save();

            if ($sponsor) {
                $user->follow($sponsor);
            }

            $this->processBranch($childBranch, $user);
        }
    }

    protected function showNotExistentUsersFromDb($tree)
    {
        $notExistentUsers = [];

        foreach (get_logins_from_tree($tree) as $login) {
            if (! User::whereLogin($login)->exists()) {
                $notExistentUsers[] = $login;
            }
        }

        dd($notExistentUsers);
    }

    private function showNotExistentUsersFromTree($tree)
    {
        $treeLogins = [];
        $notExistentUsers = [];

        foreach (get_logins_from_tree($tree) as $login) {
            $treeLogins[] = strtolower($login);
        }

        foreach (User::all()->pluck('login') as $login) {
            if (! in_array(strtolower($login), $treeLogins)) {
                $notExistentUsers[] = $login;
            }
        }

        dd($notExistentUsers);
    }

    private function cleanUpDeskRewards()
    {
        Transaction::where('description', '!=', 'REWARD_FOR_REFERRAL')->delete();
    }
}

function get_logins_from_tree($tree)
{
    $users = [];

    foreach ($tree as $login => $childTree) {
        $users[] = $login;

        foreach (get_logins_from_tree($childTree) as $childLogin) {
            $users[] = $childLogin;
        }
    }

    return $users;
}

function get_users_tree_json()
{
    return '
    {
  "office": {
    "Lana": {
      "Lana38": {
        "Pikap": {},
        "Verba": {},
        "Sirius": {}
      },
      "SW999": {
        "Ildana": {
          "Lori": {
            "Maestro": {
              "Lider": {
                "Talgat": {
                  "Borken": {
                    "Lyuba": {
                      "Lyubov": {},
                      "Lyba": {},
                      "Lada": {}
                    },
                    "Aisly1819201": {
                      "Aisulu": {},
                      "Buket": {},
                      "Sulu": {}
                    },
                    "Vorken": {}
                  },
                  "Gumar": {
                    "Aibat100": {
                      "Aibat101": {},
                      "Aibat102": {},
                      "Aibat103": {}
                    }
                  },
                  "David6": {}
                },
                "Shaina": {
                  "Zulfiya73": {
                    "Ersen92": {},
                    "Zliha94": {},
                    "Zarina97": {}
                  },
                  "Muza": {
                    "Mars": {},
                    "Pesnya": {},
                    "Saturn": {}
                  },
                  "Kabdulla": {}
                },
                "Legenda": {
                  "Apple": {},
                  "alena": {},
                  "zagisha": {}
                }
              },
              "Leha": {
                "Pobeda": {
                  "Luna": {
                    "Alexan": {
                      "Bakyt": {},
                      "Aiya": {},
                      "Marat": {}
                    },
                    "Raksha": {
                      "Raihan": {},
                      "Roza": {
                        "Asko": {},
                        "Zarina": {
                          "Hont": {},
                          "Bus": {},
                          "Tont": {}
                        },
                        "Aziza": {}
                      },
                      "Venera": {}
                    },
                    "Arsen": {
                      "Guka": {
                        "Aizhan": {
                          "Kapiza": {},
                          "Mira": {},
                          "Erkin": {
                            "Aiua": {
                              "Aisana": {},
                              "Ildar": {}
                            }
                          }
                        },
                        "Amoha": {},
                        "Zhakonyu": {}
                      },
                      "Udacha": {},
                      "Radost": {}
                    }
                  },
                  "Luna7": {},
                  "Luna8": {}
                },
                "Altynai": {},
                "Finish": {
                  "Artist": {},
                  "Tur": {},
                  "Berkut": {}
                }
              },
              "Diana": {
                "Altai": {
                  "Saule": {},
                  "Tau": {},
                  "Gory": {}
                },
                "Doni": {},
                "Wiking": {
                  "Silver": {},
                  "Evro": {},
                  "Golivud": {}
                }
              }
            },
            "Kabgul": {
              "Narsha": {
                "Rubin": {},
                "Kristall": {},
                "Fiona": {}
              },
              "Uspech": {
                "Magnat": {},
                "Pavlodar": {},
                "Bagira": {
                  "Sapfir": {},
                  "Pegas": {},
                  "Mustang": {}
                }
              },
              "Ainash": {
                "Izumrud": {
                  "Aliman": {},
                  "nargiz": {},
                  "zvezda": {}
                },
                "Daniya": {
                  "Diko": {}
                },
                "Dollar": {
                  "Kadisha": {
                    "Aigul65": {}
                  }
                }
              }
            },
            "Iskra": {
              "Luch": {},
              "Solnce": {},
              "Irbis": {}
            }
          },
          "Graff": {
            "AlmaLedi": {
              "Bota": {
                "Poli": {},
                "Agat": {},
                "Aigul": {}
              },
              "Limon": {
                "Apelsin": {},
                "Lir": {},
                "Korol": {}
              },
              "Ledi": {}
            },
            "Lord": {},
            "Gorod": {}
          },
          "Start": {
            "Danel": {
              "Gold": {},
              "Leto": {},
              "Strela": {}
            },
            "Baks": {},
            "Tenge": {}
          }
        },
        "more": {},
        "SO777": {}
      },
      "Lana21": {}
    }
  }
}
    ';
}

function get_desks_order()
{
    return [
        'Lana',
        'Lana38',
        'SW999',
        'Ildana',
        'Lori',
        'Graff',
        'Kabgul',
        'Maestro',
        'Lider',
        'Leha',
        'Talgat',
        'Pobeda',
        'Alexan',
        'AlmaLedi',
        'Borken',
        'Lyuba',
        'Aisly1819201',
        'Raksha',
        'Luna',
        'Uspech',
        'Bagira',
        'Ainash',
        'Roza',
        'Diana',
        'Arsen',
        'Zulfiya73',
        'Narsha',
        'Zarina',
        'Altai',
        'Bota',
        'Danel',
        'Aizhan',
        'Iskra',
        'Limon',
        'Start',
        'Wiking',
        'Aibat100',
        'Muza',
        'Finish',
        'Shaina',
        'Guka',
        'Legenda',
        'Izumrud',
    ];
}