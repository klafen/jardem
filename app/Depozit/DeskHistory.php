<?php

namespace App\Depozit;

use App\User;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * App\Depozit\DeskHistory.
 *
 * @property int $id
 * @property int $desk_id
 * @property int $user_id
 * @property int $position
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $level
 * @property-read \App\Depozit\Desk $desk
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Depozit\DeskHistory whereUser_id($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Depozit\DeskHistory wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Depozit\DeskHistory whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Depozit\DeskHistory whereLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Depozit\DeskHistory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DeskHistory extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}

