<?php

namespace App\Depozit;

use App\User;
use Illuminate\Support\Collection;

class Tree
{
    /**
     * @var User
     */
    private $rootUser;

    /** @var Collection */
    private $otherUsers = null;

    public function __construct(User $rootUser, Collection $otherUsers)
    {
        $this->rootUser = $rootUser;

        $this->otherUsers = $otherUsers;
    }

    public function firstAvailable()
    {
        $triplets = $this->getTriplets();
        $rootLevel = $this->getRootLevel();

        return $this->processLevel($rootLevel, $triplets);
    }

    private function processLevel($currLevel, Collection $triplets)
    {
        foreach($currLevel as $user) {
            $followers = $triplets->get($user->id, collect());

            if ($followers->count() < config('money.'.$this->rootUser->tariff->caption.'.QUANTITY_FOR_GO_TO_TABLE')) {
                return $user;
            }
        }

        $nextLevel = $this->getNextLevel($currLevel, $triplets);

        return $this->processLevel($nextLevel, $triplets);
    }

    private function getNextLevel(Collection $currLevel, Collection $triplets)
    {
        $nextLevel = collect([]);

        foreach($currLevel as $user) {
            foreach($triplets->get($user->id, collect()) as $follower) {
                $nextLevel->push($follower);
            }
        }

        return $nextLevel;
    }

    private function getRootLevel()
    {
        return collect([$this->rootUser]);
    }

    private function getTriplets()
    {
        return $this->otherUsers->groupBy('sponsor_id');
    }
}