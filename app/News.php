<?php

namespace App;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\News.
 *
 * @property int $id
 * @property string $slug
 * @property string $title
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\News whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\News whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $author_id
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Query\Builder|\App\News whereAuthorId($value)
 */
class News extends Model
{
    use CrudTrait;

    protected $fillable = ['slug', 'title', 'content'];

    /**
     * Returns the page with the given slug and fails with 404
     * if it doesn't exist.
     *
     * @param string $slug
     * @return Page
     */
    public static function bySlug($slug)
    {
        return static::whereSlug($slug)->firstOrFail();
    }

    public function author()
    {
        return $this->belongsTo(User::class);
    }
}
