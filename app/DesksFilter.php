<?php

namespace App;

use Request;

/**
 * A helper class to filter reinvests and render the filter
 * in HTMl on the user profile page.
 */
class DesksFilter
{
    private $baseQuery;

    private $getParam = 'desk';

    private $options = [
        '' => 'Общие данные',
        'tree' => 'Дерево',
        '1' => 'Стол 1',
        '2' => 'Стол 2',
        '3' => 'Стол 3',
        '4' => 'Стол 4',
    ];

    public function __construct($baseQuery)
    {
        $this->baseQuery = $baseQuery;
    }

    public function url($option)
    {
        // We must drop the pagination when user changes the filter
        $parameters = array_merge(Request::except('page'), [$this->getParam => $option]);

        return '?'.http_build_query($parameters, '', '&');
    }

    public function optionValid()
    {
        return (! $this->hasQueryOption() || $this->has($this->getQueryOption()));
    }

    public function currentOption()
    {
        return $this->getQueryOption() ?: $this->getDefaultOption();
    }

    public function getOptions()
    {
        return collect($this->options);
    }

    public function omitOptions($options)
    {
        $this->options = $this->getOptions()->filter(function ($label, $value) use ($options) {
            return (! in_array($value, $options));
        });

        return $this;
    }

    public function getFilteredQuery()
    {
        $value = $this->currentOption();

        if ($value == 'tree') {
            return $this->baseQuery->fromTree();
        } elseif ($value) {
            return $this->baseQuery->fromDesk($value);
        }

        return $this->baseQuery;
    }

    private function getDefaultOption()
    {
        return $this->getOptions()->keys()->first();
    }

    private function getQueryOption()
    {
        return Request::get($this->getParam, null);
    }

    private function hasQueryOption()
    {
        return Request::has($this->getParam);
    }

    private function has($option)
    {
        return $this->getOptions()->has($option);
    }
}