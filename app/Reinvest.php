<?php

namespace App;

/**
 * App\Reinvest.
 *
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string $email
 * @property string $country
 * @property string $city
 * @property \Carbon\Carbon $date_of_birth
 * @property int $sponsor_id
 * @property string $first_name
 * @property string $last_name
 * @property string $bank_account
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property bool $is_admin
 * @property \Carbon\Carbon $payment_date
 * @property int $level
 * @property int $priority
 * @property int $desired_sponsor_id
 * @property int $desk_id
 * @property int $turn
 * @property int $is_reinvest
 * @property int $owner_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BankRequisite[] $bankRequisites
 * @property-read \App\Depozit\Desk $desk
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\User $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $referrals
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Reinvest[] $reinvests
 * @property-read \App\User $sponsor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Transaction[] $transactions
 * @method static \Illuminate\Database\Query\Builder|\App\User fromDesk($level)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereBankAccount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereDateOfBirth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereDesiredSponsorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereDeskId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereIsAdmin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereIsReinvest($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereOwnerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest wherePaymentDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest wherePriority($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereSponsorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereTurn($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $desk_order
 * @property int $desk_position
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $allDeskMates
 * @property-read mixed $desk_mates
 * @property-read mixed $position_in_desk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $invited
 * @method static \Illuminate\Database\Query\Builder|\App\User fromTree()
 * @method static \Illuminate\Database\Query\Builder|\App\User members()
 * @method static \Illuminate\Database\Query\Builder|\App\User onlyPaid()
 * @method static \Illuminate\Database\Query\Builder|\App\User onlyReal()
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereDeskOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reinvest whereDeskPosition($value)
 */
class Reinvest extends User
{
    protected $table = 'users';

    /**
     * Use this method instead of `Reinvest::create()` or `new Reinvest`.
     *
     * @param User $user
     * @return Reinvest
     */
    public static function makeFor(User $user)
    {
        $number = $user->reinvests()->count() + 1;

        $reinvest = new static;
        $reinvest->owner_id = $user->id;
        $reinvest->is_reinvest = true;
        //пригласитель - корневая учетка
        $reinvest->inviter_id = User::root()->id;
        //указанный спонсор - текущая учетка
        $reinvest->desired_sponsor_id = $user->id;
        //тариф - равен тарифу спонору
        $reinvest->tariff_id=$user->tariff_id;

        $reinvest->login = "{$user->login}.reinvest{$number}";
        $reinvest->email = "{$user->login}.reinvest{$number}@example.com";
        $reinvest->first_name = $user->__toString();
        $reinvest->last_name = "Reinvest #{$number}";
        $reinvest->password = bcrypt(str_random(40));

        $reinvest->save();
        $reinvest->markAsPaid();

        return $reinvest;
    }

    /**
     * Typically, we rely on Laravel to guess the foreign key
     * of a model. It takes the model classname, lowers it and
     * adds the `_id` suffix. So FK for the `User` model will be
     * `user_id`. For the `Reinvest` model it will be `reinvest_id`.
     *
     * But `Reinvest` isn't a real model, it's an extension of
     * `User`. So in reality it has the same FK as the `User`,
     * `user_id`. Laravel cannot automatically handle this case
     * and we must to define it manually.
     *
     * @return string
     */
    public function getForeignKey()
    {
        return 'user_id';
    }

    public function giveMoney($sum, $description = '')
    {
        $this->owner->giveMoney($sum, $description);

        return $this;
    }

    public function giveReinvests($count)
    {
        $this->owner->giveReinvests($count);

        return $this;
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }
}