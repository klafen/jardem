<?php

namespace App;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Page.
 *
 * @property int $id
 * @property string $slug
 * @property string $title
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Page whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Page whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Page whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Page whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Page whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Page extends Model
{
    use CrudTrait;

    protected $fillable = ['slug', 'title', 'content'];

    /**
     * Returns the page with the given slug and fails with 404
     * if it doesn't exist.
     *
     * @param string $slug
     * @return Page
     */
    public static function findBySlug($slug)
    {
        return static::whereSlug($slug)->firstOrFail();
    }
}
