<?php

namespace App;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

/**
 * App\Transaction.
 *
 * @property int $id
 * @property int $user_id
 * @property int $sum
 * @property string $description
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Transaction whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Transaction whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Transaction whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Transaction whereSum($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Transaction whereUserId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\Transaction withSort()
 * @property string $type
 * @method static \Illuminate\Database\Query\Builder|\App\Transaction whereType($value)
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Transaction outcome()
 * @property-read mixed $localized_description
 */
class Transaction extends Model
{
    use CrudTrait;

    public function scopeWithSort($query)
    {
        $transactionType = Input::get('transaction-type');

        if (! empty($transactionType)) {
            $sign = $transactionType == 'in' ? '>' : '<';
            $query->where('sum', $sign, 0);
        }
    }

    public function scopeOutcome($query)
    {
        return $query->where('sum', '<', 0);
    }

    public function getLocalizedDescriptionAttribute()
    {
        $langKey = "money.{$this->description}";

        return trans()->has($langKey) ? trans($langKey) : $this->description;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bankRequisite()
    {
        return $this->belongsTo(BankRequisite::class);
    }
}
