<?php

namespace App\Http\Requests\Admin;

use Backpack\CRUD\app\Http\Requests\CrudRequest;

class NewsStoreRequest extends CrudRequest
{
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => 'required|alpha_dash|unique:news,slug,'.$this->id,
            'content' => 'required',
        ];
    }
}