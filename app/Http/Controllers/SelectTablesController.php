<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Table;

class SelectTablesController extends Controller
{
    public function show()
    {
        $tables = Table::where('active', 1)->get();
        return view('auth.select_tables')->with('tables', $tables);
    }
}
