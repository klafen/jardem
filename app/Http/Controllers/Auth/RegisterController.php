<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = 'account/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Проверяем
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        /* Validation rules */
        $rules = [
            'login' => 'required|max:255|unique:users|regex:/^[a-z0-9_]+$/i',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'date_of_birth' => 'required|before:18 years ago',
            'tariff_id' => 'required|exists:tariffs,id',
            'sponsor_login' => 'exists:users,login,tariff_id,'.$data['tariff_id'].'|paid',
            'inviter' => 'exists:users,login,tariff_id,'.$data['tariff_id'].'|paid',
            'country' => 'required',
            'city' => 'required',
            'confirm_terms' => 'accepted',
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'skype' => 'nullable'
        ];

        $messages = [
            'required' => 'Поле :attribute обязательно для заполнения',
            'login.regex' => 'Логин может содержать только латинские буквы, цифры и знак подчеркивания',
            'unique' => 'Такой :attribute уже занят',
            'max' => 'Поле :attribute может содеражать не более :max символов',
            'min' => 'Поле :attribute должно содеражать минимум :min символов',
            'email' => 'Введите корректный адрес электронной почты',
            'sponsor_login.exists' => 'Логин спонсора введен неверно либо он находиться на другом пакете пожалуйста уточните пакет спонсора!',
            'sponsor_login.paid' => 'Спонсора с таким логином нет в нашей базе, либо он еще не оплатил вход. В таком случае оставьте поле пустым',
            'inviter.exists' => 'Логин пригласитель введен неверно либо он находиться на другом пакете пожалуйста уточните пакет пригласителя!',
            'inviter.paid' => 'Пригласителя с таким логином нет в нашей базе, либо он еще не оплатил вход. В таком случае оставьте поле пустым',
            'date_of_birth.before' => 'Регистрация доступна только лицам, достигшим 18 лет',
            'date_of_birth.required' => 'Необходимо ввести дату рождения',
            'confirm_terms.accepted' => 'Необходимо согласиться с правилами договора о сотрудничестве',
            'password.confirmed' => 'Значение полей "Пароль" и "Подтверждение пароля" должны совпадать',
        ];

        return Validator::make($data, $rules, $messages);
    }

    /**
     * Создание нового пользователя после регистрации
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = new User($data);

        $user->password = bcrypt($data['password']);
        //у всех пользователей должен быть спонсор. Если не указан, то цепляем к корневому спнсору - Office
        // All users must have a sponsor and if user hasn't chosen one
        // during registration, we use the default root sponsor - `office`.
        $sponsor_login = $data['sponsor_login'] ?: 'office';
        //Аналогично с пригласителем. Если не указан, то будет равен спонсорскому логину
        $invite_login = $data['inviter'] ?: $sponsor_login;



        //если у пригласителя есть реинвесты, то надо цеплять к ним
        $inviter=User::byLogin($invite_login);
        if ($inviter->reinvests->count()>0) {
            $inviter=$inviter->lastreinvest();
        }

        //аналогично и со спонсором
        //если у спонсора есть реинвесты, то надо цеплять к ним
        $sponsor=User::byLogin($sponsor_login);
        if ($sponsor->reinvests->count()>0) {
            $sponsor=$sponsor->lastreinvest();
        }


        $user->desired_sponsor_id = $sponsor->id;
        $user->inviter_id = $inviter->id;

        $user->save();

        return $user;
    }
}
