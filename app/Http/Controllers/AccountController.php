<?php

namespace App\Http\Controllers;

use App\BankRequisite;
use App\Depozit\Desk;
use App\Depozit\DeskHistory;
use App\DesksFilter;
use App\Transaction;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showPage($page)
    {
        return view('account.pages.'.$page, ['data' => Auth::user()]);
    }

    public function showReferralData(Request $request)
    {
        $mode='';
        $user_login=$request->get('user_login');
        $desk_level=$request->get('desk');
        
        
        if($user_login)  {
            $user=User::byLogin($user_login) ?: Auth::user();
        }
        else {
            $user=Auth::user();
        }
        //$user = Auth::user();
        $user->load('referrals');
        $user->load('allDeskMates');

        $filter = new DesksFilter($user->reinvests());

        $reinvests = $filter->getFilteredQuery()
            ->paginate(config('profile.reinvests_per_page'))
            ->appends(['reinvests' => $request->reinvests]);
        $desk=null;
        if ($desk_level) {
            if ($desk_level=='tree') {
                $mode='tree';
            }
            else {
                $mode='desk';
                $deskhistory=DeskHistory::wherePosition(1)->whereUser_id($user->id)->whereLevel($desk_level)->first();
                if ($deskhistory) {
                    $desk=Desk::whereId($deskhistory->desk_id)->first();
                }
            }


        }

        
        return view('account.pages.referral-data', [
            'user' => $user,
            'filter' => $filter,
            'reinvests' => $reinvests,
            'desk'=>$desk,
            'mode'=>$mode
        ]);
    }

    //это все приглашенные (свзь через поле пригласитель)
    public function showReferralInvitedUsers()
    {
        $referralUsers = Auth::user()->invited()->onlyPaid()->paginate(10);

        return view('account.pages.referral-invited-users', ['users' => $referralUsers]);
    }

    public function showHistory()
    {
        $data = Transaction::whereUserId(Auth::user()->id)
            ->orderBy('created_at', 'DESC')
            ->withSort()
            ->paginate(10);

        $data->appends(Input::get());

        return view('account.pages.history', ['data' => $data]);
    }

    public function showPaymentData()
    {
        $data = Auth::user()->getRequisites();

        return view('account.pages.payment-data', ['data' => $data]);
    }

    public function showCreateUpdateRequisiteForm()
    {
        $requisiteId = Input::get('id');

        if ($requisiteId) {
            $data = BankRequisite
                ::whereUserId(Auth::user()->id)
                ->findOrFail($requisiteId);

            return view('account.pages.update-requisite-form', ['data' => $data]);
        } else {
            return view('account.pages.create-requisite-form');
        }
    }

    public function createUpdateRequisite(Request $request)
    {
        $this->requisiteValidator($request);
        $requisite = BankRequisite::whereId($request->id)->first();

        if (! $requisite) {
            $requisite = new BankRequisite();
            $requisite->user_id = Auth::user()->id;
        }

        $requisite->first_name = $request->first_name;
        $requisite->last_name = $request->last_name;
        $requisite->bank_account = $request->bank_account;
        $requisite->bank_requisite = $request->bank_requisite;
        $requisite->save();

        return $this->showPaymentData();
    }

    public function updateProfile(Request $request)
    {
        $this->validateProfile($request);

        Auth::user()->update($request->input());

        return redirect('/account/profile');
    }

    public function withdrawFunds(Request $request)
    {
        $this->validateWithdrawal($request);

        $transaction = new Transaction();
        $transaction->sum = -$request->amount;
        $transaction->description = 'Вывод средств';
        $transaction->user_id = Auth::id();
        $transaction->bank_requisite_id = $request->bank_id;
        $transaction->status = 'wait';
        $transaction->save();

        return redirect('/account/withdrawals')->with(['transactionSuccess' => true]);
    }

    public function validateWithdrawal($request)
    {
        $availableMoney = Auth::user()->totalMoney();

        $rules = [
            'bank_id' => [
                'required',
                Rule::exists('bank_requisites', 'id')->where(function ($query) {
                    $query->whereUserId(Auth::id());
                }),
            ],

            'amount' => [
                'required',
                'numeric',
                'min:500',
                "max:{$availableMoney}",
            ],
        ];

        $messages = [
            'bank_id.required' => 'Вы должны указать банковский счёт для вывода',
            'amount.required' => 'Необходимо ввести сумму',
            'amount.numeric' => 'Сумма для вывода может содержать только цифры',
            'amount.min' => 'Выводимая сумма должна быть больше 500',
            'amount.max' => 'У вас недостаточно средств',
        ];

        $this->validate($request, $rules, $messages);
    }

    protected function requisiteValidator($request)
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'bank_account' => 'required',
            'bank_requisite' => 'required',
        ];

        $messages = [
            'first_name.required' => 'Поле "Имя" обязательно для заполнения',
            'last_name.required' => 'Поле "Фамилия" обязательно для заполнения',
            'bank_account.required' => 'Поле "Номер счета" обязательно для заполнения',
            'bank_requisite.required' => 'Поле "Название банка" обязательно для заполнения',
            'size' => 'Номер должен содержать 20 цифр',
        ];

        $this->validate($request, $rules, $messages);
    }

    protected function validateProfile($request)
    {
        $rules = [
            'country' => 'required',
            'city' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'phone' => 'required',
        ];

        $messages = [
            'country.required' => 'Необходимо указать страну',
            'city.required' => 'Необходимо указать город',
        ];

        $this->validate($request, $rules, $messages);
    }
}