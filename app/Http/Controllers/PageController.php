<?php

namespace App\Http\Controllers;

use App\Page;

class PageController extends Controller
{
    /**
     * Shows a single page for the given slug.
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        if ($slug == 'main') {
            return redirect('/');
        }

        return view('pages.show', [
            'page' => Page::findBySlug($slug),
        ]);
    }
}
