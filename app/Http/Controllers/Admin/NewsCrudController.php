<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\NewsStoreRequest;
use App\News;
use Auth;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class NewsCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(News::class);
        $this->crud->setRoute('admin/news');
        $this->crud->setEntityNameStrings('новость', 'Новости');

        $this->crud->setColumns([
            [
                'name' => 'title',
                'label' => 'Заголовок',
            ],
            [
                'name' => 'content',
                'label' => 'Содержимое',
            ],
            [
                'name' => 'slug',
                'label' => 'Ссылка',
            ],
        ]);

        $this->crud->addFields([
            ['name' => 'title', 'label' => 'Заголовок (title)'],
            ['name' => 'slug', 'label' => 'Ссылка (slug)'],
            ['name' => 'content', 'label' => 'Текст (content)', 'type' => 'wysiwyg'],
        ]);
    }

    public function store(NewsStoreRequest $request)
    {
        $response = parent::storeCrud();

        /** @var News $news */
        $news = $this->crud->entry;
        $news->author()->associate(Auth::id());
        $news->save();

        return $response;
    }

    public function update(NewsStoreRequest $request)
    {
        return parent::updateCrud();
    }
}