<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TransactionRequest as StoreRequest;
use App\Http\Requests\TransactionRequest as UpdateRequest;
use App\Transaction;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Support\Facades\Input;
use Request;

class TransactionCrudController extends CrudController
{
    public function setUp()
    {
        $this->crud->setModel(Transaction::class);
        $this->crud->setRoute(config('backpack.base.route_prefix').'/transaction');
        $this->crud->setEntityNameStrings('transaction', 'transactions');

        $this->crud->addClause('outcome');

        if (Request::get('status') == 'wait') {
            $this->crud->addClause('where', 'status', '=', 'wait');
        } else {
            $this->crud->addClause('where', 'status', '!=', 'wait');
        }

        $this->crud->addClause('orderBy', 'created_at', 'desc');

        if (Input::has('status')) {
            $this->crud->addButton('line', 'status', '', 'admin.transaction-decline-button');
            $this->crud->addButton('line', 'status', '', 'admin.transaction-approve-button');
        } else {
            $this->crud->removeAllButtons();
        }

        $this->crud->removeButton('update');
        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');

        $this->crud->setColumns(
            [
                [
                    'name' => 'user_id',
                    'type' => 'select',
                    'attribute' => 'login',
                    'entity' => 'user',
                    'label' => 'Логин пользователя',
                    'model' => User::class,
                ],
                [
                    'label' => 'Сумма',
                    'name' => 'sum',
                ],
                [
                    'label' => 'Плат. данные',
                    'name' => 'bankRequisite',
                    'type' => 'bank_requisite',
                ],
                [
                    'label' => 'Статус',
                    'name' => 'status',
                    'type' => 'transaction-icon',
                ],
                [
                    'label' => 'Дата',
                    'name' => 'created_at',
                ],
            ]
        );
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
