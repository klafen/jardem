<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\MaterialRequest as StoreRequest;
use App\Http\Requests\Admin\MaterialRequest as UpdateRequest;
use App\Material;

// VALIDATION: change the requests to match your own file names if you need form validation
use Auth;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class MaterialCrudController extends CrudController
{
    public function setUp()
    {
        $this->crud->setModel(Material::class);
        $this->crud->setRoute(config('backpack.base.route_prefix').'/material');
        $this->crud->setEntityNameStrings('материал', 'материалы');

        $this->crud->setColumns([
            [
                'name' => 'title',
                'label' => 'Заголовок',
            ],
            [
                'name' => 'content',
                'label' => 'Содержимое',
            ],
            [
                'name' => 'slug',
                'label' => 'Ссылка',
            ],
        ]);

        $this->crud->addFields([
            ['name' => 'title', 'label' => 'Заголовок (title)'],
            ['name' => 'slug', 'label' => 'Ссылка (slug)'],
            ['name' => 'content', 'label' => 'Текст (content)', 'type' => 'wysiwyg'],
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();

        /** @var Material $material */
        $material = $this->crud->entry;
        $material->author()->associate(Auth::id());
        $material->save();

        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
