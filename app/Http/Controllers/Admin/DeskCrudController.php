<?php

namespace App\Http\Controllers\Admin;

use App\DesksFilter;
use App\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class DeskCrudController extends CrudController
{
    public function index()
    {
        $filter = (new DesksFilter($this->crud->query))->omitOptions(['','tree']);

        if (! $filter->optionValid()) {
            return redirect('/admin/desk');
        }

        $this->crud->query = $filter->getFilteredQuery();
        return parent::index()->with('filter', $filter);
    }

    public function setup()
    {
        $this->crud->access = ['list', 'update', 'delete'];
        $this->crud->setModel(User::class);
        $this->crud->setRoute('admin/desk');
        $this->crud->setEntityNameStrings('стола', 'Столы');
        $this->crud->addButton('line', 'show-info', 'view', 'admin.show-info-button');

        $this->crud->removeButton('create');
        $this->crud->removeButton('update');
        $this->crud->removeButton('delete');

        $this->crud->setColumns([
            ['name' => 'login', 'label' => 'Логин'],
            ['label' => 'Стол', 'type' => 'select', 'name' => 'desk_id', 'entity' => 'desk', 'attribute' => 'level'],
            ['name' => 'email', 'label' => 'Email'],
            ['name' => 'sponsor', 'label' => 'Спонсор', 'type' => 'select', 'entity' => 'sponsor', 'attribute' => 'login'],
            ['name' => 'tariff', 'label' => 'Тариф', 'type' => 'select', 'entity' => 'tariff', 'attribute' => 'caption'],
            ['name' => 'first_name', 'label' => 'Имя'],
            ['name' => 'last_name', 'label' => 'Фамилия'],
            ['name' => 'created_at', 'label' => 'Дата регистрации'],
        ]);
        $this->crud->addFilter([ // dropdown filter
            'name' => 'tariff_id',
            'type' => 'dropdown',
            'label'=> 'Тариф'
        ], [
            1 => '25 000 тнг',
            2 => '75 000 тнг',
            3 => '1 000 000 тнг',
        ], function($value) { // if the filter is active
            $this->crud->addClause('where', 'users.tariff_id', $value);
        });
    }

    public function showDetailsRow($id)
    {
        $user = User::whereId($id)->first();

        return view('vendor.backpack.user.show', ['user' => $user]);
    }
}