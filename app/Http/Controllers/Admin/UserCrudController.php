<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\UserStoreRequest;
use App\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class UserCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->access = ['list', 'update', 'delete'];
        $this->crud->setModel(User::class);
        $this->crud->setRoute('admin/user');
        $this->crud->setEntityNameStrings('пользователя', 'пользователи');
        $this->crud->addButton('line', 'payment_date', 'time', 'admin.payment-button', 'end');
        $this->crud->addButton('line', 'show-info', 'view', 'admin.show-info-button');
        $this->crud->orderBy('created_at', 'desc');
        $this->crud->setColumns([
            ['name' => 'login', 'label' => 'Логин'],
            ['name' => 'email', 'label' => 'Email'],
            ['name' => 'sponsor', 'label' => 'Спонсор', 'type' => 'select', 'entity' => 'sponsor', 'attribute' => 'login'],
            ['name' => 'inviter', 'label' => 'Пригласитель', 'type' => 'select', 'entity' => 'inviter', 'attribute' => 'login'],
            ['name' => 'tariff', 'label' => 'Тариф', 'type' => 'select', 'entity' => 'tariff', 'attribute' => 'caption'],
            ['name' => 'first_name', 'label' => 'Имя'],
            ['name' => 'last_name', 'label' => 'Фамилия'],
            ['name' => 'created_at', 'label' => 'Дата регистрации'],
            ['name' => 'is_admin', 'label' => 'Админ', 'type' => 'Boolean'],
            ['name' => 'is_ben', 'label' => 'Бенеф.', 'type' => 'Boolean'],
        ]);

        $this->crud->addFields([
            ['name' => 'first_name', 'label' => 'Имя (first_name)'],
            ['name' => 'last_name', 'label' => 'Фамилия (last_name)'],
            ['name' => 'email', 'label' => 'Электронная почта (email)'],
            [
                'name' => 'password',
                'label' => 'Пароль профиля (password)',
                'type' => 'text', 'value' => '',
                'hint' => 'Если поле ввода пароля оставить пустым, пароль профиля не изменится при сохранении других изменений.
                 Чтобы изменить пароль, необходимо ввести минимум 6 символов.',
            ],
            [
                'name' => 'date_of_birth',
                'label' => 'Дата рождения',
                'type' => 'datetime_picker',

                'datetime_picker_options' => [
                    'format' => 'DD/MM/YYYY',
                ],
            ],
            ['name' => 'is_ben', 'type' => 'checkbox', 'label' => 'Бенефициар?'],
        ]);

        $this->crud->addFilter([ // dropdown filter
            'name' => 'tariff_id',
            'type' => 'dropdown',
            'label'=> 'Тариф'
        ], [
            1 => '25 000 тнг',
            2 => '75 000 тнг',
            3 => '1 000 000 тнг',
        ], function($value) { // if the filter is active
             $this->crud->addClause('where', 'tariff_id', $value);
        });

        $this->crud->addFilter([ // simple filter
            'type' => 'simple',
            'name' => 'payed',
            'label'=> 'Не оплатившие'
        ],
            false,
            function() { // if the filter is active
                $this->crud->addClause('where', 'payment_date', NULL);
            } );
        $this->crud->addFilter([ // simple filter
            'type' => 'simple',
            'name' => 'ben',
            'label'=> 'Бенефициары'
        ],
            false,
            function() { // if the filter is active
                $this->crud->addClause('where', 'is_ben', 1);
            } );
        $this->crud->addFilter([ // simple filter
            'type' => 'simple',
            'name' => 'desks',
            'label'=> 'Не в столах'
        ],
            false,
            function() { // if the filter is active
                $this->crud->addClause('where', 'desk_id', 0);
            } );
    }

    public function showDetailsRow($id)
    {
        $user = User::whereId($id)->first();

        return view('vendor.backpack.user.show', ['user' => $user]);
    }

    public function store(UserStoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UserStoreRequest $request)
    {
        $response = parent::updateCrud();

        $user = $this->crud->model->findOrFail($request->get('id'));

        // To prevent updating password via empty value
        if ($request->password) {
            $user->password = bcrypt($request->password);
        }

        if (! $user->hasPaid() && $request->is_paid) {
            $user->markAsPaid();
        }

        if (! $user->isBen() && $request->is_ben) {
            $user->grantBen();
        }

        $user->save();

        return $response;
    }

    public function destroy($id)
    {
        $user = $this->crud->model->findOrFail($id);
        //если есть рефералы
        if ($user->referrals()->count()>0) {
            //return($user->refferals->count());
            return  abort(403, trans('backpack::crud.unauthorized_access'));
        }
        else {
            $this->crud->hasAccessOrFail('delete');
            return $this->crud->delete($id);
        }

    }
}