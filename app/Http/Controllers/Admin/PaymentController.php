<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;

class PaymentController extends Controller
{
    public function approvePayment($id)
    {
        //берем пользователя
        $user = User::whereId($id)->first();
        //помечаем как оплаченного, здесь начинается самая веселуха
        $user->markAsPaid();

        return redirect()->back();
    }
}