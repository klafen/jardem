<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Table;
use Validator;

class TableController extends AdminMasterController
{
    
    public function index()
    {
        $tables = Table::all();
        return view("pages.tables.index")->with('tables', $tables);
    }

    
    public function create()
    {
        return $this->outputAdminView("pages.tables.create");
    }

    
    public function store(Request $request)
    {
        $data = $request->except('_token');

        $validator = Validator::make($data, [
            "name" => "required|string|min:5|max:200",
            "slug" => "required|string|alpha_num",
            "count" => "required|numeric",

            "count_home" => "required|numeric",
            "count_place_home" => "required|numeric",

            "y_lv_3" => "required|numeric",
            "y_lv_4" => "required|numeric",
            "y_lv_5" => "required|numeric",
            "y_lv_6" => "required|numeric",
            "y_lv_7" => "required|numeric",
            "y_lv_8" => "required|numeric",
            "y_lv_9" => "required|numeric",
            "y_lv_10" => "required|numeric",

            "description" => "required|string",
        ]);

        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($data);
        }

        $table = Table::create([
            "name" => $data['name'],
            "slug" => $data["slug"],
            "active" => isset($data["active"]) ? true : false,
            "count" => $data["count"],

            "count_home" => $data["count_home"],
            "count_place_home" => $data["count_place_home"],

            "y_lv_3" => $data["y_lv_3"],
            "y_lv_4" => $data["y_lv_4"],
            "y_lv_5" => $data["y_lv_5"],
            "y_lv_6" => $data["y_lv_6"],
            "y_lv_7" => $data["y_lv_7"],
            "y_lv_8" => $data["y_lv_8"],
            "y_lv_9" => $data["y_lv_9"],
            "y_lv_10" => $data["y_lv_10"],

            "description" => $data["description"],
        ]);

        return redirect("admin/tables")->with("success", "Стол успешно создан!");
    }

    
    public function show($id)
    {
        $table = Table::findOrfail($id);
        return $this->outputAdminView("pages.tables.info", "table", $table);
    }

    
    public function edit($id)
    {
        $table = Table::find($id);

        // dd($id);
        return $this->outputAdminView("pages.tables.edit", "table", $table);
    }

    
    public function update(Request $request, $id)
    {
        $data = $request->except("_token", "_method");

        // dd($data);

        $validator = Validator::make($data, [
            "name" => "required|string|min:5|max:200",
            "slug" => "required|string|alpha_num",
            "count" => "required|numeric",

            "count_home" => "required|numeric",
            "count_place_home" => "required|numeric",

            "y_lv_3" => "required|numeric",
            "y_lv_4" => "required|numeric",
            "y_lv_5" => "required|numeric",
            "y_lv_6" => "required|numeric",
            "y_lv_7" => "required|numeric",
            "y_lv_8" => "required|numeric",
            "y_lv_9" => "required|numeric",
            "y_lv_10" => "required|numeric",

            "description" => "required|string",
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($data);
        }

        $table = Table::findOrfail($id);

        $table->update([
            "name" => $data['name'],
            "slug" => $data["slug"],
            "active" => isset($data["active"]) ? true : false,
            "count" => $data["count"],

            "count_home" => $data["count_home"],
            "count_place_home" => $data["count_place_home"],

            "y_lv_3" => $data["y_lv_3"],
            "y_lv_4" => $data["y_lv_4"],
            "y_lv_5" => $data["y_lv_5"],
            "y_lv_6" => $data["y_lv_6"],
            "y_lv_7" => $data["y_lv_7"],
            "y_lv_8" => $data["y_lv_8"],
            "y_lv_9" => $data["y_lv_9"],
            "y_lv_10" => $data["y_lv_10"],

            "description" => $data["description"],
        ]);

        return redirect("admin/tables")->with("success", "Стол успешно обновлен!");
    }

    
    public function destroy($id)
    {

        $table = Table::findOrfail($id);

        if (count($table->users) > 0) {
            return redirect("admin/tables")->with("danger", "Стол не может быть удален!");
        }

        $table->delete();
        return redirect("admin/tables")->with("success", "Стол успешно удален!");
    }
}
