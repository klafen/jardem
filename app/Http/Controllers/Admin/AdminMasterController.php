<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;

class AdminMasterController extends Controller
{
    public function outputAdminView($viewName, $dataName = false, $data = false)
    {
        if ($dataName) {
            // dd($dataAndName);
            return View::make($viewName)->with($dataName, $data);
        }
        return View::make($viewName);
    }
}
