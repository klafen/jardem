<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PageStoreRequest;
use App\Page;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class PageCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Page::class);
        $this->crud->setRoute('admin/page');
        $this->crud->setEntityNameStrings('страницу', 'страницы');

        $this->crud->setColumns([
            [
                'name' => 'title',
                'label' => 'Страница',
            ],
            [
                'name' => 'content',
                'label' => 'Содержимое',
            ],
            [
                'name' => 'slug',
                'label' => 'Ссылка',
            ],
        ]);

        $this->crud->addFields([
            ['name' => 'title', 'label' => 'Заголовок (title)'],
            ['name' => 'slug', 'label' => 'Ссылка (slug)'],
            ['name' => 'content', 'label' => 'Текст (content)', 'type' => 'wysiwyg'],
        ]);
    }

    public function store(PageStoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(PageStoreRequest $request)
    {
        return parent::updateCrud();
    }
}