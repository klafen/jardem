<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Transaction;

class TransactionApprovementController extends Controller
{
    public function setTransactionStatus($transactionId, $status)
    {
        $transaction = Transaction::whereId($transactionId)->first();
        $transaction->status = $status;
        $transaction->save();

        return redirect()->back();
    }
}