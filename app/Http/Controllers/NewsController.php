<?php

namespace App\Http\Controllers;

use App\News;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::orderBy('created_at', 'DESC')
            ->paginate(9);

        return view('news.index', ['news' => $news]);
    }

    public function show($id)
    {
        $item = News::whereId($id)
            ->first();

        return view('news.show', ['item' => $item]);
    }
}