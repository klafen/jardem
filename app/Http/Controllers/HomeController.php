<?php

namespace App\Http\Controllers;

use App\Depozit\DeskHistory;
use App\Page;
use App\Transaction;
use App\User;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $users=User::orderBy('payment_date', 'DESC')->limit(20)->get();
        
        
        $deskhistories=DeskHistory::with('user')->orderBy('created_at', 'DESC')->wherePosition(1)->limit(20)->get();
        $transactions = Transaction::with('user')->whereDescription('REWARD_FOR_MAX_DESCS')->latest()->limit(20)->get();
        //View::share('transactions_marquee', $transactions);
        View::share('deskhistories', $deskhistories);
        View::share('users', $users);
        View::share('transactions', $transactions);
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home', [
           'mainPage' => Page::findBySlug('main'),
        ]);
    }
}
