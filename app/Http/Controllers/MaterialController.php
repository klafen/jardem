<?php

namespace App\Http\Controllers;

use App\Material;

class MaterialController extends Controller
{
    public function index()
    {
        $news = Material::orderBy('created_at', 'DESC')
            ->paginate(9);

        return view('news.index', ['news' => $news]);
    }

    public function show($id)
    {
        $item = Material::whereId($id)
            ->first();

        return view('news.show', ['item' => $item]);
    }
}