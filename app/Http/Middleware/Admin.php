<?php

namespace App\Http\Middleware;

use Auth;
use Backpack\Base\app\Http\Middleware\Admin as BackpackAdmin;
use Closure;

/**
 * We extend the basic Backpack Admin middleware to check
 * that the current user not only logged in but also is an
 * admin.
 */
class Admin extends BackpackAdmin
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check() && Auth::user()->isAdmin()) {
            return parent::handle($request, $next, $guard);
        }

        return redirect('/');
    }
}
