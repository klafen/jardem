<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\Http\Middleware\Admin;

Route::group(['middleware' => 'web'], function () {

    //Route::get('tables', ['uses' => 'SelectTablesController@show', 'as' => 'select.tables']);

    // Route::get('login25', ['uses' => 'Auth\LoginController@showLoginForm', 'as' => 'login25']);


    Auth::routes();

    Route::get('loginAs/{login}', function($login) {
        if (\Auth::user()->isAdmin()) {
            Auth::guard()->login(\App\User::byLogin($login));
        }

        return redirect('/');
    });

});

// $middleware = ['web'];

// if (env('DEMO_INSTANCE')) {
//     $middleware[] = 'auth';
// }

    Route::get('/', 'HomeController@index');

    Route::get('/news', ['as' => 'news','uses' => 'NewsController@index']);
    Route::get('/news/{id}', ['as' => 'news','uses' => 'NewsController@show']);

    Route::get('/materials', ['as' => 'materials','uses' => 'MaterialController@index']);
    Route::get('/materials/{id}', ['as' => 'materials','uses' => 'MaterialController@show']);



Route::group(['middleware' => ['web', "auth"]], function () {

    Route::get('account/referral-data', ['as' => 'referralData', 'uses' => 'AccountController@showReferralData']);

    Route::get('account/referral-invited-users', ['as' => 'referralInvitedUsers', 'uses' => 'AccountController@showReferralInvitedUsers']);

    Route::get('account/history', ['as' => 'history', 'uses' => 'AccountController@showHistory' ]);

    Route::get('account/payment-data', ['as' => 'paymentData', 'uses' => 'AccountController@showPaymentData' ]);

    Route::get('account/edit-requisite-form', ['as' => 'paymentData', 'uses' => 'AccountController@showCreateUpdateRequisiteForm' ]);

    Route::post('account/update-requisite', ['as' => 'updateRequisite', 'uses' => 'AccountController@createUpdateRequisite']);

    Route::post('account/update-profile', ['as' => 'updateProfile', 'uses' => 'AccountController@updateProfile']);

    Route::post('account/withdraw-funds', ['as' => 'withdrawFunds', 'uses' => 'AccountController@withdrawFunds']);

    Route::post('account/tree', ['as' => 'see.tree', 'uses' => 'AccountController@showTree']);

    Route::get('account/{page}', ['as' => 'account', 'uses' => 'AccountController@showPage']);
});

Route::group(['middleware' => ['web', 'auth', 'admin'], 'as' => 'admin.'], function() {
        
    Route::get('admin/transaction/{id}/{status}', ['as' => 'approvePayment', 'uses' => 'Admin\TransactionApprovementController@setTransactionStatus']);
    // Подтверждение платежа
    Route::get('admin/user/{id}/approve-payment', ['as' => 'approvePayment', 'uses' => 'Admin\PaymentController@approvePayment']);
    Route::post('admin/user/set-payment', ['as' => 'setPayment', 'uses' => 'Admin\PaymentController@setPayment']);

    CRUD::resource('admin/page', 'Admin\PageCrudController');
    CRUD::resource('admin/news', 'Admin\NewsCrudController');
    CRUD::resource('admin/user', 'Admin\UserCrudController');
    CRUD::resource('admin/transaction', 'Admin\TransactionCrudController');
    CRUD::resource('admin/material', 'Admin\MaterialCrudController');
    CRUD::resource('admin/desk', 'Admin\DeskCrudController');
    
    Route::resource('admin/tables', 'Admin\TableController');
    
});

/*
     * Если шаблон маршрута может отработать по статически закрепленному пути,
     * его следует размещать ниже. Иначе контроллер статического пути не отравботает,
     * а контроллер по шаблону выдаст 404
     */
Route::get('/{slug}', 'PageController@show');
