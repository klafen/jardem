<?php

use App\Depozit\Migrator;
use Illuminate\Database\Migrations\Migration;

class MigrateOldDbToTheNewStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        with(new Migrator)->migrate();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // ...
    }
}
