<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeUsersPasswords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Each user's password equal user's login
        foreach (['admin', 'office'] as $login){
            $user = User::where('login', '=', "$login")->first();
           if($user){
               $user->password = bcrypt($user->login);
               $user->save();
           }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
