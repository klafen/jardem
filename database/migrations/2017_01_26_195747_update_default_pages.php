<?php

use App\Page;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDefaultPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!App::environment('testing')) {
            Page::create([
                'title' => 'Marketing',
                'slug' => 'marketing',
                'content' => '<p>Marketing content</p>'
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Page::whereTitle('Marketing')->delete();
    }
}
