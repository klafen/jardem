<?php

use App\User;
use Illuminate\Database\Migrations\Migration;

class CreateOfficeUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        User::create([
            'email' => 'office@depozitgold.com',
            'login' => 'office',
            'password' => Hash::make('office'),
            'first_name' => 'Офис',
        ])->grantAdminPrivileges();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        User::whereLogin('office')->delete();
    }
}
