<?php

use App\Page;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Page::create([
            'title' => 'About',
            'slug' => 'about',
            'content' => '<p>About content</p>'
        ]);

        Page::create([
            'title' => 'Main',
            'slug' => 'main',
            'content' => '<p>Main content</p>'
        ]);

        Page::create([
            'title' => 'Contacts',
            'slug' => 'contacts',
            'content' => '<p>Contacts content</p>'
        ]);

        Page::create([
            'title' => 'Terms',
            'slug' => 'terms',
            'content' => '<p>Terms content</p>'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Page::whereTitle('About')->delete();
        Page::whereTitle('Main')->delete();
        Page::whereTitle('Contacts')->delete();
        Page::whereTitle('Terms')->delete();
    }
}
