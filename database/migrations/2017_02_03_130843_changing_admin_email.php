<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangingAdminEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!App::environment('testing')) {
            $admin = User::whereLogin('admin')->first();
            $admin->email = 'admin@jardem-c.com';
            $admin->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $admin = User::whereLogin('admin')->first();

        if ($admin) {
            // It's possible that admin user doesn't exist
            $admin->email = 'admin@gmail.com';
            $admin->save();
        }
    }
}
