<?php

use App\News;
use Faker\Factory;
use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    public function run()
    {
        News::truncate();

        $faker = Factory::create();
        foreach (range(1, 30) as $number) {
            News::create([
                'title' => 'Title ' . $number,
                'slug' => 'title-' . $number,
                'content' => $faker->text(500),
            ]);
        }
    }
}