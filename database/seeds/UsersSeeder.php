<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run()
    {
        $adminId = User::byLogin('admin')->id;
        $officeId = User::byLogin('office')->id;

        DB
            ::table('users')
            ->whereNotIn('id', [$adminId, $officeId])
            ->delete();

        foreach (range(1, 30) as $number){
            $user = new User;
            $user->desired_sponsor_id = $officeId;
            $user->email = "mail{$number}@mail.com";
            $user->first_name = "User {$number}";
            $user->last_name = "User {$number}";
            $user->login = "user ${number}";
            $user->password = Hash::make('secret');
            $user->save();
        }
    }
}