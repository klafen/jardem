<?php

use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RegenerateDesksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete all incorrect users
        $talgatbek = User::byLogin('Talgatbek');
        User::where('id', '>=', $talgatbek->id)->delete();

        $users = [];
        $date = Carbon::now()->month(2);

        foreach($this->getOrder() as $login) {
            $user = User::byLogin($login);
            $user->payment_date = $date;
            $user->save();

            $users[] = $user;
            $date->addMinute();
        }

        $others = User
            ::whereNotIn('login', $this->getOrder())
            ->whereNotNull('desired_sponsor_id')
            ->whereNotNull('payment_date')
            ->orderBy('payment_date')
            ->get();

        foreach($others as $user) {
            $users[] = $user;
        }

        DB::table('desks')->delete();
        DB::update('UPDATE users SET desk_id = null');
        DB::update('UPDATE users SET desk_position = null');
        DB::update('UPDATE users SET desk_order = null');
        DB::update('UPDATE users SET sponsor_id = null');
        // DB::update('UPDATE users SET payment_date = null');

        foreach($users as $user) {
            $sponsor = User::find($user->desired_sponsor_id);

            $user->fresh()->follow($sponsor);
        }

        DB::table('transactions')->delete();
    }

    private function getOrder()
    {
        return [
            'Lana',
            'Lana38',
            'SW999',
            'Lana21',
            'Pikap',
            'Verba',
            'Sirius',
            'Ildana',
            'more',
            'SO777',
            'Lori',
            'Graff',
            'Start',
            'Maestro',
            'Kabgul',
            'Iskra',
            'AlmaLedi',
            'Lord',
            'Gorod',
            'Narsha',
            'Uspech',
            'Ainash',
            'Lider',
            'Leha',
            'Diana',
            'Talgat',
            'Shaina',
            'Legenda',
            'Pobeda',
            'Altynai',
            'Finish',
            'Borken',
            'Gumar',
            'David6',
            'Luna',
            'Luna7',
            'Luna8',
            'Bakyt',
            'Aiya',
            'Marat',
            'Bota',
            'Limon',
            'Ledi',
            'Lyuba',
            'Aisly1819201',
            'Vorken',
            'Lyubov',
            'Lyba',
            'Lada',
            'Aisulu',
            'Buket',
            'Sulu',
            'Raihan',
            'Roza',
            'Venera',
            'Alexan',
            'Raksha',
            'Arsen',
            'Magnat',
            'Pavlodar',
            'Bagira',
            'Sapfir',
            'Pegas',
            'Mustang',
            'Izumrud',
            'Daniya',
            'Dollar',
            'Asko',
            'Zarina',
            'Aziza',
            'Altai',
            'Doni',
            'Wiking',
            'Guka',
            'Udacha',
            'Radost',
            'Ersen92',
            'Zliha94',
            'Zarina97',
            'Rubin',
            'Kristall',
            'Fiona',
            'Hont',
            'Bus',
            'Tont',
            'Saule',
            'Tau',
            'Gory',
            'Poli',
            'Agat',
            'Aigul',
            'Gold',
            'Leto',
            'Strela',
            'Kapiza',
            'Mira',
            'Erkin',
            'Luch',
            'Solnce',
            'Irbis',
            'Apelsin',
            'Lir',
            'Korol',
            'Danel',
            'Baks',
            'Tenge',
            'Silver',
            'Evro',
            'Golivud',
            'Aibat101',
            'Aibat102',
            'Aibat103',
            'Mars',
            'Pesnya',
            'Saturn',
            'Artist',
            'Tur',
            'Berkut',
            'Zulfiya73',
            'Muza',
            'Kabdulla',
            'Aizhan',
            'Amoha',
            'Zhakonyu',
            'Apple',
            'Alena',
            'Zagisha',
            'Aliman',
            'Nargiz',
            'Zvezda',
        ];
    }
}