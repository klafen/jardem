<?php

use App\BankRequisite;
use Faker\Provider\Lorem;
use Illuminate\Database\Seeder;

class BankRequisiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BankRequisite::truncate();

        foreach (range(1, 3) as $n){
            $number = '';
            for($i = 0; $i < 20; $i++){
                $number .= rand(0, 9);
            }
            BankRequisite::create([
                'user_id' => 2,
                'bank_account' => $number,
                'bank_requisite' => Lorem::text(100),
            ]);
        }
    }
}
