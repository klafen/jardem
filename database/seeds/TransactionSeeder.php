<?php

use App\Transaction;
use Carbon\Carbon;
use Faker\Provider\Lorem;
use Illuminate\Database\Seeder;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Transaction::truncate();

        foreach (range(1, 50) as $n){
            $sign = rand(1, 3) == 2 ? '-' : '+';

            $status = rand(1, 2) == 2 ? 'success' : 'wait';
            $status = rand(1, 4) == 2 ? 'fail' : $status;

            Transaction::create([
                'user_id' => 2,
                'sum' => $sign . $n * 100,
                'description' => Lorem::text(20),
                'status' => $status,
                'created_at' => Carbon::now()->addMinute($n),
            ]);
        }
    }
}
