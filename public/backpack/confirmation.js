$(function(){
    $('.approvement').on('click', function(event){
        if(!confirm('Подтвердить действие.')){
            event.preventDefault();
        }
    });

    $('.mark-as-paid').on('click', function(event){
        if(!confirm('Вы уверены, что хотите отметить пользователя "' + $(this).data('login') + '" как оплаченного?')){
            event.preventDefault();
        }
    });
})
